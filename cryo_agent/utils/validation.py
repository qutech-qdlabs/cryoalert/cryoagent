import logging
import re
from urllib.parse import urlparse

LOGGER = logging.getLogger(__name__)

REGEX_DOMAIN_NAME = re.compile(
    r"^(localhost|(?:[a-z0-9](?:[a-z0-9-]{0,61}[a-z0-9])?\.)+[a-z0-9][a-z0-9-_]{0,61}[a-z])$", re.IGNORECASE
)
REGEX_IP_ADDRESS = re.compile(r"^((25[0-5]|(2[0-4]|1\d|[1-9]|)\d)\.?\b){4}$", re.IGNORECASE)
REGEX_PORT = re.compile(
    r"^(6553[0-5]|655[0-2][0-9]|65[0-4][0-9]{2}|6[0-4][0-9]{3}|[1-5][0-9]{4}|[1-9][0-9]{0,3})$", re.IGNORECASE
)


def check_url(maybe_url: str) -> tuple[bool, str | None]:
    """
    Validate a URL for correctness

    Args:
        maybe_url (str): The URL to check for validity

    Returns:
        tuple[bool, str | None]: Boolean to indicate success.
            If not succesful, the second argument will contain the error message. Otherwise it will be None
    """
    if "://" in maybe_url and not maybe_url.startswith("http"):
        return False, "URL contains scheme other than `http(s)` "

    if not maybe_url.startswith("http://") and not maybe_url.startswith("https://"):
        maybe_url = f"http://{maybe_url}"

    netloc = urlparse(maybe_url).netloc
    if not netloc:
        LOGGER.debug(f"Could not parse the url `{maybe_url}`")
        return False, f"Could not parse the url `{maybe_url}`"

    if ":" not in netloc:
        address, port = netloc, None
    else:
        tmp = netloc.split(":")
        address, port = tmp[0], tmp[1]

    # Validate Address
    if address and address[0].isdigit():
        # Probably an IP-address
        if not REGEX_IP_ADDRESS.match(address) and not REGEX_DOMAIN_NAME.match(address):
            return False, f"Address `{address}` could not be matched to a valid ip-address or domain name"
    else:
        # Probably a Domain-name
        if not REGEX_DOMAIN_NAME.match(address) and not REGEX_IP_ADDRESS.match(address):
            return False, f"Address `{address}` could not be matched to a valid domain name or ip-address"

    # Validate Port
    if port is not None and not REGEX_PORT.match(port):
        return False, f"Port `{port} is not a valid port"

    return True, None
