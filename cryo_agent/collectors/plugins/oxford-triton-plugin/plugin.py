import logging
import re
from copy import deepcopy
from datetime import datetime
from typing import Any

from sqlalchemy.orm import Session

import cryo_agent
from cryo_agent.collectors.plugins.core import BaseCollectorPlugin
from cryo_agent.logic.base import write_latest_datapoints
from cryo_agent.utils.tcp_client import TcpRemoteClient

LOGGER = logging.getLogger(__name__)

VALUE_REGEX = re.compile(
    r"(?P<name>[A-Z]+):((?P<value>ON|OK|OFF|NOK|NOSW|FLT|ERR|[+\-]?(?:0|[1-9]\d*)(?:\.\d+)?(?:[eE][+\-]?\d+)?)(?P<unit>[A-Za-z]*))"
)


def parse_values(data: str):
    LOGGER.debug(f"Received {data=}")
    res = [m.groupdict() for m in VALUE_REGEX.finditer(data)]

    # Loop over all result items, to filter for string values, as also defined in the regex string
    for item in res:
        if isinstance(item["value"], str):
            if item["value"] in ["OK", "ON"]:
                item["value"] = 1
            elif item["value"] in ["NOK", "OFF", "ERR"]:
                item["value"] = 0
            else:
                item["value"] = float(item["value"])
        else:
            item["value"] = float(item["value"])

    LOGGER.debug(res)
    return res


class CollectorPlugin(BaseCollectorPlugin):
    """
    A multiprocess process for collecting data from an oxford-triton Cryostat
    """

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.tcp_client = TcpRemoteClient(*self.remote_address)

    def send(self, cmd: str) -> str | None:
        # Add connector name if needed
        if self.connector_name:
            cmd = f"@{self.connector_name}#{cmd}"

        # Add mandatory CRLF
        cmd = cmd + "\r\n"

        # Send through tcp client
        res = self.tcp_client.send(cmd.encode())
        if res is None:
            return None

        # Decode response and strip the (CR)LF
        return res.decode().strip()

    @property
    def remote_address(self) -> tuple[str, int]:
        return (self.settings.get("remote_host", "localhost"), int(self.settings.get("remote_port", 33576)))

    @property
    def connector_name(self) -> str:
        return self.settings.get("connector_name")

    def collect_values(self):
        ret = {}
        for data_group in self.data_groups:
            # Extract group meta information (everything except the datapoints information)
            group_meta = deepcopy(data_group)
            group_meta.pop("data_points")
            group_meta.update({"agent_version": cryo_agent.__version__})  # add version for compatability

            group_type = data_group.get("group_label")
            LOGGER.debug(f"Found group with {group_type=}: {data_group}")
            ret.update(self.collect_datagroup(data_group, group_meta))

        return ret

    def collect_datagroup(self, data_group: dict[str, Any], group_meta: dict[str, Any]):
        ret = {}
        for data_point in data_group.get("data_points", []):
            # Construct command to send
            data_command = data_group.get("data_command", None)
            if data_command:
                data_command = data_command.format(channel=data_point["channel"], data_name=data_point["data_name"])
                latest_data = self.collect_datapoint(data_command, data_point["data_name"])
                if latest_data:
                    latest_data.update({"meta": group_meta})
                    ret.update({data_point["id"]: latest_data})
            else:
                LOGGER.debug(f"No data_command was found for data_group={data_group.get('group_label')}")

        return ret

    def collect_datapoint(self, data_command: str, data_name: str):
        # Send command, and parse the result
        resp = self.send(data_command)
        if resp is None:
            return {}

        try:
            data_value = next(data_value for data_value in parse_values(resp) if data_value["name"] == data_name)
            val = float(data_value["value"])
        except StopIteration:
            LOGGER.warning(f"No value found for {data_name=}")
            val = float("nan")

        return {
            "timestamp": datetime.utcnow(),
            "value": val,
        }

    def collect(self, session: Session) -> None:
        """
        Entrypoint for single cycle of the collector
        """

        # Collect latest values from source
        latest_values = self.collect_values()
        if (latest_values is None) or (len(latest_values) == 0):
            return

        # Write latest_values
        write_latest_datapoints(session, latest_values)
