import logging
import multiprocessing as mp
import time
from copy import copy
from datetime import datetime
from multiprocessing.connection import Connection

from sqlalchemy import create_engine
from sqlalchemy.orm import Session

from cryo_agent.collectors.plugins import (  # pylint: disable=unused-import # import for loading
    tasks,
)
from cryo_agent.logic.collectors import get_collector
from cryo_agent.utils.base_process import BaseProcess
from cryo_agent.utils.exceptions import InvalidUnitException
from cryo_agent.utils.logging import init_process_logging
from cryo_agent.utils.tasks import TaskHandler
from cryo_agent.utils.types import CollectorDataGroupDict, CollectorDict
from cryo_agent.utils.units import get_base_unit

LOGGER = logging.getLogger(__name__)


class BaseCollectorPlugin(BaseProcess):
    """
    A multiprocess base process for collecting data from a data source
    """

    def __init__(self, sqlite_path: str, config: CollectorDict, log_queue: mp.Queue, task_conn: Connection):
        super().__init__()

        # Add properties
        self._sqlite_path = sqlite_path
        self.config = config
        self.log_queue = log_queue
        self.task_conn = task_conn
        self.task_handler: TaskHandler | None = None  # Initialized only when the new process has spawned

    @property
    def collector_name(self) -> str:
        return self.config["collector_name"]

    @property
    def collector_class(self) -> str:
        return self.config["collector_class"]

    @property
    def scan_interval(self) -> int:
        return self.config["scan_interval"]

    @property
    def log_level(self) -> str:
        return self.config["log_level"]

    @property
    def settings(self) -> dict:
        return self.config["settings"]

    @property
    def data_groups(self) -> list[CollectorDataGroupDict]:
        return self.config["data_groups"]

    def refresh_config(self) -> None:
        LOGGER.debug("refreshing config")
        with Session(create_engine(self._sqlite_path)) as session:
            collector = get_collector(session, self.collector_name)
            assert collector is not None

            prev_log_level = copy(self.log_level)  # copy because overwritten when updating config in next line
            self.config = collector.serialize()

            # Update the log_level here, because it behaves differently than the other settings
            if prev_log_level != collector.log_level:
                LOGGER.info(f"Changed root logging level to `{collector.log_level}`")
                logging.getLogger().setLevel(collector.log_level)

    def init(self) -> None:
        # Initialize task handler in the right process
        self.task_handler = TaskHandler(self, "plugin")

    def run(self) -> None:
        """
        Entrypoint of process to run the webserver
        """
        # Init logging again in new process
        init_process_logging(log_queue=self.log_queue, log_level=self.log_level)

        # Do any further initializion if needed
        self.init()

        # collect
        while not self.is_stopped:
            try:
                LOGGER.info(f"Starting CollectorPlugin `{self.collector_name}`")
                self._collect()
            except KeyboardInterrupt:
                pass
            except Exception as exc:
                LOGGER.error(exc, exc_info=True)
            finally:
                LOGGER.info(f"Stopped CollectorPlugin `{self.collector_name}`")
                try:
                    if not self.is_stopped:
                        time.sleep(5)
                except Exception:
                    pass

    def _collect(self):
        with Session(create_engine(self._sqlite_path)) as session:
            self.handle_tasks()  # Handle tasks if needed
            last_scan_cycle_time = datetime.fromtimestamp(0)
            while not self.is_stopped:
                if (datetime.utcnow() - last_scan_cycle_time).total_seconds() > self.scan_interval:
                    LOGGER.debug(f"Time to collect after {(datetime.utcnow() - last_scan_cycle_time).total_seconds()}")
                    self.collect(session)
                    LOGGER.debug(f"Finished collecting!")
                    last_scan_cycle_time = datetime.utcnow()
                else:
                    self.handle_tasks()
                    time.sleep(0.1)  # short sleep to not use all CPU

    def collect(self, session: Session) -> None:
        raise NotImplementedError("This method should be overwritten")

    def handle_tasks(self) -> None:
        if self.task_handler and self.task_conn.poll():
            self.task_handler.handle_task(self.task_conn.recv())

    @staticmethod
    def validate_collector_config(
        config: dict, collector_config: dict | None = None, key_exceptions: list[str] | None = None
    ) -> list[str]:
        """
        Validates a filled collector config.
        Optionally should be implemented by the plugin creator.
        Returns a list of error messages to be presented in the web UI

        Args:
            config (dict): The unvalidated config data
            collector_config (Optional, dict): The configuration of the collector as given by its plugin.yaml file
            key_exceptions (Optional, list[str]): List of keys that, when missing from config,
                will not trigger an error_message. Used when some keys are read-only.

        Returns:
            list[str]: list of error messages
        """
        error_messages = []
        key_exceptions = key_exceptions or []

        # Check keys in config that should be present
        if collector_config and "base" in collector_config:
            for key, base_config in collector_config["base"].items():
                if base_config.get("required", False) and key not in key_exceptions:
                    if key not in config:
                        error_messages.append(f"`{base_config['label']}` must be provided")

        return error_messages

    @staticmethod
    def validate_datagroup_config(
        config: dict, collector_config: dict | None = None, key_exceptions: list[str] | None = None
    ) -> list[str]:
        """
        Validates a filled datagroup config.
        Optionally should be implemented by the plugin creator.
        Returns a list of error messages to be presented in the web UI

        Args:
            config (dict): The unvalidated config data
            collector_config (Optional, dict): The configuration of the collector as given by its plugin.yaml file
            key_exceptions (Optional, list[str]): List of keys that, when missing from config,
                will not trigger an error_message. Used when some keys are read-only.
        Returns:
            list[str]: list of error messages
        """
        error_messages = []
        key_exceptions = key_exceptions or []

        # Check keys in config that should be present
        if collector_config and "data_groups" in collector_config:
            for key, data_group_config in collector_config["data_groups"].items():
                if data_group_config.get("required", False) and key not in key_exceptions:
                    if key not in config:
                        error_messages.append(f"`{data_group_config['label']}` must be provided")

        return error_messages

    @staticmethod
    def validate_datapoint_config(
        config: dict, collector_config: dict | None = None, key_exceptions: list[str] | None = None
    ) -> list[str]:
        """
        Validates a filled datagroup config.
        Optionally should be implemented by the plugin creator.
        Returns a list of error messages to be presented in the web UI

        Args:
            config (dict): The unvalidated config data
            collector_config (Optional, dict): The configuration of the collector as given by its plugin.yaml file
            key_exceptions (Optional, list[str]): List of keys that, when missing from config,
                will not trigger an error_message. Used when some keys are read-only.

        Returns:
            list[str]: list of error messages
        """
        error_messages = []
        key_exceptions = key_exceptions or []

        # Check keys in config that should be present
        if collector_config and "data_points" in collector_config:
            for key, data_point_config in collector_config["data_points"].items():
                if data_point_config.get("required", False) and key not in key_exceptions:
                    if key not in config:
                        error_messages.append(f"`{data_point_config['label']}` must be provided")

        # Check if unit can be made sense of
        unit = config["unit"]
        try:
            get_base_unit(unit)
        except InvalidUnitException:
            error_messages.append(f"Invalid unit `{unit}`")

        return error_messages

    def __str__(self) -> str:
        return f"<{self.__class__.__name__}(name={self.collector_name}, class={self.collector_class})>"

    __repr__ = __str__
