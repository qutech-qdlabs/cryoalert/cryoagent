from datetime import datetime

from flask import Blueprint, current_app
from sqlalchemy.orm import Session

from cryo_agent.logic.base import get_all_latest_datapoints
from cryo_agent.publisher.db import get_engine

bp = Blueprint("latest", __name__, url_prefix="/latest")


@bp.route("/")
def get_latest_data():
    with Session(get_engine()) as session:
        datapoint = get_all_latest_datapoints(session)

    # Send datapoints with some metadata
    return {
        "name": current_app.config["DATA_SOURCE_NAME"],
        "timestamp": datetime.utcnow(),
        "data": datapoint,
    }
