from __future__ import annotations

import logging
import multiprocessing as mp
import threading
from datetime import datetime
from multiprocessing.connection import Connection
from time import sleep
from typing import Optional

from sqlalchemy import create_engine
from sqlalchemy.orm import Session

from cryo_agent.collectors.manager import CollectorManager
from cryo_agent.logic.collectors import get_collector
from cryo_agent.publisher.process import WebProcess
from cryo_agent.utils.logging import logger_thread
from cryo_agent.utils.tasks import TaskHandler
from cryo_agent.utils.types import TaskDict

LOGGER = logging.getLogger(__name__)


class CryoAgent:
    """Class that controls the starting of processes used by the agent."""

    def __init__(
        self,
        web_proc: WebProcess,
        collector_manager: CollectorManager,
        sqlite_path: str,
        log_queue: mp.Queue,
        task_conn: Connection,
    ) -> None:
        self._web_proc = web_proc
        self._collector_manager = collector_manager
        self.log_queue = log_queue
        self._log_thread = threading.Thread(target=logger_thread, args=(self.log_queue,))
        self._sqlite_path = sqlite_path
        self._process_health_interval = 1
        self.task_handler: Optional[TaskHandler] = None  # Must be initialized in the correct process
        self.task_conn = task_conn

    def handle_task(self, task_data: TaskDict) -> None:
        if self.task_handler:
            self.task_handler.handle_task(task_data)
        else:
            LOGGER.warning("TaskHandler was not yet initialized, so task cannot be executed")

    def enable_collector(self, collector_name: str) -> None:
        with Session(create_engine(self._sqlite_path)) as session:
            collector = get_collector(session, collector_name)

        if collector:
            # With the collected collector setting, create the collector process and start it
            self._collector_manager.create_collector(collector)
            self._collector_manager.start_collector(collector.collector_name)
        else:
            LOGGER.warning(f"Tried to enable collector with name `{collector_name}, but no such collector exists")

    def disable_collector(self, collector_name: str):
        LOGGER.debug(f"Disabling Collector `{collector_name}`")
        self._collector_manager.stop_collector(collector_name)

        LOGGER.debug(f"Joining Collector `{collector_name}`")
        self._collector_manager.join_collector(collector_name)

        LOGGER.debug(f"Finished Disabling Collector `{collector_name}`")

    def setup(self) -> None:
        """Method that runs only once, before the first run of the loop."""

        # Initialize task handler in correct process
        self.task_handler = TaskHandler(self, "cryo_agent")

        # Start logging thread
        self._log_thread.start()

        # Start web process
        self._web_proc.start()

        # Start collectors
        self._collector_manager.start_collectors()

    def loop(self) -> None:
        """Method that run forever, unless aborted."""
        # Get start time of this scan cycle
        LOGGER.debug("Start new scan cycle")
        dt_start = datetime.utcnow()

        while self.task_conn.poll():
            self.handle_task(self.task_conn.recv())

        # SLeep to not over saturate this process if queue was empty
        sleep_time = max([0, self._process_health_interval - (datetime.utcnow() - dt_start).total_seconds()])
        LOGGER.debug(f"sleeping for {sleep_time} seconds")
        sleep(sleep_time)
        LOGGER.debug(f"slept for {sleep_time} seconds")

    def run(self) -> None:
        """Method for starting the processes and loop forever ensuring
        processes are alive."""

        # First, run the setup once
        self.setup()

        # run the loop forever
        LOGGER.info(f"Started {self.__class__.__name__}")
        try:
            while True:
                self.loop()
        except KeyboardInterrupt:
            # Stop the processes
            self._collector_manager.stop_collectors()
            self._web_proc.stop()

            # Wait untill both processes have stopped
            self._collector_manager.join_collectors()
            self._web_proc.join()

            # Stop the logging thread to finish
            self.log_queue.put(None)
            self._log_thread.join()

            # Print something on the terminal
            LOGGER.info(f"Aborting {self.__class__.__name__}!")
