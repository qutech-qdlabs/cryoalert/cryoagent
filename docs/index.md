# CryoAgent Documentation

The CryoAgent is part of the CryoAlert platform. It serves to safely expose data from a data source. An external application can poll data from the agent to obtain the latest set of data. A prometheus endpoint is available for scraping the latest datapoints.

Additional documentation about the [architecture](./development/architecture/index.md) and the [plugins](./development/plugins/index.md) is available. The architecture documentation describes the internal workings of the CryoAgent. The plugins documentation describes the available plugins in more detail and how to create your own (external) plugins.

It should be noted that CryoAgent was designed for use on Windows machines. While it should be possible to run CryoAgent on other platforms, this is not tested nor documented.

## Chapters

- [Overview](./overview.md)
- [Installation](./installation.md)
- [Usage](./usage/index.md)
- [Development](./development/index.md)
