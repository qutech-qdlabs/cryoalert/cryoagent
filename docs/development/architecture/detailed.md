# Detailed Architecture

As previously mentioned, CryoAgent uses `multiprocessing` to start its collectors and its web process. However, to facilitate communication between the processes, a so-called `TaskRouter` is used that can route `tasks` to every process. However, only the web process can send these tasks. An example might be to change the log-level of a certain collector, or to start/stop a certain collector. These tasks are triggered based on user input in the web interface and therefore allows for updating the collectors via the web interface without restarting them.

![Detailed Architecture](./img/DetailArch.png "Detailed Architecture")
