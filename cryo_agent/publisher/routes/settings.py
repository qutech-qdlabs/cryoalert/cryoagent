import logging

from flask import Blueprint, redirect, render_template, request, url_for
from sqlalchemy.orm import Session

from cryo_agent.logic.settings import (
    get_cryo_agent_settings,
    get_web_settings,
    write_cryo_agent_settings,
    write_web_settings,
)
from cryo_agent.publisher import tasks
from cryo_agent.publisher.db import get_engine

LOGGER = logging.getLogger(__name__)

bp = Blueprint("settings", __name__, url_prefix="/settings")


@bp.route("/")
def index():
    with Session(get_engine()) as session:
        return render_template(
            "settings/index.html",
            cryo_agent_settings=get_cryo_agent_settings(session),
            web_settings=get_web_settings(session),
        )


@bp.route("/update_cryo_agent_settings/", methods=["POST"])
def update_cryo_agent_settings():
    with Session(get_engine()) as session:
        changed_items = write_cryo_agent_settings(session, **request.form)

        if "log_level" in changed_items:
            tasks.update_cryo_agent_log_level(changed_items["log_level"])

    return redirect(url_for("settings.index", _anchor="cryo_agent_settings"))


@bp.route("/update_web_settings/", methods=["POST"])
def update_web_settings():
    with Session(get_engine()) as session:
        changed_items = write_web_settings(session, **request.form)

        if "log_level" in changed_items:
            tasks.update_web_log_level(changed_items["log_level"])

    return redirect(url_for("settings.index", _anchor="web_settings"))
