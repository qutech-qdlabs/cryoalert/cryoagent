import logging

from flask import current_app

from cryo_agent.collectors.plugins.tasks import Tasks as CollectorTasks
from cryo_agent.controller.tasks import Tasks as ControllerTasks
from cryo_agent.utils.tasks import TaskRouter

LOGGER = logging.getLogger(__name__)


def _get_task_router() -> TaskRouter:
    return current_app.config["TASK_ROUTER"]


def task_print(msg: str | None = None) -> None:
    _get_task_router().send(
        to_name="cryo_agent",
        task_data={
            "task": ControllerTasks.PRINT,
            "data": msg or "Hello There",
        },
    )


def update_cryo_agent_log_level(new_log_level: str) -> None:
    _get_task_router().send(
        to_name="cryo_agent",
        task_data={
            "task": ControllerTasks.UPDATE_LOG_LEVEL,
            "data": {"log_level": new_log_level},
        },
    )


def update_web_log_level(new_log_level: str) -> None:
    LOGGER.info(f"Changed root logging level to `{new_log_level}`")
    # This task remains in the web process, so execute immediately
    logging.getLogger().setLevel(new_log_level)


def enable_collector(collector_name: str) -> None:
    _get_task_router().send(
        to_name="cryo_agent",
        task_data={
            "task": ControllerTasks.ENABLE_COLLECTOR,
            "data": {"collector_name": collector_name},
        },
    )


def disable_collector(collector_name: str) -> None:
    """
    Disabling the collector effectively removes it.
    Disabling is idempotent, so disabling it again does nothing

    Args:
        collector_name (str): name of the collector to be deleted
    """
    _get_task_router().send(
        to_name="cryo_agent",
        task_data={
            "task": ControllerTasks.DISABLE_COLLECTOR,
            "data": {"collector_name": collector_name},
        },
    )


def update_collector_settings(collector_name: str, new_collector_settings: dict) -> None:
    LOGGER.debug(f"Updating the settings of collector `{collector_name}`")
    _get_task_router().send(
        to_name=collector_name,
        task_data={
            "task": CollectorTasks.UPDATE_SETTINGS,
            "data": new_collector_settings,
        },
    )
