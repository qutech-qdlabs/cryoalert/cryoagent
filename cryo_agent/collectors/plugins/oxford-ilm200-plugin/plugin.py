import logging
import re
from copy import deepcopy
from datetime import datetime
from typing import Any

from sqlalchemy.orm import Session

import cryo_agent
from cryo_agent.collectors.plugins.core import BaseCollectorPlugin
from cryo_agent.logic.base import write_latest_datapoints
from cryo_agent.utils.tcp_client import TcpRemoteClient

LOGGER = logging.getLogger(__name__)


class CollectorPlugin(BaseCollectorPlugin):
    """
    A multiprocess process for collector data from the Oxford ILM200 series
    """

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.tcp_client = TcpRemoteClient(*self.remote_address)

    def send(self, cmd: str) -> str | None:
        # Add mandatory CRLF
        cmd = cmd + "\r\n"

        # Send through tcp client
        res = self.tcp_client.send(cmd.encode())
        if res is None:
            return None

        # Decode response and strip the (CR)LF
        return res.decode().strip()

    def send_command(self, cmd: str) -> str | None:
        if self.isobus_address >= 0:
            data_cmd = f"@{self.isobus_address}{cmd}"
        else:
            data_cmd = cmd

        if self.connector_name:
            full_cmd = f"@{self.connector_name}#{data_cmd}"
        else:
            full_cmd = data_cmd

        return self.send(full_cmd)

    @property
    def connector_name(self) -> str:
        return self.settings.get("connector_name", None)

    @property
    def isobus_address(self) -> int:
        try:
            return int(self.settings.get("isobus_address", -1))
        except ValueError:
            return -1

    @property
    def remote_address(self) -> tuple[str, int]:
        return (self.settings.get("remote_host", "localhost"), int(self.settings.get("remote_port", 33576)))

    def collect_values(self):
        ret = {}
        for data_group in self.data_groups:
            # Extract group meta information (everything except the datapoints information)
            group_meta = deepcopy(data_group)
            group_meta.pop("data_points")
            group_meta.update({"agent_version": cryo_agent.__version__})  # add version for compatability

            command_type = data_group.get("command_type")
            LOGGER.debug(f"Found group with {command_type=}: {data_group}")
            if command_type.lower() == "read":
                ret.update(self.collect_read_data_group(data_group, group_meta))
            elif command_type.lower() == "status":
                ret.update(self.collector_status_data_group(data_group, group_meta))
            else:
                LOGGER.warning(f"Found unknown command_type: `{command_type}`. Expected `Read`, or `Status`.")

        return ret

    def collect_read_data_group(self, data_group: dict[str, Any], group_meta: dict[str, Any]):
        ret = {}
        for data_point in data_group.get("data_points", []):
            cmd = f"R{data_point['channel']}"
            latest_data = self.collect_read_data_point(cmd)
            if latest_data:
                latest_data.update({"meta": group_meta})
                ret.update({data_point["id"]: latest_data})

        return ret

    def collect_read_data_point(self, cmd: str) -> dict[str, Any]:
        resp = self.send_command(cmd)
        if resp is None:
            return {}

        try:

            val = float(resp[1:]) * 0.1
        except ValueError:
            val = float("nan")

        return {
            "timestamp": datetime.utcnow(),
            "value": val,
        }

    def collector_status_data_group(self, data_group: dict[str, Any], group_meta: dict[str, Any]):
        # TODO
        return {}

    def collect(self, session: Session) -> None:
        """
        Entrypoint for single cycle of the collector
        """

        # Collect latest values from source
        latest_values = self.collect_values()
        if (latest_values is None) or (len(latest_values) == 0):
            return

        # Write latest_values
        write_latest_datapoints(session, latest_values)
