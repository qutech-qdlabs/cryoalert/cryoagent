import logging

from sqlalchemy.orm import Query, Session, attributes

from cryo_agent.models import Collector, CollectorDataGroup, CollectorDataPoint

LOGGER = logging.getLogger(__name__)


def get_base_collector(session: Session) -> Collector | None:
    ret = (
        session.query(Collector)
        .filter(Collector.is_plugin == False)  # pylint: disable=singleton-comparison
        .filter(Collector.collector_name == "base")
        .first()
    )
    return ret


def setup_base_collector(session: Session) -> None:
    # Check if collector exists
    base_collector = get_base_collector(session)

    if base_collector is None:
        base_collector = Collector(collector_name="base", is_plugin=False)
        session.add(base_collector)
        session.commit()

    # Setup data groups
    _setup_nitrogen_data_group(session, base_collector_id=base_collector.id)
    _setup_alerts_data_group(session, base_collector_id=base_collector.id)

    # Commit
    session.commit()


def _setup_base_data_group(session: Session, base_collector_id: int, data_group_label: str) -> CollectorDataGroup:
    data_group: CollectorDataGroup | None = (
        session.query(CollectorDataGroup)
        .filter(CollectorDataGroup.collector_id == base_collector_id)
        .filter(CollectorDataGroup.group_label == data_group_label)
        .first()
    )
    if data_group is None:
        data_group = CollectorDataGroup(collector_id=base_collector_id, group_label=data_group_label)
        session.add(data_group)
        session.commit()  # Commit needed to obtain their id

    return data_group


def _setup_base_data_point(session: Session, data_group_id: int, variable_name: str, unit: str):
    datapoint = (
        session.query(CollectorDataPoint)
        .filter(CollectorDataPoint.data_group_id == data_group_id)
        .filter(CollectorDataPoint.variable_name == variable_name)
        .first()
    )
    if datapoint is None:
        datapoint = CollectorDataPoint(data_group_id=data_group_id, variable_name=variable_name, unit=unit)
        session.add(datapoint)


def _setup_nitrogen_data_group(session: Session, base_collector_id: int) -> None:
    nitrogen_data_group = _setup_base_data_group(session, base_collector_id, "nitrogen")

    # Add datapoints
    _setup_base_data_point(session, nitrogen_data_group.id, "clean", "seconds")
    _setup_base_data_point(session, nitrogen_data_group.id, "refill", "seconds")


def _setup_alerts_data_group(session: Session, base_collector_id: int) -> None:
    alerts_data_group = _setup_base_data_group(session, base_collector_id, "alerts")

    # Add datapoints
    _setup_base_data_point(session, alerts_data_group.id, "enabled", "")


def _base_query(session: Session, include_base: bool = False) -> Query:
    if include_base:
        return session.query(Collector)
    else:
        return session.query(Collector).filter(Collector.is_plugin)


def get_collectors(session: Session, include_base: bool = False) -> list[Collector]:
    return _base_query(session, include_base=include_base).all()


def get_enabled_collectors(session: Session) -> list[Collector]:
    return _base_query(session).filter(Collector.enabled).all()


def is_collector_name_used(session: Session, collector_name: str) -> bool:
    exists_query = _base_query(session).filter(Collector.collector_name == collector_name).exists()
    return session.query(exists_query).scalar()


def get_collector(session: Session, collector_name: str, **kwargs) -> Collector | None:
    return _base_query(session, **kwargs).filter(Collector.collector_name == collector_name).scalar()


def add_new_collector(session: Session, collector_name: str, collector_class_alias: str) -> None:
    """
    Add new collector to Collector table.
    It should already be verified that collector_name does not already exists in the table.

    Args:
        session (Session): Active session
        collector_name (str): Unique collector name for the to-be-created collector
        collector_class_alias (str): Unique alias of the collector class for the to-be-created collector
    """
    new_collector = Collector(
        collector_name=collector_name,
        collector_class=collector_class_alias,
        log_level="INFO",
        enabled=False,
        settings={},
        is_plugin=True,
    )
    session.add(new_collector)
    session.commit()


def update_collector(session: Session, collector_name: str, data: dict) -> None:
    collector = get_collector(session, collector_name)
    assert collector is not None
    for attr, val in data.items():
        if hasattr(collector, attr):
            setattr(collector, attr, val)
        else:
            collector.settings.update({attr: val})

    # Manually let SQLAlchemy know that the settings have been updated
    attributes.flag_modified(collector, "settings")
    session.commit()


def delete_collector(session: Session, collector_name: str) -> None:
    # Delete collector (including its children)
    collector = get_collector(session, collector_name)
    assert collector is not None
    session.delete(collector)
    session.commit()


def add_data_group(session: Session, collector_name: str, data_group_label: str, data: dict) -> None:
    collector = get_collector(session, collector_name)
    assert collector is not None
    collector.data_groups.append(CollectorDataGroup(group_label=data_group_label, settings=data))

    session.commit()


def get_data_group(session: Session, collector_id: int, group_label: str) -> CollectorDataGroup | None:
    return (
        session.query(CollectorDataGroup)
        .filter(CollectorDataGroup.collector_id == collector_id)
        .filter(CollectorDataGroup.group_label == group_label)
        .scalar()
    )


def update_data_group(session: Session, collector_name: str, group_label: str, data: dict) -> None:
    collector = get_collector(session, collector_name)
    assert collector is not None

    data_group = get_data_group(session, collector.id, group_label)
    assert data_group is not None

    data_group.settings.update(data)

    # Manually let SQLAlchemy know that the settings have been updated
    attributes.flag_modified(data_group, "settings")
    session.commit()


def delete_data_group(session: Session, collector_id: int, group_label: str) -> None:
    # Delete datagroup and its children
    data_group = get_data_group(session, collector_id, group_label)
    assert data_group is not None

    session.delete(data_group)

    session.commit()


def add_data_point(
    session: Session,
    collector_name: str,
    data_group_label: str,
    variable_name: str,
    unit: str,
    data: dict,
) -> None:
    collector = get_collector(session, collector_name)
    assert collector is not None

    data_group = get_data_group(session, collector.id, data_group_label)
    assert data_group is not None

    data_group.data_points.append(
        CollectorDataPoint(
            data_group_id=data_group.id,
            variable_name=variable_name,
            unit=unit,
            settings=data,
        )
    )

    session.commit()


def get_data_point(
    session: Session,
    data_group_id: int,
    variable_name: str,
) -> CollectorDataPoint | None:
    return (
        session.query(CollectorDataPoint)
        .filter(CollectorDataPoint.data_group_id == data_group_id)
        .filter(CollectorDataPoint.variable_name == variable_name)
        .scalar()
    )


def update_data_point(session: Session, data_group_id: int, variable_name: str, data: dict) -> None:
    data_point = get_data_point(session, data_group_id, variable_name)
    assert data_point is not None
    data_point.settings.update(data)

    # Manually let SQLAlchemy know that the settings have been updated
    attributes.flag_modified(data_point, "settings")
    session.commit()


def delete_data_point(session: Session, data_group_id: int, variable_name: str) -> None:
    # Delete datapoint
    data_point = get_data_point(session, data_group_id, variable_name)
    assert data_point is not None
    session.query(CollectorDataPoint).filter(CollectorDataPoint.id == data_point.id).delete()

    # NOTE because there is no link between variable names and datapoint labels, the latest_datapoint data cannot be updated
    session.commit()
