from datetime import datetime

from flask import Blueprint, redirect, render_template, request, url_for
from sqlalchemy.orm import Session

from cryo_agent.logic.clean import (
    get_last_cleaned_time,
    get_last_cleaned_time_ago,
    write_new_trap_clean,
)
from cryo_agent.publisher.db import get_engine

bp = Blueprint("clean", __name__)


@bp.route("/clean/", methods=["GET", "POST"])
def clean():
    with Session(get_engine()) as session:
        if request.method == "POST":
            clean_time = request.form.get("clean-time", None)
            if clean_time:
                clean_time = datetime.strptime(clean_time, "%Y-%m-%dT%H:%M")
            else:
                clean_time = datetime.now()
            write_new_trap_clean(session, clean_time=clean_time)
            return redirect(url_for("clean.clean"))

        return render_template(
            "clean.html",
            last_cleaned=get_last_cleaned_time(session),
            last_cleaned_ago=get_last_cleaned_time_ago(session),
        )
