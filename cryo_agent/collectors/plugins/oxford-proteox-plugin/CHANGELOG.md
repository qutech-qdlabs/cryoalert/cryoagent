# Changelog

All notable changes to this collector plugin will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.1.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [0.1.0] - 2024-10-30

### Added

### Fixed

- Fixed bug which caused response with multiple values to not be parsed, resulting in 'NaN'

### Changed

### Removed

## [0.0.1] - 2024-10-28

### Added

- Initial release

### Fixed

### Changed

### Removed
