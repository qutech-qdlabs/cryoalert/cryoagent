# Plugin Overview

Each plugin is a standalone process which, in its most simple description, reads data from a datasource and transforms it into a [standardized format](../architecture/database.md) and puts it into a database. How the plugin does this, is completely up to the plugin and highly specific to the datasource. However, there is some required structure to the plugin to make it cooperate nicely with the rest of the CryoAgent. This document will describe these required structures.

It is noted that there exists the possiblity of adding plugins external to the CryoAgent package. See the [external plugin](external-plugins.md) documentation for the details. All that is described in this document applies to every plugin, internal or external.

NB: a plugin is a collector. The two terms are used interchangeably in this document.

[[_TOC_]]

## Folder structure

The (minimal) folder structure for each plugin looks as follow:

```sh
my-plugin
    ├── CHANGELOG.md
    ├── plugin.py
    ├── plugin.yaml
    ├── README.md
```

Additional folders/files are possible to use for code seperation. Note that the plugin root folder name **must** end with `-plugin` for it to be recognized as a plugin folder. The [`create-plugin` CLI command](../../usage/cli.md#create-plugin) automatically adds this if not provided.

## CHANGELOG and README files

While not strictly necessary, it is good practice to maintain [CHANGELOG.md](https://keepachangelog.com/) and README.md files for each plugin. The README should clearly describe the configuration settings for the plugin.

## plugin.yaml

The `plugin.yaml` file provides a configuration interface for the plugin. This will be used by CryoAgent to construct the web interface for the plugin and how to start the plugin.

It is divided into three parts; a descriptive part, a runtime part, and a settings part.

### Descriptive part

The descriptive part provided CryoAgent with information about the plugin, specifically `alias`. The `alias` is used inside the web interface as its name/identifier.

```yaml
name: 'Bluefors HTTP Plugin'
version: "0.0.1"
alias: "bluefors-http"
creator: "d.brinkman-1@tudelft.nl"
description: "Collector plugin for Bluefors Fridges using its HTTP API"
```

### Runtime part

The runtime part provides information that CryoAgent needs to run the plugin. Currently, this is only the name of the file which contains the plugin entry code.

```yaml
runtime:
  main: plugin.py
```

### Settings part

The settings part provided information about the (required) configuration for the plugin. It is used by CryoAgent to render the web interface for configuring this plugin. The configuration is provided to the plugin process.

The minimal structure of the settings part is provided below, and can be divided into three parts; Base, Data group, and Data points.

#### Base

The base part provides a configuration definition for the overall collector. For example, one can provide configuration options for connection parameters to the datasource here. This information is then available for all data groups and points of this collector.

This part _can_ have a special property called `data_groups`. If provided, it will expect a `data_groups` part to be included in the configuration. In almost all cases, you would want this.

```yaml
base:
  #some_collector_setting:
  #  label: "label"
  #  type: int #or str, float, bool (list is reserved for data_groups and data_points)
  #  required: false #or true
  #  description: "Description of the label"
  #  choices: ["list", "of", "choices", "for", "str", "types"]
  data_groups:
    label: "Data groups"
    type: list
```

#### Data groups

If the base part provided the special property `data_groups`, then this part is also necessary.

This part _must_ have the property called `group_label`. This property is used as an unique identifier for the group within a collector.

This part _can_ have a special property called `data_points`. If provided, it will expect a `data_points` part to be included in the configuration. In almost all cases, you would want this.

```yaml
data_groups:
  group_label: 
    label: "Group label" 
    type: str
    required: true  
    description: "Unique name (per collector) of the data group"
  data_points:
    label: "Data points"
    type: list 
```

#### Data points

If the data groups part provided the special property `data_points`, then this part is also necessary.

This part _must_ have two properties called `variable_name` and `unit`. Their meanings are clear. Additionaly properties can be defined and used to extract data from the datasource.

```yaml
data_points: 
  variable_name:
    label: "Variable name"
    type: str
    required: true
    description: "Variable name of the data point. Will be used when exporting data"
  unit:
    label: "Unit"
    type: str 
    required: false
    description: "Unit of the retrieved variable. SI-prefixes are understood"
```

#### Minimal structure

```yaml
settings:
  base:
    data_groups:
      label: "Data groups"
      type: list
  data_groups:
    group_label: 
      label: "Group label" 
      type: str
      required: true  
      description: "Unique name (per collector) of the data group"
    data_points:
      label: "Data points"
      type: list 
  data_points: 
    variable_name:
      label: "Variable name"
      type: str
      required: true
      description: "Variable name of the data point. Will be used when exporting data"
    unit:
      label: "Unit"
      type: str 
      required: false
      description: "Unit of the retrieved variable. SI-prefixes are understood"
```

## plugin.py

This file contains the code that effectively _is_ the collector. Note that the name of the file may be different, depending on the settings in the [runtime](#runtime-part) part. For more details about the structure of the code, see the [documentation](plugin_process.md) of the plugin process.
