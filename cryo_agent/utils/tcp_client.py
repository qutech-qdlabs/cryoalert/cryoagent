import logging
import socket
from typing import Optional

LOGGER = logging.getLogger(__name__)


class TcpRemoteClient:
    def __init__(self, host: str, port: int) -> None:
        self._socket: Optional[socket.socket] = None
        self._host = host
        self._port = port
        self.is_connected = False

    def connect(self):
        self._socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self._socket.connect((self._host, self._port))
        self.is_connected = True
        LOGGER.info(f"Connected to remote server located on {self._host, self._port}")

    def disconnect(self):
        self._socket.close()
        self.is_connected = False
        self._socket = None

    def send(self, data: bytes) -> Optional[bytes]:
        # Check if is connected. If not, connect
        try:
            LOGGER.debug("Check connection")
            if not self.is_connected:
                self.connect()
            assert self._socket is not None

            LOGGER.debug(f"Sending: {data!r}")
            self._socket.sendall(data)

            LOGGER.debug("Retrieve data")
            res: bytes | None = self._socket.recv(2048)
        except (ConnectionError, ConnectionRefusedError, ConnectionAbortedError, ConnectionResetError):
            LOGGER.info(f"Could not connect to remote server located on {self._host, self._port}")
            self.disconnect()
            return None

        LOGGER.debug(f"Received: {res!r}")
        if not res:
            LOGGER.info(f"Connection on remote server located on {self._host, self._port} was closed")
            res = None
            self.disconnect()
        return res
