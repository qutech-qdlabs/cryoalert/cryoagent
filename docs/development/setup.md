# Development Setup

For development, the following steps are needed:

- clone this repository
- install poetry globally: `pip install poetry`
- (Optional) configure poetry to create a virtual environment in the project folder such that your IDE knows which interpreter it needs to look at.
- install dependencies and create virtual environment using `poetry install`
- run cryo_agent locally using `poetry run cryo_agent <command>`
- New features should always be pushed to a new feature_branch before merging into main (or develop, if it exists)

## CLI Usage

The following commands are available:

```cmd
> cryo_agent --help
Usage: cryo_agent [OPTIONS] COMMAND [ARGS]...

  CLI commands to run the CryoAgent

Options:
  --help  Show this message and exit.

Commands:
  change-port    CLI command to change the web port of the agent
  create-plugin  CLI command for creating external plugins
  start          CLI command to start the agent
```

For a more detailed explanation of the CLI commands, see its [documentation](../usage/cli.md)
