# Architecture Documentation

## Functional description

Within the CryoAlert platform, CryoAgent is responsible for collecting data from the cryogenic fridges used for quantum experiments. It does this by means of so-called `collectors`, which extracts data in some manner and transforms it into a standardized form, stored in a local [SQLite](https://www.sqlite.org/) database. This data is then extracted by a [Prometheus](https://prometheus.io/) instance via a `/metrics/` web endpoint made available by the CryoAgent.

Furthermore, CryoAgent provides a web interface for the operators of a fridge. Using this interface, operators can add, modify, or delete collectors, modify alert limits, enable/disable alerts, and add entries into a logbook.

To do all this, CryoAgent uses the `multiprocessing` package for the various processes and for the communication between the processes, and the `Flask` package for the web process

## Chapters

- [Detailed Architecture](detailed.md)
- [Database Design](database.md)
