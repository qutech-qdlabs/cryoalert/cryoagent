from typing import Any

from sqlalchemy.orm import Session


def get_column_names(session: Session, table_name: str) -> list[str]:
    return session.execute(f"SELECT name FROM pragma_table_info('{table_name}');").scalars().all()


def add_column(session: Session, table_name: str, column_name: str, column_type: str) -> None:
    session.execute(f"ALTER TABLE {table_name} ADD COLUMN {column_name} {column_type}")


def get_latest_datapoint_var_name(session: Session, var_name: str) -> list[tuple[str, dict[str, Any]]]:
    """Get the latest datapoint of a variable name across all collectors, in case of datapoints with the same name

    Args:
        session (Session): sqlalchemy.orm.Session
        var_name (str): Variable name to be retrieved

    Returns:
        list[tuple[str, dict[str, Any]]]: return a list(<string:collector_name>, <dict:datapoint_data>).
        example: [('test', '{"var_name":"50K-flange","timestamp":1672308650282,"value":46.07,"unit":"kelvin","meta":{}}')]
    """
    query_txt = f'SELECT collector_name, value FROM latest_data_point, json_each(latest_data_point.data, "$") WHERE json_extract(value, "$.var_name") =  "{var_name}";'
    return session.execute(query_txt).all()
