import logging

import numpy as np

LOGGER = logging.getLogger(__name__)


def steinhart_hart_model(r: float, a: float, b: float, c: float) -> float:
    """
    Compute the temperature given a measured resistance by means of the Steinhart-Hart model:

    1/T = a + b*ln(R) + c*ln(R)^3

    Args:
        r (float): input/measured resistance [Ohm]
        a (float): model parameter
        b (float): model parameter
        c (float): model parameter

    Returns:
        float: Temperature [K]
    """
    try:
        return a + b * np.log(r) + c * (np.log(r) ** 3)
    except Exception as exc:
        LOGGER.error(exc, exc_info=True)
        return float("nan")


def convert(raw_val: float, conversion_data: dict[str, float]) -> float:
    a, b, c = conversion_data["a"], conversion_data["b"], conversion_data["c"]
    return 1 / steinhart_hart_model(raw_val, a, b, c)


def validate_conversion_data(conversion_data: dict) -> list[str]:
    error_messages = []

    for param in ["a", "b", "c"]:
        if param not in conversion_data:
            error_messages.append(f"Parameter `{param}` was not present in conversion data. Could not convert data")

    return error_messages
