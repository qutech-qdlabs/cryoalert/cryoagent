import logging

import numpy as np

LOGGER = logging.getLogger(__name__)


def allen_barry_model(r: float, a: float, b: float, c: float) -> float:
    """
    Compute the temperature given a measured resistance by means of the Allen-Barry model:

    T = exp(ln((ln(R)-a)/b)/(-c))

    Args:
        r (float): input/measured resistance [Ohm]
        a (float): model parameter
        b (float): model parameter
        c (float): model parameter

    Returns:
        float: Temperature [K]
    """
    try:
        eq1 = np.log(r) - a
        eq2 = eq1 / b
        eq3 = np.log(eq2) / c
        return np.exp(eq3)
    except Exception as exc:
        LOGGER.error(exc, exc_info=True)
        return float("nan")


def convert(raw_val: float, conversion_data: dict[str, float]) -> float:
    a, b, c = conversion_data["a"], conversion_data["b"], conversion_data["c"]
    return 1 / allen_barry_model(raw_val, a, b, c)


def validate_conversion_data(conversion_data: dict) -> list[str]:
    error_messages = []

    for param in ["a", "b", "c"]:
        if param not in conversion_data:
            error_messages.append(f"Parameter `{param}` was not present in conversion data. Could not convert data")

    return error_messages
