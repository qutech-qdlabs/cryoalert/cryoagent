from datetime import datetime

from flask import Blueprint, redirect, render_template, request, url_for
from sqlalchemy.orm import Session

from cryo_agent.logic.nitrogen import (
    get_last_filled_time,
    get_last_filled_time_ago,
    write_new_trap_fill,
)
from cryo_agent.publisher.db import get_engine

bp = Blueprint("nitrogen", __name__)


@bp.route("/nitrogen/", methods=["GET", "POST"])
def nitrogen():
    with Session(get_engine()) as session:
        if request.method == "POST":
            refill_time = request.form.get("refill-time", None)
            if refill_time:
                refill_time = datetime.strptime(refill_time, "%Y-%m-%dT%H:%M")
            else:
                refill_time = datetime.now()

            write_new_trap_fill(session, refill_time=refill_time)
            return redirect(url_for("nitrogen.nitrogen"))

        return render_template(
            "nitrogen.html",
            last_filled=get_last_filled_time(session),
            last_filled_ago=get_last_filled_time_ago(session),
        )
