import logging
from copy import deepcopy
from typing import Any

from flask import Blueprint, request
from sqlalchemy.orm import Session

from cryo_agent.logic.alerts import write_new_alert_definitions
from cryo_agent.logic.settings import get_cryo_agent_settings
from cryo_agent.publisher.db import get_engine

LOGGER = logging.getLogger(__name__)


bp = Blueprint("alerts", __name__, url_prefix="/alerts")


@bp.route("/update_alert_definitions/", methods=["POST"])
def update_alert_definitions():
    if request.method != "POST":
        # Return false in any other case
        return {"ok": False}

    LOGGER.debug("Received new alert definitions")
    with Session(get_engine()) as session:
        # Dont change the alert settings with the API when the CryoAgentSettings sepcify another upload method
        cas = get_cryo_agent_settings(session)
        if cas.alert_settings_upload_method is None or cas.alert_settings_upload_method.lower() != "api":
            LOGGER.debug("Changing the alert settings via the API has been disabled")
            return {
                "ok": False,
                "error_message": "Changing the alert settings via the API has been disabled",
            }

        # Extract alert name
        data: list[dict[str, Any]] = deepcopy(request.json)  # type: ignore
        write_new_alert_definitions(session, alert_definitions=data)

        # Return response
        return {"ok": True}
