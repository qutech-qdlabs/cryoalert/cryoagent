from flask import Blueprint

from .v1.blueprint import bp as bp_api_v1

# Define API parent blueprint
bp = Blueprint("api", __name__, url_prefix="/api")

# Add versioned api children blueprints
bp.register_blueprint(bp_api_v1)
