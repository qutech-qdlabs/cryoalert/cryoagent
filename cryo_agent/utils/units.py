from cryo_agent.utils.exceptions import InvalidUnitException

SI_PREFIXES = {
    "y": 1e-24,
    "z": 1e-21,
    "a": 1e-18,
    "f": 1e-15,
    "p": 1e-12,
    "n": 1e-9,
    "u": 1e-6,
    "m": 1e-3,
    "h": 1e2,
    "k": 1e3,
    "M": 1e6,
    "G": 1e9,
    "T": 1e12,
    "P": 1e15,
    "E": 1e18,
    "Z": 1e21,
    "Y": 1e24,
}

READABLE_UNITS_MAP = {
    # pressures
    "Pa": "pascal",
    "Bar": "bar",
    "Psi": "psi",
    #
    # time
    "s": "seconds",
    "seconds": "seconds",  # Add for clarity in config files
    #
    # temperatures
    "K": "kelvin",
    "Kelvin": "kelvin",
    "Celsius": "celsius",
    "C": "celsius",
    "F": "fahrenheit",
    "Fahrenheit": "fahrenheit",
    #
    # weights
    "kg": "kg",
    #
    # Frequencies
    "Hz": "hertz",
    #
    # Power
    "W": "watt",
}


def convert_value_unit(val: float, unit: str) -> tuple[float, str]:
    """Convert the value in the given unit to the corresponding base unit
    This is a very basic function, only converts what was needed. If more
    is needed, please add to this function.
    Can only deal with single letter prefixes.

    Args:
        val (float): value
        unit (str): unit of the value

    Returns:
        val (float): value in the base unit
        unit (str): base unit
    """
    if unit:
        if unit in READABLE_UNITS_MAP:
            return val, READABLE_UNITS_MAP[unit]
        else:
            # Extract prefix and base_unit
            prefix, base_unit = unit[0], unit[1:]

            if base_unit not in READABLE_UNITS_MAP:
                raise InvalidUnitException(f"Unknown unit encountered: {base_unit}")

            # Convert the value and return it and the base unit
            return val * SI_PREFIXES[prefix], READABLE_UNITS_MAP[base_unit]
    else:
        return val, ""


def get_base_unit(unit: str) -> str:
    if unit:
        if unit in READABLE_UNITS_MAP:
            return READABLE_UNITS_MAP[unit]
        else:
            # Extract prefix and base_unit
            base_unit = unit[1:]

            if base_unit not in READABLE_UNITS_MAP:
                raise InvalidUnitException(f"Unknown unit encountered: {base_unit}")

            # Convert the value and return it and the base unit
            return READABLE_UNITS_MAP[base_unit]
    else:
        return ""
