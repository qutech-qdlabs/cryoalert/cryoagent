# Helper scripts

## Bluefors HTTP Demo Server

This demo server simulates the Bluefors HTTP API. It allows for testing the `bluefors-http-plugin` collector as realistically as possible. The server can be run using the following command:

```py
python bluefors_http_demo_server.py
```
