from dataclasses import dataclass
from typing import Any, Optional

from dataclasses_json import CatchAll, Undefined, dataclass_json


@dataclass_json(undefined=Undefined.INCLUDE)
@dataclass
class ValueDataClass:
    value: Any
    date: int
    outdated: bool
    extra_field: CatchAll


@dataclass_json(undefined=Undefined.INCLUDE)
@dataclass
class ContentDataClass:
    extra_field: CatchAll
    latest_valid_value: Optional[ValueDataClass] = None
    latest_value: Optional[ValueDataClass] = None


@dataclass_json(undefined=Undefined.INCLUDE)
@dataclass
class DataPointDataClass:
    name: str
    type: Optional[str]
    extra_field: CatchAll
    content: Optional[ContentDataClass] = None


@dataclass_json(undefined=Undefined.INCLUDE)
@dataclass
class BlueforsDataClass:
    data: dict[str, DataPointDataClass]
    extra_field: CatchAll
