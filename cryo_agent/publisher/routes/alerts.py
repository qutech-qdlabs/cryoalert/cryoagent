import logging

import yaml

try:
    from yaml import CLoader as Loader
except ImportError:
    from yaml import Loader

from flask import Blueprint, flash, redirect, render_template, request, url_for
from sqlalchemy.orm import Session

from cryo_agent.logic.alerts import (
    get_alert_definition,
    get_alert_settings,
    get_current_enabled_status,
    save_new_alert_values,
    write_new_alert_definitions,
    write_new_enable_status,
)
from cryo_agent.logic.settings import get_cryo_agent_settings
from cryo_agent.publisher.db import get_engine
from cryo_agent.utils.misc import recursive_get

LOGGER = logging.getLogger(__name__)

bp = Blueprint("alerts", __name__, url_prefix="/alerts")


@bp.route("/", methods=["GET", "POST"])
def overview():
    with Session(get_engine()) as session:
        if request.method == "POST":
            if "toggle_enable" in request.form:
                write_new_enable_status(session)
                return redirect(url_for("alerts.overview"))
            elif "update_alert_settings" in request.form:
                if "alert_file" not in request.files:
                    flash("No file was selected")
                    return redirect(url_for("alerts.overview"))

                alert_file = request.files["alert_file"]
                if alert_file.filename == "":
                    flash("No file was selected")
                    return redirect(url_for("alerts.overview"))

                write_new_alert_definitions(session, alert_definitions=yaml.load(alert_file.stream, Loader=Loader))
                return redirect(url_for("alerts.overview"))

        return render_template(
            "alerts/alerts_overview.html",
            alert_settings=get_alert_settings(session),
            current_status=get_current_enabled_status(session),
            cryo_agent_settings=get_cryo_agent_settings(session),
        )


@bp.route("/modify/<int:alert_id>", methods=["GET", "POST"])
def modify(alert_id):
    with Session(get_engine()) as session:
        if request.method == "POST":
            # Extract from form
            enabled = "alert_enabled" in request.form
            lower_bounds_enable = "lower_bounds_enable" in request.form
            lower_bounds_value = request.form.get("lower_bounds_value", None)
            upper_bounds_enable = "upper_bounds_enable" in request.form
            upper_bounds_value = request.form.get("upper_bounds_value", None)
            importance = request.form.get("importance", 0)

            # Construct new values dict
            new_values = {"enabled": enabled, "bounds": {}}
            if lower_bounds_value is not None:
                try:
                    val = float(lower_bounds_value)
                except ValueError:
                    flash("Could not parse 'Lower bound value'")
                else:
                    new_values["bounds"].update({"lower": {"enabled": lower_bounds_enable, "value": val}})

            if upper_bounds_value is not None:
                try:
                    val = float(upper_bounds_value)
                except ValueError:
                    flash("Could not parse 'Upper bound value'")
                else:
                    new_values["bounds"].update({"upper": {"enabled": upper_bounds_enable, "value": val}})

            # If everything is okay, save the new values
            lower_bounds_ok = lower_bounds_value is None or (
                lower_bounds_value is not None and recursive_get(new_values, "bounds", "lower", "value") is not None
            )
            upper_bounds_ok = upper_bounds_value is None or (
                upper_bounds_value is not None and recursive_get(new_values, "bounds", "upper", "value") is not None
            )
            if lower_bounds_ok and (upper_bounds_ok):
                save_new_alert_values(session, alert_id, new_values, importance=importance)
                return redirect(url_for("alerts.overview"))

        return render_template(
            "alerts/alerts_modify.html",
            alert_def=get_alert_definition(session, alert_id),
        )
