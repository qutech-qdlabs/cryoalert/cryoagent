import logging
from copy import deepcopy

from sqlalchemy.orm import Session

import cryo_agent
from cryo_agent.collectors.plugins.core import BaseCollectorPlugin
from cryo_agent.logic.base import write_latest_datapoints
from cryo_agent.utils.types import CollectorDataGroupDict, DataPointData

LOGGER = logging.getLogger(__name__)


class CollectorPlugin(BaseCollectorPlugin):
    """
    A multiprocess process for collecting data
    """

    def _loop_data_group(self, data_group_info: CollectorDataGroupDict) -> dict[int, DataPointData]:
        """
        Internal method to process for a specific data group
        """

        # Extract group meta information (everything except the datapoints information)
        # This is good practice to keep track of where each datapoint came from and from which version of cryo-agent
        group_meta: dict = dict(**data_group_info)  # convert to raw dict to do further manipulation
        group_meta.pop("data_points")
        group_meta.update({"agent_version": cryo_agent.__version__})  # add version for compatability

        # Get data for this datagroup here
        ...

        return {}

    def collect(self, session: Session) -> None:
        """
        Entrypoint for single cycle of the collector
        """

        # Implement collection logic here
        latest_values: dict[int, DataPointData] = {}
        for data_group_info in self.data_groups:
            latest_values.update(self._loop_data_group(data_group_info))

        # Write latest_values
        write_latest_datapoints(session, latest_values)

    @staticmethod
    def validate_collector_config(
        config: dict, collector_config: dict | None = None, key_exceptions: list[str] | None = None
    ) -> list[str]:
        """
        Validates a filled collector config.
        Optionally should be implemented by the plugin creator.
        Returns a list of error messages to be presented in the web UI

        Args:
            config (dict): The unvalidated config data
            collector_config (Optional, dict): The configuration of the collector as given by its plugin.yaml file
            key_exceptions (Optional, list[str]): List of keys that, when missing from config,
                will not trigger an error_message. Used when some keys are read-only.

        Returns:
            list[str]: list of error messages
        """
        error_messages = BaseCollectorPlugin.validate_collector_config(
            config,
            collector_config=collector_config,
            key_exceptions=key_exceptions,
        )

        # Add custom validation here

        return error_messages

    @staticmethod
    def validate_datagroup_config(
        config: dict, collector_config: dict | None = None, key_exceptions: list[str] | None = None
    ) -> list[str]:
        """
        Validates a filled datagroup config.
        Optionally should be implemented by the plugin creator.
        Returns a list of error messages to be presented in the web UI

        Args:
            config (dict): The unvalidated config data
            collector_config (Optional, dict): The configuration of the collector as given by its plugin.yaml file
            key_exceptions (Optional, list[str]): List of keys that, when missing from config,
                will not trigger an error_message. Used when some keys are read-only.
        Returns:
            list[str]: list of error messages
        """
        error_messages = BaseCollectorPlugin.validate_datagroup_config(
            config,
            collector_config,
            key_exceptions=key_exceptions,
        )

        # Add custom validation here

        return error_messages

    @staticmethod
    def validate_datapoint_config(
        config: dict, collector_config: dict | None = None, key_exceptions: list[str] | None = None
    ) -> list[str]:
        """
        Validates a filled datagroup config.
        Optionally should be implemented by the plugin creator.
        Returns a list of error messages to be presented in the web UI

        Args:
            config (dict): The unvalidated config data
            collector_config (Optional, dict): The configuration of the collector as given by its plugin.yaml file
            key_exceptions (Optional, list[str]): List of keys that, when missing from config,
                will not trigger an error_message. Used when some keys are read-only.

        Returns:
            list[str]: list of error messages
        """
        error_messages = BaseCollectorPlugin.validate_datapoint_config(
            config,
            collector_config=collector_config,
            key_exceptions=key_exceptions,
        )

        # Add custom validation here

        return error_messages
