# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.1.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [2.1.4] - 2024-11-04

### Added

### Fixed

### Changed
  
### Removed

- Removed pandas package; it was no longer used

## [2.1.3] - 2024-10-30

### Added

### Fixed

### Changed

- Updated `oxford-proteox-plugin` to version 0.1.0. See its [changelog](./cryo_agent/collectors/plugins/oxford-proteox-plugin/CHANGELOG.md) for more information
- Updated `waitress` package to version 3.0.1
  
### Removed

## [2.1.2] - 2024-10-28

### Added

- Added Oxford-Proteox-Plugin

### Fixed

- Fixed unable to add new data_groups due to `group_label` becoming "read-only". This was caused by a pass-by-reference bug

### Changed

- Updated `werkzeug` package to version 3.0.6
  
### Removed

## [2.1.1] - 2024-10-23

### Added

### Fixed

- Fixed unable to add new data_groups due to 'missing group_label'

### Changed
  
- Changed import ordering (isort)

### Removed

- Removed unused imports

## [2.1.0] - 2024-09-03

### Added

- Added alert settings table to be ordered by means of an 'importance' variable
- Added QCoDeS usage documentation
- Added `CONTRIBUTING.md`
- Added `CODE_OF_CONDUCT.md`
- Added code style development documentation
- Added selectable datetime for nitrogen trap refill and cleaning actions
- Added Collector input validation
- Added Data group input validation
- Added Data point input validation
- Added URL validation utility
- Added Plugin template directory and files
- Added the `create-plugin` CLI command to create external plugins using the CLI
- Added the option to create a plugin settings variables with a boolean type
- Added log level to collectors overview page

### Fixed

- Fixed plugin template having incorrect `validate_*` method signatures
- Fixed data groups not saveable due to missing readonly parameter in validation
- Fixed WebUI of collectors sizing differently when typing
- Fixed deleting of latest data entries (`/latest_data/modify/<data_point_id>`) not working

### Changed

- Updated `bluefors-pressure-logs-plugin` to 0.1.0. See its [changelog](./cryo_agent/collectors/plugins/bluefors-pressure-logs-plugin/CHANGELOG.md) for more information
- Updated `bluefors-status-logs-plugin` to 0.1.0. See its [changelog](./cryo_agent/collectors/plugins/bluefors-status-logs-plugin/CHANGELOG.md) for more information
- Updated `bluefors-temperature-logs-plugin` to 0.1.0. See its [changelog](./cryo_agent/collectors/plugins/bluefors-temperature-logs-plugin/CHANGELOG.md) for more information
- Updated all dependencies
- Added setuptools dependency explicitly
- Refactored templates to better reuse code
- Updated `bluefors-http-plugin` to 0.1.0. See its [changelog](./cryo_agent/collectors/plugins/bluefors-http-plugin/CHANGELOG.md) for more information

### Removed

## [2.0.6] - 2024-07-16

### Added

- Added the "Hertz (Hz)" unit
- Added the "Watt (W)" unit

### Fixed

### Changed

### Removed

## [2.0.5] - 2024-07-16

### Added

### Fixed

- Fixed not able to load external plugins due to `Path.cwd()` not added to the `sys.path` list
- Fixed not able to provide floating point numbers in numerical values

### Changed

### Removed

## [2.0.4] - 2024-07-12

### Added

- Start of documentation

### Fixed

- Fixed settings with options not being saved on data-groups and data-points pages

### Changed

- Updated `resistance-thermometer-plugin` to `0.2.0`. See its [changelog](./cryo_agent/collectors/plugins/resistance-thermometer-plugin/CHANGELOG.md)

### Removed

## [2.0.3] - 2024-06-21

### Added

### Fixed

- Fixed `/metrics/` endpoint crashing after data_point was removed from a collector
- Fixed `bluefors-pressure-logs-plugin` crashing when no logfile was found
- Fixed `bluefors-status-logs-plugin` crashing when no logfile was found
- Fixed `bluefors-temperature-logs-plugin` crashing when no logfile was found

### Changed

### Removed

- Removed unused files in `bluefors-status-logs-plugin`

## [2.0.2] - 2024-06-14

### Added

### Fixed

- Fixed `/metrics/` endpoint crashing after an alert update has removed an alert definition which also has lower and/or upper bounds

### Changed

### Removed

## [2.0.1] - 2024-06-14

### Added

### Fixed

- Fixed `/metrics/` endpoint crashing after an alert update has removed an alert definition

### Changed

### Removed

## [2.0.0] - 2024-06-11

### Added

### Fixed

### Changed

- Updated resistance thermometer plugin to 0.1.0 (see its [changelog](./cryo_agent/collectors/plugins/resistance-thermometer-plugin/CHANGELOG.md))

### Removed

## [2.0.0.dev0+0.2.5] - 2024-05-20

### Added

- Added CHANGELOG.md
- Added MIT license
- Triton Collector:  Add `data_name` to `data_command` format options

### Fixed

- Triton Collector: Make `connector_name` not required.
- Fixed bug that caused web process to be unaware of newly started collectors.

### Changed

- Triton Collector: Allow more data groups

### Removed
