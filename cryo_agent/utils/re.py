# pylint: disable=invalid-name
import re


def is_match(regex: re.Pattern[str], file_name) -> bool:
    # Match file name. If no match, return None
    m = regex.search(file_name)
    if not m:
        return False

    return True


def get_match(regex: re.Pattern[str], file_name) -> re.Match[str] | None:
    # Match file name. If no match, return None
    return regex.search(file_name)
