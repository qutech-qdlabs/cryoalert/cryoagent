import logging
import re
from copy import deepcopy
from functools import partial

import prometheus_client
from prometheus_client import Gauge, Info
from sqlalchemy import create_engine
from sqlalchemy.orm import Session

import cryo_agent
from cryo_agent.logic.collectors import (
    get_collector,
    get_collectors,
    get_data_group,
    get_data_point,
)
from cryo_agent.logic.settings import get_fridge_name
from cryo_agent.models import AlertDefinition, LatestData
from cryo_agent.utils.misc import recursive_get
from cryo_agent.utils.units import convert_value_unit, get_base_unit

LOGGER = logging.getLogger(__name__)


ALERT_METRICS: dict[str, Gauge] = {}
METRICS: dict[tuple[str, str, str, str], Gauge] = {}


METRIC_INVALID_TOKENS_REGEX = re.compile(r"[^a-zA-Z0-9_:]")


def create_metric_name(var_name: str, unit: str) -> str:
    # Forge into correct format
    name = f"cryoalert_{var_name}"
    if unit:
        name += f"_{unit}"

    # Clean up name to be sure a valid metric name is generated
    name = METRIC_INVALID_TOKENS_REGEX.sub("_", name)

    # Finally, return the parsed name
    return name


def get_data(var_name: str, sqlite_path: str, collector_name: str, group_label: str) -> float:
    LOGGER.debug(f"Executing 'get_data' for {var_name=}")
    with Session(create_engine(sqlite_path)) as session:
        collector = get_collector(session, collector_name, include_base=True)
        assert collector is not None

        data_group = get_data_group(session, collector.id, group_label)
        assert data_group is not None

        data_point = get_data_point(session, data_group.id, var_name)
        assert data_point is not None

        if data_point is None:
            # This can happen when a data_point was deleted in a collector
            fridge_name = get_fridge_name(session)
            old_metric = METRICS.pop((fridge_name, collector_name, group_label, var_name), None)
            if (
                old_metric
                and old_metric in prometheus_client.REGISTRY._collector_to_names  # pylint: disable=protected-access
            ):
                prometheus_client.REGISTRY.unregister(old_metric)

            return float("NaN")

        latest_data: LatestData = data_point.latest_data

    # Return the desired data
    if latest_data is None or latest_data.value is None:
        return float("NaN")

    val, base_unit = convert_value_unit(latest_data.value, data_point.unit)
    LOGGER.debug(f"Found {var_name} in latest values, value is: {val} [{base_unit}]")
    return val


def create_or_update_metric(
    name: str,
    gauge_name: str,
    sqlite_path: str,
    fridge_name: str,
    collector_name: str,
    group_label: str,
    descr: str = "",
):
    metric = METRICS.get((fridge_name, collector_name, group_label, name), None)
    if metric is None:
        if gauge_name not in prometheus_client.REGISTRY._names_to_collectors:  # pylint: disable=protected-access
            metric = Gauge(gauge_name, descr, ["fridge", "collector", "group_label"])
        else:
            metric = prometheus_client.REGISTRY._names_to_collectors[gauge_name]  # type: ignore # pylint: disable=protected-access

        METRICS[(fridge_name, collector_name, group_label, name)] = metric  # type: ignore

    (
        metric.labels(fridge=fridge_name, collector=collector_name, group_label=group_label).set_function(  # type: ignore
            partial(get_data, name, sqlite_path, collector_name, group_label)
        )
    )


def create_alert_metrics(alert_name: str, sqlite_path: str, fridge_name: str):
    # Get alert definition from the database
    with Session(create_engine(sqlite_path)) as session:
        alert_def: AlertDefinition = (
            session.query(AlertDefinition).filter(AlertDefinition.alert_name == alert_name).first()
        )

    # Construct alert metric name
    unit = get_base_unit(alert_def.settings["unit"])
    metric_name = f"cryoalert_{alert_name}_settings"

    # Construct the metric
    metric = Gauge(metric_name, "", ["fridge", "parameter"])
    ALERT_METRICS[alert_name] = metric

    # Example
    metric.labels(fridge=fridge_name, parameter="enabled").set_function(
        partial(get_alert_definition_data, alert_name, sqlite_path, ["enabled"])
    )
    if alert_def.settings.get("bounds", None):
        if "lower" in alert_def.settings["bounds"]:
            metric.labels(fridge=fridge_name, parameter="bounds_lower_enabled").set_function(
                partial(get_alert_definition_data, alert_name, sqlite_path, ["bounds", "lower", "enabled"])
            )

            label = f"bounds_lower_value_{unit}" if unit else "bounds_lower_value"
            metric.labels(fridge=fridge_name, parameter=label).set_function(
                partial(
                    get_alert_definition_data, alert_name, sqlite_path, ["bounds", "lower", "value"], check_unit=True
                )
            )
        if "upper" in alert_def.settings["bounds"]:
            metric.labels(fridge=fridge_name, parameter="bounds_upper_enabled").set_function(
                partial(get_alert_definition_data, alert_name, sqlite_path, ["bounds", "upper", "enabled"])
            )

            label = f"bounds_upper_value_{unit}" if unit else "bounds_upper_value"
            metric.labels(fridge=fridge_name, parameter=label).set_function(
                partial(
                    get_alert_definition_data, alert_name, sqlite_path, ["bounds", "upper", "value"], check_unit=True
                )
            )


def get_alert_definition_data(
    alert_name, sqlite_path: str, nested_params: list[str], check_unit: bool = False
) -> float:
    # Get alert definition from the database
    with Session(create_engine(sqlite_path)) as session:
        alert_def: AlertDefinition = (
            session.query(AlertDefinition).filter(AlertDefinition.alert_name == alert_name).first()
        )

    # If an alert has been removed, it can still happen that the metric still exists.
    # In that case, unregister the metric and return NaN for now
    if alert_def is None:
        unused_metric = ALERT_METRICS[alert_name]
        if unused_metric in prometheus_client.REGISTRY._collector_to_names:  # pylint: disable=protected-access
            prometheus_client.REGISTRY.unregister(unused_metric)
        return float("NaN")

    # Get the correct value
    nested_parameters_copy = deepcopy(nested_params)
    if alert_def.values:
        data = alert_def.values
    else:
        nested_parameters_copy[-1] = f"default_{nested_parameters_copy[-1]}"
        data = alert_def.settings

    LOGGER.debug(f"Retrieving values from {alert_name} with {nested_parameters_copy=}")
    if check_unit:
        conv_val, _ = convert_value_unit(recursive_get(data, *nested_parameters_copy), unit=alert_def.settings["unit"])
    else:
        conv_val = recursive_get(data, *nested_parameters_copy)

    return conv_val


def create_initializer_metric(sqlite_path: str, fridge_name: str):
    # Create initializer metric
    metric = Gauge(create_metric_name("initializer", ""), "Used to initialize dynamic metrics. Ignore")
    metric.set_function(partial(_add_missing_metrics, sqlite_path, fridge_name))


def _add_missing_metrics(sqlite_path: str, fridge_name: str):
    # Get data from database
    # (fridge_name, collector_name, group_label, variable_name) -> metric
    with Session(create_engine(sqlite_path)) as session:
        collectors = get_collectors(session, include_base=True)
        for collector in collectors:
            for data_group in collector.data_groups:
                for data_point in data_group.data_points:
                    metric_id = (
                        fridge_name,
                        collector.collector_name,
                        data_group.group_label,
                        data_point.variable_name,
                    )
                    if metric_id not in METRICS:
                        gauge_name = create_metric_name(data_point.variable_name, get_base_unit(data_point.unit))
                        LOGGER.debug(f"Create missing metric: {gauge_name}")
                        create_or_update_metric(
                            data_point.variable_name,
                            gauge_name,
                            sqlite_path=sqlite_path,
                            fridge_name=fridge_name,
                            collector_name=collector.collector_name,
                            group_label=data_group.group_label,
                        )

        # Check for missing alert definitions
        alert_defs: list[AlertDefinition] = session.query(AlertDefinition).all()
        for missing_alert_name in [
            alert_def.alert_name for alert_def in alert_defs if alert_def.alert_name not in ALERT_METRICS
        ]:
            LOGGER.debug(f"Create missing alert setting metric: {missing_alert_name}")
            create_alert_metrics(missing_alert_name, sqlite_path, fridge_name)

    return float("NaN")


def initialize_metrics(sqlite_path: str, fridge_name: str):
    # Unregister default collectors
    prometheus_client.REGISTRY.unregister(prometheus_client.GC_COLLECTOR)
    prometheus_client.REGISTRY.unregister(prometheus_client.PLATFORM_COLLECTOR)
    prometheus_client.REGISTRY.unregister(prometheus_client.PROCESS_COLLECTOR)

    # Because this is a chicken and egg problem, have one metric that is always defined
    create_initializer_metric(sqlite_path=sqlite_path, fridge_name=fridge_name)

    # Create info metrics for meta-data
    i = Info("metadata", "Meta-data about the cryo-agent")
    i.info({"version": cryo_agent.__version__, "fridge": fridge_name})
