import logging

from cryo_agent.collectors.plugins.core import BaseCollectorPlugin
from cryo_agent.logic.base import write_latest_datapoints
from cryo_agent.utils.types import DataGroupSettings

from .parsers.dummy_parser import get_dummy_data

LOGGER = logging.getLogger(__name__)


class CollectorPlugin(BaseCollectorPlugin):
    def _loop_data_group(self, session, data_group_info: DataGroupSettings):

        latest_data_points_data = {}
        for data_point_info in data_group_info["data_points"]:
            latest_data_points_data.update(
                {
                    data_point_info["id"]: self._loop_data_point(session, data_point_info),
                }
            )

        # Add random datapoints and commit it
        write_latest_datapoints(session, latest_data_points_data)

    def _loop_data_point(self, session, data_point_info: dict):
        # Get dummy datapoint
        return get_dummy_data()

    def collect(self, session):
        LOGGER.debug("Pretending to collect data")

        for data_group_info in self.data_groups:
            self._loop_data_group(session, data_group_info)
