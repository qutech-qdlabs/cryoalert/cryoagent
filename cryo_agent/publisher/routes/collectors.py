import logging
from copy import copy, deepcopy
from typing import Any

from flask import (
    Blueprint,
    current_app,
    flash,
    redirect,
    render_template,
    request,
    url_for,
)
from sqlalchemy.orm import Session

from cryo_agent.logic.collectors import (
    add_data_group,
    add_data_point,
    add_new_collector,
    delete_collector,
    delete_data_group,
    delete_data_point,
    get_collector,
    get_collectors,
    get_data_group,
    get_data_point,
    is_collector_name_used,
    update_collector,
    update_data_group,
    update_data_point,
)
from cryo_agent.publisher import tasks
from cryo_agent.publisher.db import get_engine
from cryo_agent.utils.exceptions import ObjectNotFoundException
from cryo_agent.utils.types import PluginData

LOGGER = logging.getLogger(__name__)

bp = Blueprint("collectors", __name__, url_prefix="/collectors")


@bp.route("/")
def index():
    with Session(get_engine()) as session:
        collectors = get_collectors(session)
        return render_template(
            "collectors/index.html",
            collectors=collectors,
        )


@bp.route("/add/", methods=["GET", "POST"])
def add_collector():
    registered_plugins: dict[str, dict] = deepcopy(current_app.config.get("REGISTERED_PLUGINS", {}))
    assert isinstance(registered_plugins, dict)

    if request.method == "POST":
        with Session(get_engine()) as session:
            LOGGER.debug("adding new collector!")
            data = request.form
            collector_name = data.get("collector_name")
            collector_class_alias = data.get("collector_class")
            collector_name_used = False

            # Flash messages that entries are requried
            if not collector_name:
                flash("Collector name is requried")
            if not collector_class_alias:
                flash("Collector class is required")
            if collector_name and is_collector_name_used(session, collector_name):
                collector_name_used = True
            if collector_name_used:
                flash(f"Collector name `{collector_name}` is already used")

            if all([collector_name, collector_class_alias, not collector_name_used]):
                LOGGER.debug("Creating new collector!")
                # NOTE: no need to start collector yet, as it is not enabled after creation
                add_new_collector(session, collector_name, collector_class_alias)
                return redirect(url_for("collectors.details_collector", collector_name=collector_name))

    return render_template(
        "collectors/add_collector.html",
        registered_plugins=registered_plugins,
        external_plugins_present=any([not reg_plugin["internal"] for reg_plugin in registered_plugins.values()]),
    )


@bp.route("/<string:collector_name>/", methods=["GET", "POST"])
def details_collector(collector_name: str):
    registered_plugins: dict[str, dict] = deepcopy(current_app.config.get("REGISTERED_PLUGINS", {}))
    assert isinstance(registered_plugins, dict)
    with Session(get_engine()) as session:
        collector = get_collector(session, collector_name)
        if collector is None:
            raise ObjectNotFoundException(f"Could not find collector with name: {collector_name}")

        collector_config = registered_plugins[collector.collector_class]

        if request.method == "POST" and ("save" in request.form or "save_continue" in request.form):
            data: dict[str, Any] = {
                key: item for key, item in request.form.items() if key not in ["save", "save_continue"]
            }
            data["enabled"] = True if "enabled" in data else False

            # Get possible error messages from the collector class
            error_messages = collector_config["plugin_class"].validate_collector_config(
                data,
                collector_config.get("config", {}).get("settings", {}),
            )

            if len(error_messages) != 0:
                for error_message in error_messages:
                    flash(error_message)

                return render_template(
                    "collectors/details.html",
                    collector_name=collector_name,
                    collector_config=collector_config,
                    collector=collector.serialize(),
                )

            # Update settings immediately with open session
            # NOTE, collector_settings is now updated with the newest values
            prev_enabled = copy(collector.enabled)
            update_collector(session, collector_name, data)

            # Enabling collectors is handled by the cryo_agent_process
            if prev_enabled and not data["enabled"]:
                tasks.disable_collector(collector_name)
            elif not prev_enabled and data["enabled"]:
                tasks.enable_collector(collector_name)

            if prev_enabled and data["enabled"]:
                # Only update collector settings when the collector is enabled and remains enabled
                # If any other settings have been changed, send task to the corresponding collector process
                tasks.update_collector_settings(collector_name, {})

            if "save" in request.form:
                return redirect(url_for("collectors.index"))
            if "save_continue" in request.form:
                return redirect(url_for("collectors.details_collector", collector_name=collector_name))

        elif request.method == "POST" and "delete" in request.form:
            delete_collector(session, collector_name)
            tasks.disable_collector(collector_name)
            return redirect(url_for("collectors.index"))

        return render_template(
            "collectors/details.html",
            collector_name=collector_name,
            collector_config=collector_config,
            collector=collector.serialize(),
        )


@bp.route("/<string:collector_name>/datagroups/")
def detail_collector_datagroup(collector_name: str):
    """
    Simple redirect path to make breadcrumbs fully functional
    """
    return redirect(url_for("collectors.details_collector", collector_name=collector_name))


@bp.route("/<string:collector_name>/datagroups/add/", methods=["GET", "POST"])
def add_datagroup(collector_name: str):
    with Session(get_engine()) as session:
        collector = get_collector(session, collector_name)
        if collector is None:
            raise ObjectNotFoundException(f"Could not find collector with name: {collector_name}")

        collector_config = deepcopy(current_app.config.get("REGISTERED_PLUGINS", {}).get(collector.collector_class))

        if request.method == "POST":
            data = {**request.form}
            group_label = data.pop("group_label")

            # Get possible error messages from the collector class
            error_messages = collector_config["plugin_class"].validate_datagroup_config(
                data,
                collector_config.get("config", {}).get("settings", {}),
                key_exceptions=["group_label"],  # Needed, because it was popped earlier
            )
            if len(error_messages) == 0:
                add_data_group(session, collector_name, group_label, data)
                return redirect(url_for("collectors.details_collector", collector_name=collector_name))

            # If error messages are present, show them
            for error_msg in error_messages:
                flash(error_msg)

        return render_template(
            "collectors/details-data-groups-add.html",
            collector_name=collector_name,
            collector_config=collector_config,
        )


@bp.route("/<string:collector_name>/datagroups/<string:group_label>/", methods=["GET", "POST"])
def details_datagroup(collector_name: str, group_label: str):
    with Session(get_engine()) as session:
        collector = get_collector(session, collector_name)
        if collector is None:
            raise ObjectNotFoundException(f"Could not find collector with name: {collector_name}")

        data_group = get_data_group(session, collector.id, group_label)
        if data_group is None:
            raise ObjectNotFoundException(f"Could not find data_group with name: {group_label}")

        collector_config = deepcopy(current_app.config.get("REGISTERED_PLUGINS", {}).get(collector.collector_class))
        data_groups_config = collector_config["config"].get("settings", {}).get("data_groups", {})
        data_points_config = collector_config["config"].get("settings", {}).get("data_points", {})

        if request.method == "POST":
            if "save" in request.form:
                data = {key: item for key, item in request.form.items() if key != "save"}

                # Get possible error messages from the collector class
                error_messages = collector_config["plugin_class"].validate_datagroup_config(
                    data,
                    collector_config.get("config", {}).get("settings", {}),
                    key_exceptions=["group_label"],
                )

                if len(error_messages) == 0:
                    update_data_group(session, collector_name, group_label, data)
                    tasks.update_collector_settings(collector_name, {})
                    return redirect(url_for("collectors.details_collector", collector_name=collector_name))

                # If error messages are present, show them
                for error_msg in error_messages:
                    flash(error_msg)
                    return render_template(
                        "collectors/details-data-group.html",
                        collector_name=collector_name,
                        group_label=group_label,
                        data_group=data_group.serialize(),
                        data_groups_config=data_groups_config,
                        data_points_config=data_points_config,
                    )

            elif "delete" in request.form:
                delete_data_group(session, collector.id, group_label)
                tasks.update_collector_settings(collector_name, {})
                return redirect(url_for("collectors.details_collector", collector_name=collector_name))

        return render_template(
            "collectors/details-data-group.html",
            collector_name=collector_name,
            group_label=group_label,
            data_group=data_group.serialize(),
            data_groups_config=data_groups_config,
            data_points_config=data_points_config,
        )


@bp.route("/<string:collector_name>/datagroups/<string:group_label>/datapoints/")
def detail_collector_datapoint(collector_name: str, group_label: str):
    """
    Simple redirect path to make breadcrumbs fully functional
    """
    return redirect(
        url_for(
            "collectors.details_datagroup",
            collector_name=collector_name,
            group_label=group_label,
        )
    )


@bp.route("/<string:collector_name>/datagroups/<string:group_label>/datapoints/add/", methods=["GET", "POST"])
def add_datapoint(collector_name: str, group_label: str):
    with Session(get_engine()) as session:
        collector = get_collector(session, collector_name)
        if collector is None:
            raise ObjectNotFoundException(f"Could not find collector with name: {collector_name}")

        collector_config: PluginData = deepcopy(
            current_app.config.get("REGISTERED_PLUGINS", {}).get(collector.collector_class)
        )
        assert isinstance(collector_config, dict)

        if request.method == "POST":
            data = {**request.form}

            # Get possible error messages from the collector class
            error_messages = collector_config["plugin_class"].validate_datapoint_config(
                data,
                collector_config["config"].get("settings", {}),
            )

            if len(error_messages) == 0:
                variable_name = data.pop("variable_name")
                unit = data.pop("unit", "")
                add_data_point(session, collector_name, group_label, variable_name, unit, data)

                # Update the collector settings at this point, to automatically make use of the new datapoint
                tasks.update_collector_settings(collector_name, {})

                return redirect(
                    url_for(
                        "collectors.details_datagroup",
                        collector_name=collector_name,
                        group_label=group_label,
                    )
                )

            # If error messages are present, show them
            for error_msg in error_messages:
                flash(error_msg)

        return render_template(
            "collectors/details-data-points-add.html",
            collector_name=collector_name,
            group_label=group_label,
            collector_config=collector_config,
        )


@bp.route(
    "/<string:collector_name>/datagroups/<string:group_label>/datapoints/<string:variable_name>/",
    methods=["GET", "POST"],
)
def details_datapoint(collector_name: str, group_label: str, variable_name: str):
    with Session(get_engine()) as session:
        collector = get_collector(session, collector_name)
        if collector is None:
            raise ObjectNotFoundException(f"Could not find collector with name: {collector_name}")

        data_group = get_data_group(session, collector.id, group_label)
        if data_group is None:
            raise ObjectNotFoundException(f"Could not find data_group with name: {group_label}")

        data_point = get_data_point(session, data_group.id, variable_name)
        if data_point is None:
            raise ObjectNotFoundException(f"Could not find data_point with name: {variable_name}")

        collector_config = deepcopy(current_app.config.get("REGISTERED_PLUGINS", {}).get(collector.collector_class))
        data_points_config = collector_config["config"].get("settings", {}).get("data_points", {})

        if request.method == "POST":
            if "save" in request.form:
                data = {key: item for key, item in request.form.items() if key != "save"}
                # Get possible error messages from the collector class
                error_messages = collector_config["plugin_class"].validate_datapoint_config(
                    data,
                    collector_config.get("config", {}).get("settings", {}),
                    key_exceptions=["variable_name"],
                )

                if len(error_messages) == 0:
                    update_data_point(session, data_group.id, variable_name, data)
                    tasks.update_collector_settings(collector_name, {})
                    return redirect(
                        url_for(
                            "collectors.details_datagroup",
                            collector_name=collector_name,
                            group_label=group_label,
                        )
                    )

                # If error messages are present, show them
                for error_msg in error_messages:
                    flash(error_msg)
                    return render_template(
                        "collectors/details-data-point.html",
                        collector_name=collector_name,
                        group_label=group_label,
                        variable_name=variable_name,
                        data_point=data_point.serialize(),
                        data_points_config=data_points_config,
                    )

            elif "delete" in request.form:
                delete_data_point(session, data_group.id, variable_name)
                tasks.update_collector_settings(collector_name, {})
                return redirect(
                    url_for(
                        "collectors.details_datagroup",
                        collector_name=collector_name,
                        group_label=group_label,
                    )
                )

        return render_template(
            "collectors/details-data-point.html",
            collector_name=collector_name,
            group_label=group_label,
            variable_name=variable_name,
            data_point=data_point.serialize(),
            data_points_config=data_points_config,
        )
