import logging
from datetime import datetime

from sqlalchemy import desc
from sqlalchemy.orm import Session

from cryo_agent.models import LogBookEntry

LOGGER = logging.getLogger(__name__)


def get_logbook_entry(session: Session, entry_id: int) -> LogBookEntry:
    return session.query(LogBookEntry).where(LogBookEntry.id == entry_id).first()


def get_logbook_entries(session: Session) -> list[LogBookEntry]:
    return session.query(LogBookEntry).order_by(desc(LogBookEntry.timestamp)).all()


def write_new_logbook_entry(session: Session, username: str, action: str, description: str) -> None:
    # Construct new entry
    lbe = LogBookEntry(
        timestamp=datetime.now().astimezone(),
        username=username,
        action=action,
        description=description,
    )

    # Add to session and commit
    session.add(lbe)
    session.commit()


def modify_logbook_entry(session: Session, entry_id: int, username: str, action: str, description: str) -> None:
    # Get logbook entry
    lbe = get_logbook_entry(session, entry_id)

    # Update the entry
    lbe.username = username
    lbe.action = action
    lbe.description = description

    # Commit
    session.commit()
