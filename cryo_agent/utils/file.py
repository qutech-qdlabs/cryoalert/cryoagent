from typing import BinaryIO


def read_last_line(file_handler: BinaryIO) -> bytes:
    res = last_n_lines(file_handler, 1)
    if len(res) >= 1:
        return last_n_lines(file_handler, 1)[-1]
    else:
        return b""


def last_n_lines(file_handler: BinaryIO, num_lines: int) -> list[bytes]:
    """
    Exponential search for the last N lines of a file. The file must be opened
    in binary mode.

    Args:
        file_handler BinaryIO: Opened binary file
        num_lines (int): Number of lines to be returned

    Returns:
        list: List of lines to be returned
    """
    assert num_lines >= 0
    pos = num_lines + 1

    lines: list[bytes] = []
    while len(lines) <= num_lines:
        try:
            file_handler.seek(-pos, 2)
        except IOError:
            file_handler.seek(0)
            break
        finally:
            lines = list(file_handler)

        # increasing value of variable exponentially
        pos *= 2

    return lines[-num_lines:]
