from flask import current_app, g
from sqlalchemy import create_engine, event
from sqlalchemy.engine import Engine


def get_engine():
    if "engine" not in g:
        g.engine = create_engine(current_app.config["SQLITE_PATH"])

    return g.engine


@event.listens_for(Engine, "connect")
def set_sqlite_pragma(dbapi_connection, _):
    """
    With SQLite, Operations involving ForeignKeys need to be enabled on a per connection basis.
    This function enables to ForeignKeys pragma for connections
    see https://docs.sqlalchemy.org/en/20/dialects/sqlite.html#foreign-key-support
    """
    cursor = dbapi_connection.cursor()
    cursor.execute("PRAGMA foreign_keys=ON")
    cursor.close()
