# Keithley Temperature readout Plugin

## Keithley models

While many Keithley multimeters have a similar interface, not all of them are equal. For this reason, this plugin supports multiple Keithley multimeter versions. The relevant difference lies in the structure of the response data. While common ground could be found, it is simpler, more robust, and more future proof to atleast have the possibility of allowing differences between the different version numbers. To extract the relevant data from the response, a regex pattern is used.

The different models that are supported and their regex patterns are:

- 199: `^[a-zA-Z]*(?P<float>(\+|\-)?\d+(.\d+)?((E|e)(\+|\-)\d+)?)$`
- 2700: `^(?P<float>(\+|\-)?\d+(.\d+)?((E|e)(\+|\-)\d+)?)`
- -1 (default): `?P<float>(\+|\-)?\d+(.\d+)?((E|e)(\+|\-)\d+)?)`

## Conversion methods

### None

This conversion method does exactly what it says; nothing. The collected value will not be transformed.

### NTC Thermistor model

```math
1/T = 1/T0 + (1/b)*ln(R/R0)
```

where R0 is the resistance at temperature T0. The parameter B has units of Kelvin. The conversion data JSON should look like:

```json
{
    "T0": <value>,
    "b": <value>,
    "R0": <value>
}
```

To estimate the parameters for this conversion method, a simple script using scipy (not installed by cryo-agent) can be used. For example

```python
from scipy.optimize import curve_fit


R_cal = np.array([414, 432, 558])
T_cal = np.array([298, 77, 4.2])

def ntc_thermistor(r, T0, b, R0):
    return 1/T0 + (1/b) * np.log(r / R0)

popt_oneK, pcov_oneK = curve_fit(ntc_thermistor, R_cal, 1 / T_cal)
T0, b, R0 = popt
```

### Linear Interpolation

Given the provided input data, the collected data is transformed by means of linear interpolation. The conversion data JSON should look like:

```JSON
{
    "R": [<input1>,...,<inputN>],
    "T": [<output1>,...,<outputN>]
}
```

Note that the `R` array needs to be in monotonically increasing order.

### Allen-Barry resistor

```math
T = exp(ln((ln(R)-a)/b)/(-c))
```

The conversion data JSON should look like:

```json
{
    "a": <value>,
    "b": <value>,
    "c": <value>
}
```

To estimate the parameters for this conversion method, a simple script using scipy (not installed by cryo-agent) can be used. For example

```python
from scipy.optimize import curve_fit


R_cal = np.array([414, 432, 558])
T_cal = np.array([298, 77, 4.2])

def allen_barry_resistor(r, a, b, c):
    return np.exp((np.log((np.log(r) - a) / (b))) / (-c))

popt, pcov = curve_fit(allen_barry_resistor, R_cal, T_cal)
a,b,c = popt
```

### Steinhart-Hart model

```math
1/T = a + b*ln(R) + c*ln(R)^3
```

The conversion data JSON should look like:

```json
{
    "a": <value>,
    "b": <value>,
    "c": <value>
}
```

To estimate the parameters for this conversion method, a simple script using scipy (not installed by cryo-agent) can be used. For example

```python
from scipy.optimize import curve_fit

R_cal = np.array([414, 432, 558])
T_cal = np.array([298, 77, 4.2])

def steinhart_eq(r, a, b, c):
    return a + b * np.log(r) + c * (np.log(r) ** 3)

popt, pcov = curve_fit(steinhart_eq, R_cal, 1 / T_cal)
a,b,c = popt
```

## Notes

This collector can only connect to a single device. For the most common use case of reading a resistance or temperature from a multimeter, it is normal to have only a single data group with a single data point.
