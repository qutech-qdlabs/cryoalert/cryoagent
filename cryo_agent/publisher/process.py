import logging
import multiprocessing as mp
from time import sleep

from waitress import serve

from cryo_agent.metrics import initialize_metrics
from cryo_agent.models import WebSettings
from cryo_agent.publisher.app import create_app
from cryo_agent.utils.base_process import BaseProcess
from cryo_agent.utils.logging import init_process_logging
from cryo_agent.utils.tasks import TaskRouter
from cryo_agent.utils.types import PluginData

LOGGER = logging.getLogger(__name__)


class WebProcess(BaseProcess):
    """
    A multiprocess process for handling the webserver part of the cryo-agent
    """

    def __init__(
        self,
        sqlite_path: str,
        data_source_name: str,
        web_settings: WebSettings,
        log_queue: mp.Queue,
        task_router: TaskRouter,
        registered_plugins: dict[str, PluginData],
        activate_metrics: bool = False,
    ):
        super().__init__()

        self._sqlite_path = sqlite_path
        self.data_source_name = data_source_name
        self.config = web_settings
        self.log_level: str = web_settings.log_level
        self.log_queue = log_queue
        self.task_router = task_router
        self._activate_metrics = activate_metrics
        self.registered_plugins = registered_plugins

    def run(self) -> None:
        """
        Entrypoint of process to run the webserver
        """
        # Init logging again in new process
        init_process_logging(log_queue=self.log_queue, log_level=self.log_level.upper())

        # Initialize prometheus metrics
        LOGGER.debug("Initializing prometheus metrics")
        initialize_metrics(sqlite_path=self._sqlite_path, fridge_name=self.data_source_name)

        # Create app
        app = create_app(
            sqlite_path=self._sqlite_path,
            data_source_name=self.data_source_name,
            config=self.config,
            task_router=self.task_router,
            registered_plugins=self.registered_plugins,
            activate_metrics=self._activate_metrics,
        )

        # Run web app server untill cancelled
        LOGGER.info("Started Publisher Server")
        while not self.is_stopped:
            try:
                serve(app, port=int(self.config.port), threads=8)
            except Exception:
                pass
            finally:
                LOGGER.info("Stopped Publisher Server")
                try:
                    sleep(1)
                except Exception:
                    pass
