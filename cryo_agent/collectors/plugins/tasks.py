import logging
from enum import IntEnum, auto

from cryo_agent.utils.decorators import task

LOGGER = logging.getLogger(__name__)


class Tasks(IntEnum):
    PRINT = auto()
    UPDATE_SETTINGS = auto()


@task("plugin", Tasks.PRINT)
def task_handler_print(_, task_data: dict) -> None:
    print(task_data)


@task("plugin", Tasks.UPDATE_SETTINGS)
def task_update_settings(collector_process, _: dict):
    # Trigger the collector to refresh its config
    LOGGER.debug("Refreshing collector config")
    collector_process.refresh_config()
