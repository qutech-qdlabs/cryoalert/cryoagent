from flask import Blueprint, redirect, render_template, request, url_for
from sqlalchemy.orm import Session

from cryo_agent.logic.base import (
    delete_latest_data,
    get_all_latest_datapoints,
    get_latest_data,
)
from cryo_agent.publisher.db import get_engine

bp = Blueprint("latest_data", __name__, url_prefix="/latest_data")


@bp.route("/")
def latest_data_view():
    with Session(get_engine()) as session:
        datapoints = get_all_latest_datapoints(session)

    return render_template(
        "latest_data/latest_data_overview.html",
        latest_data_points=datapoints,
    )


@bp.route("/modify/<int:data_point_id>/", methods=["GET", "POST"])
def modify(data_point_id: int):
    with Session(get_engine()) as session:
        if request.method == "POST":
            delete_latest_data(session, data_point_id)
            return redirect(url_for("latest_data.latest_data_view"))

        # Get the requested object
        latest_data = get_latest_data(session, data_point_id)

        return render_template(
            "latest_data/latest_data_modify.html",
            dp_data=latest_data.serialize(),
        )
