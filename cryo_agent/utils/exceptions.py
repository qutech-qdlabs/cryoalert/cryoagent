from werkzeug.exceptions import HTTPException


class ObjectNotFoundException(HTTPException):
    """Custom Exception for when a queried object is not found"""

    code = 404
    description = "Object not found"


class InvalidUnitException(Exception):
    """Custom Exception for when a unit is invalid"""


class ExternalPluginAlreadyExistsException(Exception):
    """
    Custom Exception for when an external plugin already exists during creation of it
    """
