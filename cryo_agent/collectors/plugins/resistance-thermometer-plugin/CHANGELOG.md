# Changelog

All notable changes to this collector plugin will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.1.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [0.2.0] - 2024-07-12

### Added

- Added base configuration option to select model number of the Keithley

### Fixed

### Changed

### Removed

## [0.1.0] - 2024-05-24

### Added

### Fixed

### Changed

- Made calculation of temperatures more robust when handling incorrect/unrealistic resistance values (such as zero resistance)

### Removed

## [0.0.1] - 2024-03-26

### Added

- Initial release

### Fixed

### Changed

### Removed
