import logging
from datetime import datetime

from sqlalchemy.orm import Session

import cryo_agent
from cryo_agent.collectors.plugins.core import BaseCollectorPlugin
from cryo_agent.logic.base import write_latest_datapoints
from cryo_agent.utils.tcp_client import TcpRemoteClient
from cryo_agent.utils.types import CollectorDataGroupDict, DataPointData

LOGGER = logging.getLogger(__name__)


def _parse_response(val: str) -> float:
    if "," in val:
        return val.split(",")[0]
    else:
        return float(val)


class CollectorPlugin(BaseCollectorPlugin):
    """
    A multiprocess process for collecting data
    """

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.tcp_client = TcpRemoteClient(*self.remote_address)

    def send(self, cmd: str) -> str | None:
        # Add connector name if needed
        if self.connector_name:
            cmd = f"@{self.connector_name}#{cmd}"

        # Add mandatory newline
        cmd = cmd + "\n"

        # Send through tcp client
        res = self.tcp_client.send(cmd.encode())
        if res is None:
            return None

        # Decode response and strip the (CR)LF
        return res.decode().strip()

    @property
    def remote_address(self) -> tuple[str, int]:
        return (self.settings.get("remote_host", "localhost"), int(self.settings.get("remote_port", 33576)))

    @property
    def connector_name(self) -> str:
        return self.settings.get("connector_name")

    def collect_datagroup(self, data_group_info: CollectorDataGroupDict) -> dict[int, DataPointData]:
        """
        Internal method to process for a specific data group
        """

        # Extract group meta information (everything except the datapoints information)
        # This is good practice to keep track of where each datapoint came from and from which version of cryo-agent
        group_meta: dict = {**data_group_info}  # convert to raw dict to do further manipulation
        group_meta.pop("data_points")
        group_meta.update({"agent_version": cryo_agent.__version__})  # add version for compatability

        # Get data for this datagroup here
        ret = {}
        for data_point in data_group_info.get("data_points", []):
            command_uri = data_point["command_uri"]
            latest_data = self.collect_datapoint(command_uri)
            if latest_data:
                latest_data.update({"meta": {**group_meta, "command_uri": command_uri}})
                ret.update({data_point["id"]: latest_data})
            else:
                LOGGER.debug(f"No command_uri was found for data_group={data_group_info.get('group_label')}")

        return ret

    def collect_datapoint(self, command_uri: str):
        resp = self.send(command_uri)
        if resp is None:
            return {}

        try:
            val = _parse_response(resp)
        except ValueError:
            LOGGER.warning(f"No value found for {command_uri}")
            val = float("nan")

        return {
            "timestamp": datetime.utcnow(),
            "value": val,
        }

    def collect(self, session: Session) -> None:
        """
        Entrypoint for single cycle of the collector
        """

        # Implement collection logic here
        latest_values: dict[int, DataPointData] = {}
        for data_group_info in self.data_groups:
            latest_values.update(self.collect_datagroup(data_group_info))

        # Write latest_values
        write_latest_datapoints(session, latest_values)
