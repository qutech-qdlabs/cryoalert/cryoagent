from functools import wraps
from typing import Callable

_task_dict: dict[str, dict[int, Callable]] = {}


def get_tasks_for_process(process_name: str):
    return _task_dict.get(process_name, {})


def task(process_name: str, task_literal: int):
    def decorator(func):
        if process_name not in _task_dict:
            _task_dict[process_name] = {}
        _task_dict[process_name].update({task_literal: func})

        @wraps(func)
        def wrapper(*args, **kwargs):
            return func(*args, **kwargs)

        return wrapper

    return decorator
