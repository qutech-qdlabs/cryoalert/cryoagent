import logging
import os
from typing import Any

from sqlalchemy.orm import Session

from cryo_agent.models import CryoAgentSettings, WebSettings

LOGGER = logging.getLogger(__name__)


def get_cryo_agent_settings(session: Session) -> CryoAgentSettings:
    return session.query(CryoAgentSettings).first()


def write_cryo_agent_settings(
    session: Session,
    fridge_name: str | None = None,
    log_level: str | None = None,
    alert_settings_upload_method: str | None = None,
    **_,
) -> dict:
    settings = get_cryo_agent_settings(session)
    changed_items = {}

    if settings is None:
        cas = CryoAgentSettings(
            fridge_name=fridge_name,
            log_level=log_level,
            alert_settings_upload_method=alert_settings_upload_method,
        )
        session.add(cas)
    else:
        if fridge_name and fridge_name != settings.fridge_name:
            settings.fridge_name = fridge_name
            changed_items.update({"fridge_name": fridge_name})

        if log_level and log_level != settings.log_level:
            settings.log_level = log_level
            changed_items.update({"log_level": log_level})

        if alert_settings_upload_method and alert_settings_upload_method != settings.alert_settings_upload_method:
            settings.alert_settings_upload_method = alert_settings_upload_method
            changed_items.update({"alert_settings_upload_method": alert_settings_upload_method})

    session.commit()
    return changed_items


def get_web_settings(session: Session) -> WebSettings:
    return session.query(WebSettings).first()


def write_web_settings(
    session: Session,
    port: int | None = None,
    log_level: str | None = None,
    **_,
) -> dict:
    web_settings = get_web_settings(session)
    changed_items: dict[str, Any] = {}

    if web_settings is None:
        assert port is not None, "Port should be provided"
        log_level = log_level if log_level else "INFO"
        new_web_settings = WebSettings(port=port, log_level=log_level, secret_key=os.urandom(32).hex())
        session.add(new_web_settings)
    else:
        if port and port != web_settings.port:
            web_settings.port = port
            changed_items.update({"port": port})

        if log_level and log_level != web_settings.log_level:
            web_settings.log_level = log_level
            changed_items.update({"log_level": log_level})

    session.commit()
    return changed_items


def get_fridge_name(session: Session) -> str:
    cryo_agent_settings = get_cryo_agent_settings(session)
    return cryo_agent_settings.fridge_name
