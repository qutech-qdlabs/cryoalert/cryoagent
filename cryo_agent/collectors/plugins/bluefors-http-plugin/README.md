# Bluefors-http-plugin

Some bluefors systems have a software package (version 2(?)) that has an HTTP API for reading and setting values remotely. The CryoAgent is only concerned with reading data. To make this as generic as possible, some additional configuration settings are needed for this collector class. Note that there may be an abitrary amount of data groups in the collector settings, and each data group can contain an arbitrary amount of data points.

## Plugin Specific Configuration

- **collector_settings**
  - **base_url** \<str>: URL of the Bluefors HTTP API ('<http://localhost:49099/values/>', for example, note the trailing slash)
  - **data_group**
    - **group_label** \<str>: Unique name (per collector) of the data group
    - **group_adress** \<str>: Adress of the group for getting all data of the group with a single request. For example: `driver.bdtc.data.channels`
    - *meta_data*: Any additional key-value-pair put here will be added to the meta data of the measurement. This can used for sorting later, for example using `group_label = temperatures`
    - **data_points**
      - **variable_name** \<str>: Name of the variable. If provided, this will override the function of the `variable_name_adress` and the datapoint will always have the `variable_name` as the name of the variable. If not provided, a name will be extracted from the adress provided by `variable_name_adress`
      - **unit** \<str>: Unit of the value. Leave empty or remove for unitless.
      - **data_adress** \<str>: Adress of the datapoint of interest relative to the `group_adress`
      - **valid_data_only** \<bool>: Use latest valid data only. If not available, use non-validated data
