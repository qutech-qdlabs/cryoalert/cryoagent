import os
import shutil
import sqlite3
from copy import copy
from datetime import datetime
from importlib import resources
from pathlib import Path
from typing import Optional

import click
from jinja2 import Environment, PackageLoader, Template
from pyfiglet import Figlet
from sqlalchemy import create_engine
from sqlalchemy.orm import Session

from cryo_agent.controller.factory import CryoAgentFactory
from cryo_agent.logic.collectors import setup_base_collector
from cryo_agent.logic.settings import (
    get_cryo_agent_settings,
    get_web_settings,
    write_cryo_agent_settings,
    write_web_settings,
)
from cryo_agent.models import Base
from cryo_agent.utils.exceptions import ExternalPluginAlreadyExistsException
from cryo_agent.utils.types import CryoAgentOptions

DEFAULT_DB_PATH = "sqlite:///cryo_agent.db"


def _print_logo():
    figure = Figlet(font="slant")
    print("=" * 30)
    print(figure.renderText("CryoAgent"))
    print("=" * 30)


def _changelog_data_format(my_dt: datetime):
    return my_dt.date().isoformat()


def _prompt_for_settings_and_create(session: Session):
    # Ask questions
    print("No config was found! Please answer some questions to get started.")
    fridge_name = click.prompt("What name does the fridge have?", type=str)
    log_level = click.prompt(
        "What log-level would you like to have set?", type=click.Choice(["DEBUG", "INFO", "WARNING", "ERROR"])
    )
    web_port = click.prompt("What TCP port would like to use for the web", type=int, default=8081)
    alert_settings_upload_method = click.prompt(
        "What method what you like to use for setting the alert settings?",
        type=click.Choice(["API", "File"]),
        default="API",
    )
    print("These were all the questions, thanks for answering :)")

    # Write generic cryo_agent settings
    write_cryo_agent_settings(
        session,
        fridge_name=fridge_name,
        log_level=log_level,
        alert_settings_upload_method=alert_settings_upload_method,
    )

    # Write web-settings
    write_web_settings(session, port=web_port, log_level=log_level)


def _setup():
    # Setup log folder if not exists
    logs_path = Path.cwd() / "logs"
    if not logs_path.exists():
        logs_path.mkdir()

    # Setup run script if not exists
    # Copy the start-up script(s)
    for file in resources.files("cryo_agent.run").iterdir():
        if file.is_file() and file.name.endswith(".bat") and not (Path.cwd() / file.name).exists():
            shutil.copyfile(file, Path.cwd() / file.name)


@click.group()
def cli():
    """
    CLI commands to run the CryoAgent
    """


@cli.command()
@click.option("--port", default=8081, type=int, help="The port that will used to start the web application")
@click.option(
    "--database_path",
    default=None,
    type=click.Path(exists=False),
    help="Path to the configuration database",
)
def change_port(port: int, database_path: Optional[str]):
    """
    CLI command to change the web port of the agent
    """
    if not database_path:
        database_path = DEFAULT_DB_PATH

    if database_path.startswith("sqlite:///"):
        database_path = database_path[10:]

    try:
        sqlite3.connect(f"file:{database_path}?mode=rw", uri=True)
    except sqlite3.OperationalError:
        print(f"Could not open database at `{database_path}`. Does it exist there?")
        return

    with Session(create_engine(f"sqlite:///{database_path}")) as session:
        web_settings = get_web_settings(session)
        old_port = copy(web_settings.port)
        write_web_settings(session, port=port, log_level=web_settings.log_level)
        print(f"Port of the web application changed from `{old_port}` to `{port}`")


def _render_template(template: Template, file_name: str | Path, tmpl_kwargs: dict) -> None:
    rendered_template = template.render(**tmpl_kwargs)
    with open(file_name, "w", encoding="utf-8") as template_file:
        template_file.write(rendered_template)


@cli.command()
@click.option("--plugin_name", type=click.STRING, prompt="What is the name of the plugin?")
@click.option("--description", type=click.STRING, prompt="Please describe the function of the plugin?")
@click.option("--creator", type=click.STRING, prompt="Who is the creator of this plugin?")
def create_plugin(plugin_name: str, description: str, creator: str):
    """
    CLI command for creating external plugins
    """
    plugin_alias = plugin_name.replace(" ", "-").lower()
    plugin_folder_name = plugin_alias
    if not plugin_folder_name.endswith("-plugin"):
        plugin_folder_name = f"{plugin_folder_name}-plugin"
    plugin_name_full = " ".join([word.title() for word in plugin_alias.replace("-", " ").split(" ")])

    # Get current working directory
    cwd = Path.cwd()

    # Get plugin folder if it does not exists
    plugin_folder_path = cwd / "plugins"
    if not os.path.exists(plugin_folder_path):
        os.mkdir(plugin_folder_path)

    # Create folder for plugin. Raise Error if it already exists
    plugin_path = plugin_folder_path / plugin_folder_name
    if not os.path.exists(plugin_path):
        os.mkdir(plugin_path)
    else:
        raise ExternalPluginAlreadyExistsException(f"An external plugin with name `{plugin_alias}` already exists!")

    # Get template environment
    env = Environment(loader=PackageLoader("cryo_agent.collectors.plugins", package_path="_plugin_template"))

    # Render and write CHANGELOG.md
    tmpl_kwargs = {"date": _changelog_data_format(datetime.now())}
    _render_template(env.get_template("CHANGELOG.template.md"), plugin_path / "CHANGELOG.md", tmpl_kwargs)

    # Render and write README.md
    tmpl_kwargs = {"plugin_name": plugin_name_full}
    _render_template(env.get_template("README.template.md"), plugin_path / "README.md", tmpl_kwargs)

    # Render and write plugin.yaml
    tmpl_kwargs = {
        "plugin_name_full": plugin_name_full,
        "plugin_alias": plugin_alias,
        "creator": creator,
        "description": description,
    }
    _render_template(env.get_template("plugin.template.yaml"), plugin_path / "plugin.yaml", tmpl_kwargs)

    # Render and write plugin.py
    tmpl_kwargs = {}
    _render_template(env.get_template("plugin.template.py"), plugin_path / "plugin.py", tmpl_kwargs)


@cli.command()
@click.option(
    "--activate-metrics/--deactivate-metrics",
    " /-M",
    default=True,
    help="Run with/without metric endpoint",
)
@click.option(
    "--activate-collectors/--deactivate-collectors",
    " /-C",
    default=True,
    help="Run with/without collectors defined in provided configuration",
)
@click.option(
    "--database_path",
    default=None,
    type=click.Path(exists=False),
    help="Path to the configuration database",
)
def start(activate_metrics: bool, activate_collectors: bool, database_path: Optional[str]):
    """
    CLI command to start the agent
    """
    # Print logo
    _print_logo()

    if not database_path:
        database_path = DEFAULT_DB_PATH

    # Initialize database if needed
    Base.metadata.create_all(create_engine(database_path))

    # If no settings exist, prompt and create
    with Session(create_engine(database_path)) as session:
        # If no general settings exists, ask for them
        if not get_cryo_agent_settings(session):
            _prompt_for_settings_and_create(session)

        # If no "base" collector exists, create it
        setup_base_collector(session)

    # Setup as needed
    _setup()

    # Create a cryoagent from the factory
    cryo_agent = CryoAgentFactory.create(
        database_path,
        options=CryoAgentOptions(
            activate_collectors=activate_collectors,
            activate_metrics=activate_metrics,
        ),
    )

    # Run the created CryoAgent
    cryo_agent.run()
