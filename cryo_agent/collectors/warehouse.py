import logging
from copy import copy

from cryo_agent.utils.base_process import BaseProcess

LOGGER = logging.getLogger(__name__)


class CollectorWarehouse:
    def __init__(self) -> None:
        self._collectors: dict[str, BaseProcess] = {}

    def add_collector(self, collector_name: str, collector_process: BaseProcess) -> None:
        if collector_name in self._collectors:
            LOGGER.warning(
                f"Tried to add a collector with name `{collector_name}`, but a collector with this name already existed. The new collector will therefore not be added"
            )
            return

        self._collectors.update({collector_name: collector_process})

    def start_collector(self, collector_name: str) -> None:
        if collector_name not in self._collectors:
            LOGGER.warning(
                f"Tried to start a collector with name `{collector_name}`, but no such collector has been defined."
            )
            return

        if self._collectors[collector_name].is_alive():
            # Collect is already active, so nothing to be done here
            return

        # Start collector and add collector_name to list of active_collectors
        self._collectors[collector_name].start()

    def stop_collector(self, collector_name: str) -> None:
        if collector_name not in self._collectors:
            # Unknown collector, there stopping it is meaningless
            return

        if not self._collectors[collector_name].is_alive():
            # Collector is not active, so nothing to be done here
            return

        # Only stop the process.
        # Removing it from the the list of collectors is part of the `join_collector` method
        self._collectors[collector_name].stop()

    def join_collector(self, collector_name: str) -> None:
        if collector_name not in self._collectors:
            # Unknown collector, there joining it is meaningless
            return

        # Pop the collector process from the dictionary and join it
        collector_process = self._collectors.pop(collector_name)
        collector_process.join()

    def start_collectors(self) -> None:
        for collector_name in self._collectors:
            self.start_collector(collector_name)

    def stop_collectors(self) -> None:
        for collector_name in self._collectors:
            self.stop_collector(collector_name)

    def join_collectors(self) -> None:
        # Copy to avoid removing items from the dict while iterating over the same dict
        collector_names = copy(list(self._collectors))
        for collector_name in collector_names:
            self.join_collector(collector_name)
