# CLI usage

Besides the web interface, CryoAgent also has several CLI commands to help deployment and development.

[[_TOC_]]

## start

This CLI command is used to start the CryoAgent application. For debugging, several options are provided to disable the metrics and/or the collectors, or choose a database (path) different than the default one.

```zsh
> cryo_agent start --help
Usage: cryo_agent start [OPTIONS]

  CLI command to start the agent

Options:
  --activate-metrics / -M, --deactivate-metrics
                                  Run with/without metric endpoint
  --activate-collectors / -C, --deactivate-collectors
                                  Run with/without collectors defined in
                                  provided configuration
  --database_path PATH            Path to the configuration database
  --help                          Show this message and exit.
```

For example, to start CryoAgent without activating any collectors (to test some web UI part, for example), one can execute the following command:

```zsh
> cryo_agent start -C
```

## create-plugin

This CLI command is primarily used to create external plugins. Note that for this command, the options do not **need** to be provided, as the user will be prompted for them.

```zsh
> cryo_agent create-plugin --help
Usage: cryo_agent create-plugin [OPTIONS]

  CLI command for creating external plugins

Options:
  --plugin_name TEXT
  --description TEXT
  --creator TEXT
  --help              Show this message and exit.
```

## change-port

This CLI command is used to change the port used to host the web interface. This is used when the default/configured port is already used by another application. For debugging purposes, it is possible to change this port for a database (path) different than the default one.

```zsh
> cryo_agent change-port --help  
Usage: cryo_agent change-port [OPTIONS]

  CLI command to change the web port of the agent

Options:
  --port INTEGER        The port that will used to start the web application
  --database_path PATH  Path to the configuration database
  --help                Show this message and exit.
```
