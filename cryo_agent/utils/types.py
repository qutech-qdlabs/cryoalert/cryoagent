from datetime import datetime
from enum import IntEnum
from typing import Any, Protocol, TypedDict

from sqlalchemy.orm import Session


class PluginClassProtocol(Protocol):
    def refresh_config(self) -> None: ...
    def run(self) -> None: ...
    def collect(self, session: Session) -> None: ...
    def handle_tasks(self) -> None: ...

    @staticmethod
    def validate_collector_config(
        config: dict, collector_config: dict | None = None, key_exceptions: list[str] | None = None
    ) -> list[str]: ...

    @staticmethod
    def validate_datagroup_config(
        config: dict, collector_config: dict | None = None, key_exceptions: list[str] | None = None
    ) -> list[str]: ...

    @staticmethod
    def validate_datapoint_config(
        config: dict, collector_config: dict | None = None, key_exceptions: list[str] | None = None
    ) -> list[str]: ...


class PluginRuntimeConfig(TypedDict):
    main: str


class PluginConfig(TypedDict):
    name: str
    version: str
    alias: str
    creator: str | list[str]
    description: str
    runtime: PluginRuntimeConfig
    settings: dict[str, Any] | None


class PluginData(TypedDict):
    config: PluginConfig
    plugin_class: type[PluginClassProtocol]
    internal: bool


class Tasks(IntEnum):
    ...


class TaskDict(TypedDict):
    task: Tasks
    data: dict[str, Any]


class CryoAgentOptions(TypedDict):
    activate_collectors: bool
    activate_metrics: bool


class DataPointData(TypedDict):
    timestamp: datetime
    value: float
    meta: dict


class DataGroupSettings(TypedDict, total=False):
    """
    In general, this dictionary will contain more than `group_label`.
    However, this is dependend on the plugin settings
    and therefore cannot be known before loading the plugin.
    """

    group_label: str


class CollectorDataPointDict(TypedDict):
    id: int
    variable_name: str
    unit: str


class CollectorDataGroupDict(TypedDict):
    group_label: str
    data_points: list[CollectorDataPointDict]


class CollectorDict(TypedDict):
    collector_name: str
    collector_class: str
    log_level: str
    scan_interval: int
    enabled: bool
    settings: dict
    data_groups: list[CollectorDataGroupDict]
