# Development Documentation

## Chapters

- [Setup](./setup.md)
- [Code Style](./code_style.md)
- [Architecture](./architecture/index.md)
- [Plugins](./plugins/index.md)
- [Publish](./publish.md)
