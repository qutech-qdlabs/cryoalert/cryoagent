# Database

CryoAgent makes use of an SQLite database using SQLAlchemy as an ORM.

## Datapoint Standardized Format

For each defined data point, a value will be collected by its collector at a certain time. This is stored in a dictionary, along with any metadata relevant for this datapoint.

```python
from cryo_agent.utils.types import DataPointData

latest_data: DataPointData = {
    "timestamp": datetime.utcnow(),
    "value": <value>,
    "meta": {
        "agent_version": cryo_agent.__version__,
        **data_group_info,
        **<other_info>
    }    
}
```

## Writing to database

A helper function `write_latest_datapoints` is available to write multiple collected data points to the database.

```python
from sqlalchemy.orm import Session

from cryo_agent.logic.base import write_latest_datapoints
from cryo_agent.utils.types import DataPointData

latest_data_point_data: dict[int, DataPointData] = {<data_point_id>: <latest_data>}
write_latest_datapoints(session, latest_data_points_data)
```

The key in the `latest_data_point_data` dictionary is the `data_point_id` of the `DataPoints` model (see below). This link is used to retrieve the variable name and the unit of the collected data point.

## Models

### Collectors

#### id

This is the primary key for this model.

#### collector_name

This is the name of the collector. It must be unique.

#### collector_class

This is the `alias` of the plugin as defined in the `plugin.yaml` file for each plugin. It must be unique. Only relevant for plugins, otherwise set to null.

#### log_level

This is the default log level for this specific collector. It will be used to initialize the logging during startup of the collector. Only relevant for plugins, otherwise set to null.

#### scan_interval

The scan interval is the interval at which the collector should start a new collection cycle. Only relevant for plugins, otherwise set to null.

#### enabled

This is a boolean describing if the collector is enabled. Only relevant for plugins, otherwise set to null.

#### settings

This is a JSON/dictionary of the base settings of the collector (see [plugin documentation](..\plugins\overview.md) for details). Only relevant for plugins, otherwise set to null.

#### is_plugin

This is a boolean to indicate of the collector is a plugin or not.

### Data groups

#### id

This is the primary key for this model.

#### collector_id

This is the primary key of the corresponding collector to which this data group belongs.

#### settings

This is a JSON/dictionary of the settings for this data group (see [plugin documentation](..\plugins\overview.md) for details).

#### group_label

Text identifier of the data group. Should be unique per collector for clarity, but this is not enforced.

### Data points

#### id

This is the primary key for this model.

#### data_group_id

This is the primary key of the corresponding data group to which this data point belongs.

#### variable_name

Name of the variable this data point represents.

#### unit

Physical unit of the variable this data point represents.

#### settings

This is a JSON/dictionary for the settings for this data point (see [plugin documentation](..\plugins\overview.md) for details).

### Latest Data

#### id

This is the primary for this model.

#### data_point_id

This is the primary key of the corresponding data point for which this model represents its latest data.

#### timestamp

Timestamp (datetime) representing when the data was collected.

#### value

Numerical value of the data. TO be set to null when older than a specified time or when something went wrong during collection

#### meta

Optional key-value pairs for storing data about this latest data entry. Commonly used to store additional context about the collected data point, including the version of CryoAgent used. Context clues are useful during debugging.

#### TODO more models
