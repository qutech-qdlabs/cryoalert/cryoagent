from datetime import datetime
from random import random

from cryo_agent.utils.types import DataPointData


def _get_random_value() -> float:
    return 10 * random() - 5


def get_dummy_data() -> list[DataPointData]:
    return {
        "timestamp": datetime.utcnow(),
        "value": _get_random_value(),
        "meta": {},
    }
