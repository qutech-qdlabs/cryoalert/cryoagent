# Oxford ILM200 Plugin

## Notes

### Status `command_type`

This data group behaves a little bit different from others. Instead of freely specifying a variable name, this data group gets different data based on the combination of the channel and the choice of variable name. The choices of variable names (that actually do something) are:

- channel{channel_id}_usage
- channel{channel_id}_status_current_flow_helium_probe
- channel{channel_id}_status_helium_probe_fast_rate
- channel{channel_id}_status_helium_probe_slow_rate
- channel{channel_id}_status_auto_fill_status
- channel{channel_id}_status_low_state_active
- channel{channel_id}_status_alarm_requested
- channel{channel_id}_status_pre_pulse_current_flowing
- relay_status_in_shut_down_state
- relay_status_alarm_sounding
- relay_status_in_alarm_state
- relay_status_alarm_silence_prohibited
- relay_status_relay1_active
- relay_status_relay2_active
- relay_status_relay3_active
- relay_status_relay4_active

Note that `{channel_id}` must be filled in by hand when creating the datapoint for this variable, in addition to providing the channel number explicitely as well. Furthermore, note that the `relay_status_*` variables do not depend on the channel. The channel value will be ignored for those variables.
