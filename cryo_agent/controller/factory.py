from __future__ import annotations

import logging
import multiprocessing as mp

from sqlalchemy import create_engine
from sqlalchemy.orm import Session

from cryo_agent.collectors.manager import CollectorManager
from cryo_agent.collectors.plugins.registry import plugin_registry
from cryo_agent.controller.process import CryoAgent
from cryo_agent.logic.collectors import get_enabled_collectors
from cryo_agent.logic.settings import get_cryo_agent_settings, get_web_settings
from cryo_agent.publisher.process import WebProcess
from cryo_agent.utils.logging import init_logging
from cryo_agent.utils.plugins import load_external_plugins, load_internal_plugins
from cryo_agent.utils.tasks import TaskRouter
from cryo_agent.utils.types import CryoAgentOptions

LOGGER = logging.getLogger(__name__)


class CryoAgentFactory:
    """Factory class for the CryoAgent."""

    @classmethod
    def create(cls, database_path: str, options: CryoAgentOptions) -> CryoAgent:
        """Create classmethod that constructs the cryoagent object."""

        # Set up log and task queue
        log_queue: mp.Queue = mp.Queue(-1)
        task_router = TaskRouter()
        collector_manager = CollectorManager(
            database_path,
            task_router,
            log_queue,
            enable_collectors=options["activate_collectors"],
        )

        # Load internal plugins
        load_internal_plugins()

        # Load external plugins (if exist)
        load_external_plugins()

        # Load settings
        with Session(create_engine(database_path)) as session:
            cryo_agent_settings = get_cryo_agent_settings(session)
            web_settings = get_web_settings(session)
            enabled_collectors = get_enabled_collectors(session)

        # Init logging
        init_logging(log_level=cryo_agent_settings.log_level)

        # Create WebProcess
        pub_proc = WebProcess(
            sqlite_path=database_path,
            data_source_name=cryo_agent_settings.fridge_name,
            web_settings=web_settings,
            log_queue=log_queue,
            task_router=task_router,
            registered_plugins=plugin_registry.plugin_registry,
            activate_metrics=options["activate_metrics"],
        )

        # Create CollectorProcesses
        collector_manager.create_collectors(enabled_collectors)

        # Create CryoAgent
        return CryoAgent(
            pub_proc,
            collector_manager,
            sqlite_path=database_path,
            log_queue=log_queue,
            task_conn=task_router.add_route("cryo_agent"),
        )
