# External Plugins

Besides plugins that come shipped with the cryo-agent package, it is also possible to define and use plugins external to it. This is useful in cases were you might want to try out a new plugin before it is committed, or if you don't want to have it committed at all. This document will guide you through the development setup and provide some useful tips and tricks for the creation of the plugin.

## Setup

First of all, install the cryo-agent package according to the [installation](../../installation.md) guide. While in the installation folder, open a terminal, activate the virtual environment, and run the following command:

```sh
> cryo_agent create-plugin
```

This command will prompt you for some questions, as shown below;

```zsh
> What is the name of the plugin?: <myplugin>
> Please describe the function of the plugin?: <my plugin description>
> Who is the creator of this plugin?: <author name>
```

Please provide meaningful answers. It is possible to change it later, but the plugin name is used to render multiple templates that are used to get you started.

After this, you should find a `plugins` folder in the installation folder. Overall, the folder structure should look something like this:

```bash
.
├── .venv
├── [logs]
├── plugins
│    ├── myplugin
│    │    ├── CHANGELOG.md
│    │    ├── plugin.py
│    │    ├── plugin.yaml
│    │    ├── README.md
│    ├── other-external-plugin
├── [cryo_agent.db]
├── [start_cryo_agent_script.bat]
```

## Development

The development of an external plugin is identical to that of an internal plugin. See its [documentation](overview.md) for the details.
