from functools import reduce


def recursive_get(d, *keys):  # pylint: disable=invalid-name
    ans = reduce(lambda c, k: c.get(k, {}), keys, d)
    if ans is not None:  # Test explicitely for None, because it can return booleans
        return ans
    else:
        return None
