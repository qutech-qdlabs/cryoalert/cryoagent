import logging

import numpy as np

LOGGER = logging.getLogger(__name__)


def ntc_thermistor_model(r: float, T0: float, b: float, R0: float) -> float:
    """
    Compute the temperature given a measured resistance by means of the NTC Thermistor model:

    1/T = 1/T0 + (1/b)*ln(R/R0)

    Args:
        r (float): input/measured resistance [Ohm]
        T0 (float): Temperature at which R0 was measured [K]
        b (float): model parameter [K]
        R0 (float): Resistance at temperature T0 [Ohm]

    Returns:
        float: Temperature [K]
    """
    try:
        return 1 / T0 + (1 / b) * np.log(r / R0)
    except Exception as exc:
        LOGGER.error(exc, exc_info=True)
        return float("nan")


def convert(raw_val: float, conversion_data: dict[str, float]) -> float:
    T0, b, R0 = conversion_data["T0"], conversion_data["b"], conversion_data["R0"]
    return 1 / ntc_thermistor_model(raw_val, T0, b, R0)


def validate_conversion_data(conversion_data: dict) -> list[str]:
    error_messages = []

    for param in ["T0", "b", "R0"]:
        if param not in conversion_data:
            error_messages.append(f"Parameter `{param}` was not present in conversion data. Could not convert data")

    return error_messages
