import multiprocessing as mp
from typing import cast

from cryo_agent.collectors.plugins.core import BaseCollectorPlugin
from cryo_agent.collectors.plugins.registry import plugin_registry
from cryo_agent.models import Collector
from cryo_agent.utils.tasks import TaskRouter


def get_collector_class(collector_setting: Collector) -> type[BaseCollectorPlugin]:
    return cast(
        type[BaseCollectorPlugin], plugin_registry.plugin_registry[collector_setting.collector_class]["plugin_class"]
    )


class CollectorFactory:
    def __init__(self, sqlite_path: str, task_router: TaskRouter, log_queue: mp.Queue) -> None:
        self.sqlite_path = sqlite_path
        self.task_router = task_router
        self.log_queue = log_queue

    def create_collector(self, collector_setting: Collector) -> BaseCollectorPlugin:
        col_class = get_collector_class(collector_setting)
        ret = col_class(
            sqlite_path=self.sqlite_path,
            config=collector_setting.serialize(),
            log_queue=self.log_queue,
            task_conn=self.task_router.add_route(collector_setting.collector_name),
        )
        return ret
