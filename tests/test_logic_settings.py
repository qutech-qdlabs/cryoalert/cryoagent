import pytest
from sqlalchemy import create_engine
from sqlalchemy.orm import Session, sessionmaker

from cryo_agent.logic.settings import (
    get_cryo_agent_settings,
    get_fridge_name,
    get_web_settings,
    write_cryo_agent_settings,
    write_web_settings,
)
from cryo_agent.models import Base, CryoAgentSettings


@pytest.fixture
def session():
    engine = create_engine("sqlite:///:memory:", connect_args={"check_same_thread": False})
    TestSession = sessionmaker(autocommit=False, autoflush=False, bind=engine)
    Base.metadata.create_all(bind=engine)
    return TestSession()


@pytest.mark.parametrize(
    "fridge_name,log_level,alert_settings_upload_method",
    [
        ("test_fridge", "INFO", "API"),
        ("test_fridge", "INFO", "File"),
    ],
)
def test_read_write_cryo_agent_settings(
    session: Session, fridge_name: str, log_level: str, alert_settings_upload_method: str
):
    write_cryo_agent_settings(session, fridge_name, log_level, alert_settings_upload_method)
    cas: CryoAgentSettings = get_cryo_agent_settings(session)
    assert cas.fridge_name == fridge_name
    assert cas.log_level == log_level
    assert cas.alert_settings_upload_method == alert_settings_upload_method


@pytest.mark.parametrize(
    "new_fridge_name,new_log_level,new_alert_settings_upload_method",
    [
        ("test_fridge2", None, None),
        (None, "INFO", None),
        (None, None, "File"),
    ],
)
def test_overwrite_cryo_agent_settings(
    session: Session, new_fridge_name: str, new_log_level: str, new_alert_settings_upload_method: str
):
    init_fridge_name, init_log_level, init_alert_settings_upload_method = "test_fridge", "DEBUG", "API"
    init_changed_items = write_cryo_agent_settings(
        session, init_fridge_name, init_log_level, init_alert_settings_upload_method
    )
    assert init_changed_items == {}

    new_changed_items = write_cryo_agent_settings(
        session, new_fridge_name, new_log_level, new_alert_settings_upload_method
    )
    if new_fridge_name and new_fridge_name != init_fridge_name:
        assert "fridge_name" in new_changed_items
        assert new_changed_items["fridge_name"] == new_fridge_name

    if new_log_level and new_log_level != init_log_level:
        assert "log_level" in new_changed_items
        assert new_changed_items["log_level"] == new_log_level

    if new_alert_settings_upload_method and new_alert_settings_upload_method != init_alert_settings_upload_method:
        assert "alert_settings_upload_method" in new_changed_items
        assert new_changed_items["alert_settings_upload_method"] == new_alert_settings_upload_method


@pytest.mark.parametrize(
    "port,log_level",
    [
        (8081, "INFO"),
    ],
)
def test_read_write_publishers_settings(session: Session, port: int, log_level: str):
    write_web_settings(session, port, log_level)
    web_settings = get_web_settings(session)
    assert web_settings.port == port
    assert web_settings.log_level == log_level


@pytest.mark.parametrize(
    "new_port,new_log_level",
    [
        (8082, None),  # only port changed
        (None, "INFO"),  # only log_level changed
        (8082, "INFO"),  # both changed
        (8081, "DEBUG"),  # nothing changed
        (8081, "INFO"),  # only log_level changed
        (8082, "DEBUG"),  # only port changed
    ],
)
def test_overwrite_publisher_settings(session: Session, new_port: int, new_log_level: str):
    init_port, init_log_level = 8081, "DEBUG"
    init_changed_items = write_web_settings(session, init_port, init_log_level)
    assert init_changed_items == {}

    new_changed_items = write_web_settings(session, new_port, new_log_level)
    if new_port and new_port != init_port:
        assert "port" in new_changed_items
        assert new_changed_items["port"] == new_port

    if new_log_level and new_log_level != init_log_level:
        assert "log_level" in new_changed_items
        assert new_changed_items["log_level"] == new_log_level


def test_get_fridge_name(session: Session):
    fridge_name = "test_fridge"
    write_cryo_agent_settings(session, fridge_name, log_level="INFO", alert_settings_upload_method="API")
    fridge_name = get_fridge_name(session)
    assert fridge_name == fridge_name
