import importlib
import logging
import sys
from importlib import resources
from importlib.abc import Traversable
from pathlib import Path

from yaml import load

try:
    from yaml import CLoader as Loader
except ImportError:
    from yaml import Loader

from cryo_agent.collectors.plugins.registry import plugin_registry
from cryo_agent.utils.types import PluginConfig

LOGGER = logging.getLogger(__name__)


def _load_plugin_config(plugin_dir: Path | Traversable) -> PluginConfig:
    config_path = plugin_dir / "plugin.yaml"
    return load(open(config_path, "r", encoding="utf-8"), Loader)  # type: ignore[call-overload]


def _load_plugin(import_path: str, resource: Path | Traversable, internal: bool) -> None:
    plugin_config = _load_plugin_config(resource)
    plugin_main_file = plugin_config["runtime"]["main"].split(".")[0]
    plugin_module = importlib.import_module(f"{import_path}.{resource.name}.{plugin_main_file}")
    plugin_registry.register_plugin(
        plugin_config=plugin_config,
        plugin_class=plugin_module.CollectorPlugin,
        internal=internal,
    )


def load_internal_plugins() -> None:
    for resource in resources.files("cryo_agent.collectors.plugins").iterdir():
        if resource.is_dir() and str(resource).endswith("-plugin") and Path(resource / "plugin.yaml").exists():  # type: ignore[arg-type]
            _load_plugin("cryo_agent.collectors.plugins", resource, internal=True)


def load_external_plugins() -> None:
    ext_plugin_path = Path.cwd() / "plugins"
    if not ext_plugin_path.exists():
        LOGGER.info("No External plugin folder found")
        return

    # Add working directory to path to find the plugin directory
    sys.path.append(str(Path.cwd()))

    # Load external plugins
    for plugin in (ext_plugin_path).iterdir():
        if plugin.is_dir() and str(plugin).endswith("-plugin") and Path(plugin / "plugin.yaml").exists():
            _load_plugin("plugins", plugin, internal=False)
