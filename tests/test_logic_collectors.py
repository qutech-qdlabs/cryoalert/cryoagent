import pytest
from sqlalchemy import create_engine
from sqlalchemy.orm import Session, sessionmaker

from cryo_agent.logic.collectors import (
    get_enabled_collectors,
    get_collectors,
    get_base_collector,
    setup_base_collector,
    add_new_collector,
    delete_collector,
    update_collector,
    add_data_group,
    update_data_group,
    add_data_point,
    update_data_point,
    get_collector,
    get_data_group,
    delete_data_group,
    delete_data_point,
    is_collector_name_used,
)
from cryo_agent.models import Base, Collector


@pytest.fixture
def _session():
    engine = create_engine("sqlite:///:memory:", connect_args={"check_same_thread": False})
    TestSession = sessionmaker(autocommit=False, autoflush=False, bind=engine)
    Base.metadata.create_all(bind=engine)
    return TestSession()


@pytest.fixture
def session(_session: Session):
    setup_base_collector(_session)
    add_new_collector(_session, "test_collector1", "test_collector_alias1")
    update_collector(_session, "test_collector1", {"log_level": "WARNING", "test_settings": 1234})
    add_new_collector(_session, "test_collector2", "test_collector_alias2")
    add_data_group(_session, "test_collector2", "test_data_group", {})
    update_data_group(_session, "test_collector2", "test_data_group", {"test": 4567})
    add_data_point(_session, "test_collector2", "test_data_group", "test_data_point", "", {})

    test_collector2 = get_collector(_session, "test_collector2")
    test_data_group = get_data_group(_session, test_collector2.id, "test_data_group")
    update_data_point(_session, test_data_group.id, "test_data_point", {"test": 7890})
    yield _session
    delete_collector(_session, "test_collector1")

    delete_data_point(_session, test_data_group.id, "test_data_point")
    delete_data_group(_session, test_collector2.id, "test_data_group")
    delete_collector(_session, "test_collector2")


def test_get_base_collector_before_setup(_session: Session):
    base_collector: Collector = get_base_collector(_session)
    assert base_collector is None, print(base_collector)


def test_get_base_collector_after_setup(session: Session):
    base_collector: Collector = get_base_collector(session)
    assert base_collector.collector_name == "base"


@pytest.mark.parametrize(
    "include_base",
    [
        False,
        True,
    ],
)
def test_get_collectors(session: Session, include_base: bool):
    collectors: list[Collector] = get_collectors(session, include_base=include_base)
    collector_names = [collector.collector_name for collector in collectors]
    assert ("base" in collector_names) == include_base


@pytest.mark.parametrize(
    "collector_name,is_enabled",
    [
        ("test_collector1", False),
        ("test_collector2", False),
    ],
)
def test_get_enabled_collectors(session: Session, collector_name: str, is_enabled: bool):
    enabled_collectors: list[Collector] = get_enabled_collectors(session)
    if is_enabled:
        assert collector_name in [collector.collector_name for collector in enabled_collectors]
    else:
        assert collector_name not in [collector.collector_name for collector in enabled_collectors]


@pytest.mark.parametrize(
    "name,is_used",
    [
        ("test_collector1", True),
        ("blabla", False),
    ],
)
def test_is_collector_name_used(session: Session, name: str, is_used: bool):
    assert is_collector_name_used(session, name) == is_used
