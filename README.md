# CryoAgent

![https://gitlab.tudelft.nl/qutech-qdlabs/cryoalert](https://gitlab.tudelft.nl/api/v4/projects/7522/jobs/artifacts/main/raw/public/badges/owner.svg?job=badges)
![https://gitlab.tudelft.nl/qutech-qdlabs/cryoalert/cryoagent/](https://gitlab.tudelft.nl/qutech-qdlabs/cryoalert/cryoagent/badges/main/pipeline.svg)
![https://gitlab.tudelft.nl/qutech-qdlabs/cryoalert/cryoagent/](https://gitlab.tudelft.nl/qutech-qdlabs/cryoalert/cryoagent/badges/main/coverage.svg?job=pytest)
![https://pypi.org/project/pip-audit/](https://gitlab.tudelft.nl/api/v4/projects/7522/jobs/artifacts/main/raw/public/badges/audit.svg?job=badges)
![https://gitlab.tudelft.nl/qutech-qdlabs/cryoalert/cryoagent/-/blob/main/LICENSE](https://gitlab.tudelft.nl/api/v4/projects/7522/jobs/artifacts/main/raw/public/badges/license_name.svg?job=badges)

The CryoAgent is part of the CryoAlert platform. It serves to safely expose data from a data source. An external application can poll data from the agent to obtain the latest set of data. A prometheus endpoint is available for scraping the latest datapoints. Additionally, it contains a webUI for interacting with CryoAgent. See the [documentation](./docs/index.md) for a detailed description of CryoAgent.
