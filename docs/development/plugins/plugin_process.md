# Plugin Process

The plugin process is the code of the collector. This document will provide a basic overview on the inner workings of this process.

## Code

In its simplest form, a collector is a subclass of a `BaseCollectorPlugin` and it __must__ be called `CollectorPlugin`. It has a `collect` method as an entrypoint for a single cycle. The base class will ensure it repeats with an interval as configurable via the web interface of this collector (this is included for all collectors). See the code below for a basic example. Please consult the source code of the [`CollectorPlugin`](../../../cryo_agent/collectors/plugins/_plugin_template/plugin.template.py) template or other collectors to see further implementation details.

```python
class CollectorPlugin(BaseCollectorPlugin):
    """
    A multiprocess process for collecting data
    """

    def collect(self, session: Session) -> None:
        """
        Entrypoint for single cycle of the collector
        """

        # Implement collection logic here
        latest_values: dict[int, DataPointData] = {}
        for data_group_info in self.data_groups:
            latest_values.update(...)

        # Write latest_values
        write_latest_datapoints(session, latest_values)
```

## BaseCollectorPlugin

The `BaseCollectorPlugin` is itself a subclass of `multiprocessing.Process`. It provides many abstractions and quality-of-life features that can be used by every collector. The most important ones are the processing loop, the ability to properly restart the collector after a crash, and the ability to handle tasks as triggered by the web interface process. See the [documentation](./tasks.md) for more information about the tasks. Note that, as a plugin developer, you probably need not worry about the inter-process tasks.

For more details on the `BaseCollectorPlugin`, please consult [the source code](../../../cryo_agent/collectors/plugins/core.py).
