from datetime import datetime
from typing import Any

from sqlalchemy import JSON, Boolean, Column, DateTime, Float, ForeignKey, Integer, Text
from sqlalchemy.orm import Mapped, declarative_base, relationship

from cryo_agent.utils.types import (
    CollectorDataGroupDict,
    CollectorDataPointDict,
    CollectorDict,
)

# Define the base for sqlalchemy models
Base = declarative_base()


class LogBookEntry(Base):  # type: ignore[valid-type, misc]
    __tablename__ = "logbook_entry"

    id = Column(Integer, primary_key=True)
    timestamp = Column(DateTime, nullable=False)
    username = Column(Text)
    action = Column(Text)
    description = Column(Text)


class AlertDefinition(Base):  # type: ignore[valid-type, misc]
    __tablename__ = "alert_definition"

    id = Column(Integer, primary_key=True)
    alert_name = Column(Text, unique=True)
    importance = Column(Integer)
    settings = Column(JSON)
    values = Column(JSON, nullable=True)


class CryoAgentSettings(Base):  # type: ignore[valid-type, misc]
    __tablename__ = "cryo_agent_settings"

    id = Column(Integer, primary_key=True)
    fridge_name = Column(Text)
    log_level = Column(Text)
    alert_settings_upload_method = Column(Text)


class WebSettings(Base):  # type: ignore[valid-type, misc]
    __tablename__ = "publisher_settings"

    id = Column(Integer, primary_key=True)
    port = Column(Integer)  # -> should only take effect after restart
    log_level = Column(Text)
    secret_key = Column(Text)  # -> not changeable, should not be shown

    def get_publisher_settings(self) -> dict:
        return {"port": self.port, "log_level": self.log_level, "secret_key": self.secret_key}


class Collector(Base):  # type: ignore[valid-type, misc]
    __tablename__ = "collector"

    id = Column(Integer, primary_key=True)
    is_plugin = Column(Boolean)
    collector_name = Column(Text)
    collector_class = Column(Text)
    log_level = Column(Text)
    scan_interval = Column(Integer)
    enabled = Column(Boolean)
    settings = Column(JSON)  # key-value pairs

    data_groups = relationship(
        "CollectorDataGroup",
        back_populates="collector",
        cascade="all, delete",
        passive_deletes=True,
        lazy="joined",
    )

    def serialize(self) -> CollectorDict:
        return {
            "collector_name": self.collector_name,
            "collector_class": self.collector_class,
            "log_level": self.log_level,
            "scan_interval": self.scan_interval,
            "enabled": self.enabled,
            "settings": self.settings,
            "data_groups": [data_group.serialize() for data_group in self.data_groups],
        }

    def is_configured(self) -> bool:
        if self.settings and self.settings != {}:
            return True

        return False


class CollectorDataGroup(Base):  # type: ignore[valid-type, misc]
    __tablename__ = "collector_data_group"

    id = Column(Integer, primary_key=True)

    collector_id = Column(Integer, ForeignKey("collector.id", ondelete="CASCADE"))
    collector = relationship("Collector", back_populates="data_groups")

    group_label = Column(Text, nullable=False)
    settings = Column(JSON)  # Key-value pairs

    data_points = relationship(
        "CollectorDataPoint",
        back_populates="data_group",
        cascade="all, delete-orphan",
        passive_deletes=True,
        lazy="joined",
    )

    def serialize(self) -> CollectorDataGroupDict:
        return {
            "group_label": self.group_label,
            **self.settings,
            "data_points": [data_point.serialize() for data_point in self.data_points],
        }


class CollectorDataPoint(Base):  # type: ignore[valid-type, misc]
    __tablename__ = "collector_data_point"

    id = Column(Integer, primary_key=True)

    data_group_id = Column(Integer, ForeignKey("collector_data_group.id", ondelete="CASCADE"))
    data_group = relationship("CollectorDataGroup", back_populates="data_points")

    variable_name = Column(Text, nullable=False)
    unit = Column(Text)
    settings = Column(JSON)  # key-value pairs

    latest_data = relationship(
        "LatestData",
        back_populates="data_point",
        cascade="all, delete-orphan",
        passive_deletes=True,
        lazy="joined",
        uselist=False,
    )

    def serialize(self) -> CollectorDataPointDict:
        return {"id": self.id, "variable_name": self.variable_name, "unit": self.unit, **self.settings}


class LatestData(Base):  # type: ignore[valid-type, misc]
    __tablename__ = "latest_data"

    id: Mapped[int] = Column(Integer, primary_key=True)
    data_point_id = Column(Integer, ForeignKey("collector_data_point.id", ondelete="CASCADE"))
    data_point = relationship("CollectorDataPoint", back_populates="latest_data")

    timestamp: Mapped[datetime] = Column(DateTime(timezone=False), nullable=False)
    value: Mapped[float] = Column(Float)
    meta: Mapped[dict] = Column(JSON)

    def serialize(self) -> dict[str, Any]:
        return {
            "data_point_id": self.data_point_id,
            "variable_name": self.data_point.variable_name,
            "unit": self.data_point.unit,
            "timestamp": self.timestamp.isoformat(),
            "value": self.value,
            "meta": self.meta,
        }
