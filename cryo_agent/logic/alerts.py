import logging
from datetime import datetime
from typing import Any

from sqlalchemy.orm import Session

from cryo_agent.logic.base import get_latest_data, write_latest_datapoints
from cryo_agent.logic.collectors import (
    get_base_collector,
    get_data_group,
    get_data_point,
)
from cryo_agent.models import AlertDefinition, CollectorDataPoint

LOGGER = logging.getLogger(__name__)


def get_alerts_enabled_data_point(session: Session) -> CollectorDataPoint:
    collector = get_base_collector(session)
    assert collector is not None

    data_group = get_data_group(session, collector.id, "alerts")
    assert data_group is not None

    alert_data_point = get_data_point(session, data_group.id, "enabled")
    assert alert_data_point is not None
    return alert_data_point


def write_new_enable_status(session: Session) -> None:
    # Get the current enabled status of the fridge to determine what the new one should be
    alerts_enabled_data_point = get_alerts_enabled_data_point(session)
    current_status = get_current_enabled_status(session, alerts_enabled_data_point_id=alerts_enabled_data_point.id)

    # Create new datapoint
    write_latest_datapoints(
        session,
        data_points_dict={
            alerts_enabled_data_point.id: {
                "timestamp": datetime.utcnow(),
                "value": not (current_status),
                "meta": {},
            }
        },
    )


def get_current_enabled_status(session: Session, alerts_enabled_data_point_id: int | None = None) -> bool:
    if alerts_enabled_data_point_id is None:
        alerts_enabled_data_point = get_alerts_enabled_data_point(session)
        alerts_enabled_data_point_id = alerts_enabled_data_point.id

    latest_data = get_latest_data(session, alerts_enabled_data_point_id)
    if latest_data:
        return bool(latest_data.value)

    return False


def get_alert_default_values(alert_def: dict[str, Any]):
    ret = {"unit": alert_def["unit"], "enabled": alert_def["default_enabled"], "bounds": {}}
    if alert_def.get("bounds", None):
        if "lower" in alert_def["bounds"]:
            ret["bounds"].update(
                {
                    "lower": {
                        "enabled": alert_def["bounds"]["lower"]["default_enabled"],
                        "value": alert_def["bounds"]["lower"]["default_value"],
                    }
                }
            )
        if "upper" in alert_def["bounds"]:
            ret["bounds"].update(
                {
                    "upper": {
                        "enabled": alert_def["bounds"]["upper"]["default_enabled"],
                        "value": alert_def["bounds"]["upper"]["default_value"],
                    },
                }
            )

    # Finally, return the default settings
    LOGGER.debug(f"Parsed default values: {ret}")
    return ret


def write_new_alert_definitions(session: Session, alert_definitions: list[dict[str, Any]]) -> None:
    # First, check for new alert definitions
    for alert_definition in alert_definitions:
        write_new_alert_definition(session, alert_definition["alert_name"], alert_definition)

    # Then, check if any alert definition is in the database but not in the new alert_definitions.
    # If so, remove those alert definitions and their alert values
    alert_defs = get_alert_settings(session)
    alert_names = [alert_definition["alert_name"] for alert_definition in alert_definitions]
    for alert_def in alert_defs:
        if alert_def.alert_name not in alert_names:
            LOGGER.debug(
                f"Found alert definition with {alert_def.alert_name=} that is not in published list. Deleting it"
            )
            session.query(AlertDefinition).filter(AlertDefinition.alert_name == alert_def.alert_name).delete()

    session.commit()


def write_new_alert_definition(session: Session, alert_name: str, settings: dict[str, Any]) -> None:
    # Check if an alert definition with given alert_name already exists
    exists_query = session.query(AlertDefinition.id).filter(AlertDefinition.alert_name == alert_name).exists()

    # If not exist, create new one
    # If exist, update the settings
    if session.query(exists_query).scalar():
        LOGGER.debug(f"There already exists an alert definition with {alert_name=}. Updating settings")
        session.query(AlertDefinition).filter(AlertDefinition.alert_name == alert_name).update(
            {
                "settings": settings,
                "importance": settings.get("importance", 0),
            }
        )
        return

    # Create new alert definition
    LOGGER.debug(f"No alert definition with {alert_name=} found. Creating new one")
    new_alert_def = AlertDefinition(
        alert_name=alert_name,
        settings=settings,
        importance=settings.get("importance", 0),
    )

    # Add to session and commit
    LOGGER.debug(f"Commiting new alert definition")
    session.add(new_alert_def)


def save_new_alert_values(session: Session, alert_id: int, new_values: dict[str, Any], importance: int = 0) -> None:
    # Update values column of the alert definition
    session.query(AlertDefinition).filter(AlertDefinition.id == alert_id).update(
        {
            "values": new_values,
            "importance": importance,
        }
    )

    # commit session
    LOGGER.debug(f"Commiting new alert settings")
    session.commit()


def get_alert_settings(session: Session) -> list[AlertDefinition]:
    return session.query(AlertDefinition).order_by(AlertDefinition.importance.desc()).all()


def get_alert_definition(session: Session, alert_id: int) -> AlertDefinition:
    return session.query(AlertDefinition).filter(AlertDefinition.id == alert_id).first()
