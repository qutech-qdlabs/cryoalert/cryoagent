import logging
from copy import deepcopy
from datetime import datetime
from typing import Any

from sqlalchemy.orm import Session

import cryo_agent
from cryo_agent.collectors.plugins.core import BaseCollectorPlugin
from cryo_agent.logic.base import write_latest_datapoints
from cryo_agent.utils.tcp_client import TcpRemoteClient

LOGGER = logging.getLogger(__name__)


def parse_values(data: str) -> float:
    try:
        return float(data)
    except ValueError:
        LOGGER.error(f"Could not parse `{data}` to a float")
        return float("nan")


class CollectorPlugin(BaseCollectorPlugin):
    """
    A multiprocess process for collecting data from a cryoscale
    """

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.tcp_client = TcpRemoteClient(*self.remote_address)

    def send(self, cmd: str) -> str | None:
        # Add mandatory CRLF
        cmd = cmd + "\r\n"

        # Send through tcp client
        res = self.tcp_client.send(cmd.encode())
        if res is None:
            return None

        # Decode response and strip the (CR)LF
        return res.decode().strip()

    @property
    def remote_address(self) -> tuple[str, int]:
        return (self.settings.get("remote_host", "localhost"), int(self.settings.get("remote_port", 33576)))

    def collect_values(self):
        ret = {}
        for data_group in self.data_groups:
            # Extract group meta information (everything except the datapoints information)
            group_meta = deepcopy(data_group)
            group_meta.pop("data_points")
            group_meta.update({"agent_version": cryo_agent.__version__})  # add version for compatability

            group_type = data_group.get("group_label")
            LOGGER.debug(f"Found group with {group_type=}: {data_group}")
            ret.update(self.collect_datagroup(data_group, group_meta))

        return ret

    def collect_datagroup(self, data_group: dict[str, Any], group_meta: dict[str, Any]):
        ret = {}
        for data_point in data_group.get("data_points", []):
            # Construct command to send
            connector_name = data_group["connector_name"]

            latest_data = self.collect_datapoint(connector_name, data_point["data_command"])
            if latest_data:
                latest_data.update({"meta": group_meta})
                ret.update({data_point["id"]: latest_data})

        return ret

    def collect_datapoint(self, connector_name: str, data_command: str):
        # Send command, and parse the result
        resp = self.send(f"@{connector_name}#{data_command}")
        if resp is None:
            return {}

        return {
            "timestamp": datetime.utcnow(),
            "value": parse_values(resp),
        }

    def collect(self, session: Session) -> None:
        """
        Entrypoint for single cycle of the collector
        """

        # Collect latest values from source
        latest_values = self.collect_values()
        if (latest_values is None) or (len(latest_values) == 0):
            return

        # Write latest_values
        write_latest_datapoints(session, latest_values)
