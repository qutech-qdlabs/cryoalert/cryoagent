# Installation Instructions

## Setup and install

First, create a folder in which the database and log files can live. Then, go into this folder in the terminal.

```cmd
mkdir example
cd example
```

It is prefered to run the cryo-agent in a virtual environment, as not to interfer with any other python installations on the machine.
This can be done using the following command (assuming python has been installed on the machine):

```cmd
python -m venv .venv
```

To work in the virtual environment, it needs to be activated.
This can be done using:

```cmd
.\.venv\Scripts\activate.bat 
```

It is then possible to install the cryo_agent in the virtual environment:

```cmd
pip install cryo-agent --extra-index-url https://gitlab.tudelft.nl/api/v4/projects/7052/packages/pypi/simple
```

## Firewall

Because a Prometheus agent scrapes CryoAgent for the fridge data, CryoAgent must be reachable by the Prometheus Agent. For this reason, the port on which CryoAgent web interface is served must be opened in the firewall. By default, the port to be opened in 8081.

## Run in Terminal

The agent can be run using the terminal using:

```cmd
cryo_agent start
```

## Run via Script and Automatic start

After the first start, a (windows) batch script will be copied to the root folder where CryoAgent was installed (the folder containing the `.venv` folder). This script can be used to start CryoAgent. Furthermore, a shortcut to this script can be made and copied into the startup folder of the PC (found at `C:\ProgramData\Microsoft\Windows\Start Menu\Programs\StartUp`). This ensures CryoAgent starts automatically when the computer has restarted (after login, if that is needed).
