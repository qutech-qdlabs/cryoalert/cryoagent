import logging
from datetime import datetime, timezone

from sqlalchemy.orm import Session

from cryo_agent.logic.base import write_latest_datapoints
from cryo_agent.logic.collectors import (
    get_base_collector,
    get_data_group,
    get_data_point,
)
from cryo_agent.models import CollectorDataPoint, LatestData

LOGGER = logging.getLogger(__name__)


def get_nitrogen_clean_data_point(session: Session) -> CollectorDataPoint:
    collector = get_base_collector(session)
    assert collector is not None
    data_group = get_data_group(session, collector.id, "nitrogen")
    assert data_group is not None

    nitrogen_clean_data = get_data_point(session, data_group.id, "clean")
    assert nitrogen_clean_data is not None
    return nitrogen_clean_data


def _get_last_cleaned_time(session: Session) -> datetime | None:
    nitrogen_clean_data_point = get_nitrogen_clean_data_point(session)

    latest_data: LatestData = (
        session.query(LatestData).filter(LatestData.data_point_id == nitrogen_clean_data_point.id).first()
    )
    if latest_data:
        return datetime.fromtimestamp(latest_data.value)

    return None


def get_last_cleaned_time(session: Session) -> datetime | None:
    if last_cleaned_time := _get_last_cleaned_time(session):
        last_cleaned_time = last_cleaned_time.replace(tzinfo=timezone.utc)
        return last_cleaned_time.astimezone(datetime.now().astimezone().tzinfo)

    return None


def get_last_cleaned_time_ago(session: Session) -> int | None:
    last_cleaned_time = _get_last_cleaned_time(session)
    if last_cleaned_time is None:
        return None

    return (datetime.utcnow().date() - last_cleaned_time.date()).days


def write_new_trap_clean(session: Session, clean_time: datetime | None = None) -> None:
    nitrogen_clean_data_point = get_nitrogen_clean_data_point(session)

    # Construct datapoint
    clean_time = clean_time or datetime.utcnow()
    write_latest_datapoints(
        session,
        {
            nitrogen_clean_data_point.id: {
                "timestamp": clean_time,
                "value": clean_time.timestamp(),
                "meta": {},
            }
        },
    )
