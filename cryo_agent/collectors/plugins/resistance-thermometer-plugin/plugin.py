import json
import logging
import re
from copy import deepcopy
from datetime import datetime
from typing import Any

from sqlalchemy.orm import Session

import cryo_agent
from cryo_agent.collectors.plugins.core import BaseCollectorPlugin
from cryo_agent.logic.base import write_latest_datapoints
from cryo_agent.utils.tcp_client import TcpRemoteClient

from .conversions import allen_barry, linear_interpolation, ntc, steinhart

LOGGER = logging.getLogger(__name__)

CONVERSION_METHODS = [
    "None",
    "NTC Thermistor Model",
    "Linear Interpolation",
    "Allen-Barry Model",
    "Steinhart-Hart Model",
]


REGEX_RESP_TO_FLOAT_MAP = {
    -1: re.compile(r"(?P<float>(\+|\-)?\d+(.\d+)?((E|e)(\+|\-)\d+)?)"),
    199: re.compile(r"^[a-zA-Z]*(?P<float>(\+|\-)?\d+(.\d+)?((E|e)(\+|\-)\d+)?)$"),
    2700: re.compile(r"^(?P<float>(\+|\-)?\d+(.\d+)?((E|e)(\+|\-)\d+)?)"),
}


def convert_raw_value(raw_val, conversion_method, conversion_data):
    if conversion_method not in CONVERSION_METHODS:
        return float("nan")

    if conversion_method == "None":
        return raw_val
    elif conversion_method == "NTC Thermistor Model":
        return ntc.convert(raw_val, conversion_data)
    elif conversion_method == "Linear Interpolation":
        return linear_interpolation.convert(raw_val, conversion_data)
    elif conversion_method == "Allen-Barry Model":
        return allen_barry.convert(raw_val, conversion_data)
    elif conversion_method == "Steinhart-Hart Model":
        return steinhart.convert(raw_val, conversion_data)

    LOGGER.warning(f"Unknown conversion method `{conversion_method}` selected. Raw value will be returned instead")
    return raw_val


class CollectorPlugin(BaseCollectorPlugin):
    """
    A multiprocess process for collecting data from a generic resistance thermometer
    """

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.tcp_client = TcpRemoteClient(*self.remote_address)

    def send(self, cmd: str) -> str | None:
        # Add mandatory CRLF
        cmd = cmd + "\r\n"

        # Send through tcp client
        res = self.tcp_client.send(cmd.encode())
        if res is None:
            return None

        # Decode response and strip the (CR)LF
        return res.decode().strip()

    def send_command(self, cmd: str) -> str | None:
        if self.connector_name:
            full_cmd = f"@{self.connector_name}#{cmd}"
        else:
            full_cmd = cmd

        return self.send(full_cmd)

    @property
    def connector_name(self) -> str:
        return self.settings.get("connector_name", None)

    @property
    def remote_address(self) -> tuple[str, int]:
        return (self.settings.get("remote_host", "localhost"), int(self.settings.get("remote_port", 33576)))

    def collect_values(self):
        ret = {}
        for data_group in self.data_groups:
            # Extract group meta information (everything except the datapoints information)
            group_meta = deepcopy(data_group)
            group_meta.pop("data_points")
            group_meta.update({"agent_version": cryo_agent.__version__})  # add version for compatability

            # Collect datagroup
            ret.update(self.collect_datagroup(data_group, group_meta))

        return ret

    def collect_datagroup(self, data_group: dict[str, Any], group_meta: dict[str, Any]):
        ret = {}

        for data_point in data_group.get("data_points"):
            latest_data = self.collect_datapoint(data_point, data_group)
            if latest_data:
                latest_data.update({"meta": group_meta})
                ret.update({data_point["id"]: latest_data})

        return ret

    def collect_datapoint(self, data_point: dict[str, Any], data_group: dict[str, Any]):
        keithley_model_number = int(data_group.get("keithley_model", -1))
        data_cmd = data_point["data_command"]
        scaling_factor = float(data_point["scaling_factor"])
        conversion_method = data_point["conversion_method"]
        try:
            conversion_data = json.loads(data_point["conversion_data"])
        except json.JSONDecodeError:
            conversion_data = {}

        # Send command
        resp = self.send_command(data_cmd)
        if resp is None:
            return {}

        if match := REGEX_RESP_TO_FLOAT_MAP[keithley_model_number].match(resp):
            try:
                raw_val = float(match.group("float")) * scaling_factor
                val = convert_raw_value(raw_val, conversion_method, conversion_data)
            except ValueError:
                val = float("nan")
        else:
            val = float("nan")

        return {
            "timestamp": datetime.utcnow(),
            "value": val,
        }

    def collect(self, session: Session) -> None:
        """
        Entrypoint for single cycle of the collector
        """

        # Collect latest values from source
        latest_values = self.collect_values()
        if (latest_values is None) or (len(latest_values) == 0):
            return

        # Write latest_values
        write_latest_datapoints(session, latest_values)

    @staticmethod
    def validate_datapoint_config(
        config: dict, collector_config: dict = None, key_exceptions: list[str] = None
    ) -> list[str]:
        # Get base error messages
        error_messages = BaseCollectorPlugin.validate_datapoint_config(
            config,
            collector_config=collector_config,
            key_exceptions=key_exceptions,
        )

        # Validate the conversion data needed for the chosen conversion method
        conversion_method = config.get("conversion_method")
        try:
            string_conversion_data = config.get("conversion_data")
            if string_conversion_data:
                conversion_data = json.loads(string_conversion_data)
            else:
                conversion_data = {}
        except json.JSONDecodeError as exc:
            error_messages.append(f"Could not parse conversion data into valid JSON. {exc}")
            conversion_data = {}

        if conversion_method == "None":
            # Conversion data is ignored in this case, so do as you please
            pass
        elif conversion_method == "NTC Thermistor Model":
            error_messages += ntc.validate_conversion_data(conversion_data)
        elif conversion_method == "Linear Interpolation":
            error_messages += linear_interpolation.validate_conversion_data(conversion_data)
        elif conversion_method == "Allen-Barry model":
            error_messages += allen_barry.validate_conversion_data(conversion_data)
        elif conversion_method == "Steinhart-Hart Model":
            error_messages += steinhart.validate_conversion_data(conversion_data)
        else:
            error_messages.append(f"Unknown conversion method `{conversion_method}` selected. Please choose another")

        # return all error messages
        return error_messages
