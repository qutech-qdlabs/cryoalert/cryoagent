import logging
import multiprocessing as mp

from cryo_agent.utils.decorators import get_tasks_for_process
from cryo_agent.utils.types import TaskDict

LOGGER = logging.getLogger(__name__)


class TaskHandler:
    def __init__(self, process, process_name) -> None:
        self.process = process
        self.task_map = get_tasks_for_process(process_name)

    def handle_task(self, task_object: TaskDict) -> None:
        task = task_object["task"]
        if task_handler := self.task_map.get(task):
            task_handler(self.process, task_object["data"])
        else:
            LOGGER.info(f"Unknown task `{task.name}` requested")


class TaskRouter:
    """
    Simple TaskRouter based on non-duplex multiprocessing pipes.
    Only the web process of cryo_agent can send tasks.
    """

    def __init__(self) -> None:
        # Make use of dict objects from multiprocessing to update these pipe maps
        # between different processes. e.g. update in one is update in all
        self.sending_pipe_map = mp.Manager().dict()

    def add_route(self, dest_name: str):
        if dest_name not in self.sending_pipe_map:
            rec_conn, send_conn = mp.Pipe()
            self.sending_pipe_map.update(
                {
                    dest_name: {
                        "rec_conn": rec_conn,
                        "send_conn": send_conn,
                    }
                }
            )
            return rec_conn
        else:
            return self.sending_pipe_map[dest_name]["rec_conn"]

    def send(self, to_name: str, task_data: dict) -> None:
        if conn_data := self.sending_pipe_map.get(to_name):
            conn_data["send_conn"].send(task_data)
            return

        LOGGER.debug(f"No destination with name `{to_name}` found. Most likely the collector is not enabled")
