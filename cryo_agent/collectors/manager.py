import logging

from cryo_agent.collectors.factory import CollectorFactory
from cryo_agent.collectors.warehouse import CollectorWarehouse
from cryo_agent.models import Collector

LOGGER = logging.getLogger(__name__)


class CollectorManager:
    def __init__(self, sqlite_path, task_router, log_queue, enable_collectors: bool = True) -> None:
        self.warehouse = CollectorWarehouse()  # Active collectors only
        self.factory = CollectorFactory(sqlite_path, task_router, log_queue)
        self.enable_collector = enable_collectors

    def create_collectors(self, collector_settings: list[Collector]) -> None:
        for collector_setting in collector_settings:
            self.create_collector(collector_setting)

    def create_collector(self, collector_setting: Collector) -> None:
        col_proc = self.factory.create_collector(collector_setting)
        self.warehouse.add_collector(collector_setting.collector_name, col_proc)

    def start_collector(self, collector_name: str):
        # If the flag for allowing to enable collectors is not set, simply do nothing
        if not self.enable_collector:
            LOGGER.info("Collectors have been disabled")
            return None

        return self.warehouse.start_collector(collector_name)

    def stop_collector(self, collector_name: str):
        if self.enable_collector:
            return self.warehouse.stop_collector(collector_name)

        return None

    def join_collector(self, collector_name: str):
        if self.enable_collector:
            return self.warehouse.join_collector(collector_name)

        return None

    def start_collectors(self):
        # If the flag for allowing to enable collectors is not set, simply do nothing
        if not self.enable_collector:
            LOGGER.info("Collectors have been disabled")
            return None

        return self.warehouse.start_collectors()

    def stop_collectors(self) -> None:
        if self.enable_collector:
            return self.warehouse.stop_collectors()

        return None

    def join_collectors(self) -> None:
        if self.enable_collector:
            return self.warehouse.join_collectors()

        return None
