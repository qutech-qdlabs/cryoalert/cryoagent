# Alerts

For each CryoAgent, is it possible to set different bound values and enabling/disabling for a specific alert rule. This allows for easy interaction with the CryoAlert platform and customizing alert settings that make sense for each fridge and/or expirement.

## Alert Overview

The overview page shows a table of alert rules currently known to CryoAgent. Clicking on a row will route you to the modification page for the alert rule displayed in that row.

![alert_overview](../img/alerts.PNG)

## Alert Modification

The modification page allows the user to modify the settings for the alert rule. This includes;

- Enable/disable the alert rule
- Upper bound (if applicable, not shown otherwise):
  - enabled
  - value
- Louwer bound (if applicable, not shown otherwise):
  - enabled
  - value

![alert_modification](../img/alerts_modification.PNG)
