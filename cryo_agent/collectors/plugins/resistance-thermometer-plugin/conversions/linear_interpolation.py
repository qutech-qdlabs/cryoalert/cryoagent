import logging

import numpy as np

LOGGER = logging.getLogger(__name__)


def convert(raw_val: float, conversion_data: dict[str, list[float]]) -> float:
    Rs = conversion_data["R"]
    Ts = conversion_data["T"]

    # Linearly extrapolate on both ends
    if raw_val < Rs[0]:
        return Ts[0] + (Ts[1] - Ts[0]) / (Rs[1] - Rs[0]) * (raw_val - Rs[0])
    elif raw_val > Rs[-1]:
        return Ts[-1] + (Ts[-1] - Ts[-2]) / (Rs[-1] - Rs[-2]) * (raw_val - Rs[-1])
    else:
        return np.interp(raw_val, Rs, Ts)


def validate_conversion_data(conversion_data: dict) -> list[str]:
    error_messages = []

    for param in ["R", "T"]:
        if param not in conversion_data:
            error_messages.append(f"Parameter `{param}` was not present in conversion data. Could not convert data")

    if "R" in conversion_data and "T" in conversion_data:
        Rs = conversion_data["R"]
        Ts = conversion_data["T"]

        # Check if the interpolation data arrays have the same length
        if len(Rs) != len(Ts):
            error_messages.append(f"Interpolation data arrays do not have the same length")

    return error_messages
