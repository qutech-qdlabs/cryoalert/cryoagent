import logging
from datetime import datetime, timezone

from sqlalchemy.orm import Session

from cryo_agent.logic.base import write_latest_datapoints
from cryo_agent.logic.collectors import (
    get_base_collector,
    get_data_group,
    get_data_point,
)
from cryo_agent.models import CollectorDataPoint, LatestData

LOGGER = logging.getLogger(__name__)


def get_nitrogen_refill_data_point(session: Session) -> CollectorDataPoint:
    collector = get_base_collector(session)
    assert collector is not None
    data_group = get_data_group(session, collector.id, "nitrogen")
    assert data_group is not None

    nitrogen_refill_data = get_data_point(session, data_group.id, "refill")
    assert nitrogen_refill_data is not None
    return nitrogen_refill_data


def _get_last_filled_time(session: Session) -> datetime | None:
    nitrogen_refill_data_point = get_nitrogen_refill_data_point(session)
    latest_data: LatestData = (
        session.query(LatestData).filter(LatestData.data_point_id == nitrogen_refill_data_point.id).first()
    )
    if latest_data:
        return datetime.fromtimestamp(latest_data.value)

    return None


def get_last_filled_time(session: Session) -> datetime | None:
    if last_filled_time := _get_last_filled_time(session):
        last_filled_time = last_filled_time.replace(tzinfo=timezone.utc)
        return last_filled_time.astimezone(datetime.now().astimezone().tzinfo)

    return None


def get_last_filled_time_ago(session: Session) -> int | None:
    last_filled_time = _get_last_filled_time(session)
    if last_filled_time is None:
        return None

    return (datetime.utcnow().date() - last_filled_time.date()).days


def write_new_trap_fill(session: Session, refill_time: datetime | None = None) -> None:
    nitrogen_refill_data_point = get_nitrogen_refill_data_point(session)

    # Construct datapoint
    refill_time = refill_time or datetime.utcnow()
    write_latest_datapoints(
        session,
        {
            nitrogen_refill_data_point.id: {
                "timestamp": refill_time,
                "value": refill_time.timestamp(),
                "meta": {},
            }
        },
    )
