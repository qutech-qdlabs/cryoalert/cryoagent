# Code Style

## Naming and coding convenctions

There is no general guideline for naming and coding conventions. However, it is advised to stick to clear names for variables and functions, and to prefer readable over clever code.

## Linting

To maintain some coherent coding style, a linting script is included that lints the code the same for every contributor.

```bat
> .\.build-support\tools\lint.bat
```

This will run the following linting tools:

- [black](https://github.com/psf/black)
- [djhtml](https://github.com/rtts/djhtml)
- [autopep8](https://github.com/hhatto/autopep8)
- [isort](https://github.com/PyCQA/isort)
- custom version checker
  - Checks if version numbers in `pyproject.toml` and `cryo_agent.__version__` are identical
- [pylint](https://github.com/pylint-dev/pylint)
