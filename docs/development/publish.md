# Publish Instructions

A helper script has been made to make publication easier. However, to use it, some settings needs to be configured to make poetry aware of where to publish it.

First, add the tudelft gitlab repository to the poetry config;

```cmd
> poetry config repositories.gitlab-tudelft https://gitlab.tudelft.nl/api/v4/projects/7052/packages/pypi
```

Afterwards, add configurtation for accessing the repository. Using the below command will prompt you for a password.

```cmd
> poetry config http-basic.gitlab-tudelft <pip-access-token>
```

If thats done successfully, the following script can be run to publish the package to the registry:

```cmd
> .\.build-support\publish\publish.ps1
Building cryo_agent (0.2.0)
  - Building sdist
  - Built cryo_agent-0.2.0.tar.gz
  - Building wheel
  - Built cryo_agent-0.2.0-py3-none-any.whl

Publishing cryo_agent (0.2.0) to gitlab-tudelft
 - Uploading cryo_agent-0.2.0-py3-none-any.whl 0%  
 - Uploading cryo_agent-0.2.0-py3-none-any.whl 100%
 - Uploading cryo_agent-0.2.0.tar.gz 0%
 - Uploading cryo_agent-0.2.0.tar.gz 100%
```
