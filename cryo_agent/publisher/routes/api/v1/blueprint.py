from flask import Blueprint

from .alerts import bp as bp_alerts
from .latest_data import bp as bp_latest_data

# Define API/v1 parent blueprint
bp = Blueprint("v1", __name__, url_prefix="/v1")

# Add versioned api children blueprints
bp.register_blueprint(bp_latest_data)
bp.register_blueprint(bp_alerts)
