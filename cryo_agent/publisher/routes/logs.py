from flask import Blueprint, flash, redirect, render_template, request, url_for
from sqlalchemy.orm import Session

from cryo_agent.logic.logs import (
    get_logbook_entries,
    get_logbook_entry,
    modify_logbook_entry,
    write_new_logbook_entry,
)
from cryo_agent.publisher.db import get_engine

bp = Blueprint("logs", __name__, url_prefix="/logs")


@bp.route("/overview/", methods=["GET"])
def overview():
    with Session(get_engine()) as session:
        return render_template(
            "logbook/logs_overview.html",
            entries=get_logbook_entries(session),
        )


@bp.route("/add/", methods=["GET", "POST"])
def add():
    if request.method == "POST":
        with Session(get_engine()) as session:
            username = request.form["fusername"]
            action = request.form["faction"]
            description = request.form["fdescription"]

            # Flash messages that entries are required
            if not username:
                flash("Username is required")
            if not action:
                flash("Action is required")
            if not description:
                flash("Description is required")

            # If all are present, add entry
            if all([username, action, description]):
                write_new_logbook_entry(session, username, action, description)
                return redirect(url_for("logs.overview"))

    # By default, render add page
    return render_template("logbook/logs_add.html")


@bp.route("/details/<int:entry_id>", methods=["GET", "POST"])
def details(entry_id):
    with Session(get_engine()) as session:
        if request.method == "POST":
            username = request.form["fusername"]
            action = request.form["faction"]
            description = request.form["fdescription"]

            # Flash messages that entries are required
            if not username:
                flash("Username is required")
            if not action:
                flash("Action is required")
            if not description:
                flash("Description is required")

            # If all are present, add entry
            if all([username, action, description]):
                modify_logbook_entry(session, entry_id, username, action, description)
                return redirect(url_for("logs.overview"))

        return render_template(
            "logbook/logs_details.html",
            entry=get_logbook_entry(session, entry_id),
        )
