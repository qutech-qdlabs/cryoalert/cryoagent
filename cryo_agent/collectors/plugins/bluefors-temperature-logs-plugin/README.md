# Bluefors-temperature-logs-plugin

## Notes

This collector should only have a single data group called `temperatures`. Although the name itself is irrelevant, it describes best what data is collected. Any other data groups will collect the same data.
