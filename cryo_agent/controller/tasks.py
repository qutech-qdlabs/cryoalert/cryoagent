import logging
from enum import IntEnum, auto

from cryo_agent.utils.decorators import task

LOGGER = logging.getLogger(__name__)


class Tasks(IntEnum):
    PRINT = auto()
    UPDATE_LOG_LEVEL = auto()
    ENABLE_COLLECTOR = auto()
    DISABLE_COLLECTOR = auto()


@task("cryo_agent", Tasks.PRINT)
def task_handler_print(_, task_data: dict) -> None:
    print(task_data)


@task("cryo_agent", Tasks.UPDATE_LOG_LEVEL)
def task_handler_update_log_level(_, task_data: dict) -> None:
    """Change log level of the root logger of the CryoAgentController (main) process"""
    LOGGER.info(f"Changed root logging level to `{task_data['log_level']}`")
    logging.getLogger().setLevel(task_data["log_level"])


@task("cryo_agent", Tasks.DISABLE_COLLECTOR)
def task_handler_disable_collector(cryo_agent, task_data: dict) -> None:
    collector_name = task_data["collector_name"]
    LOGGER.info(f"Stopping collector {collector_name}")
    cryo_agent.disable_collector(collector_name)


@task("cryo_agent", Tasks.ENABLE_COLLECTOR)
def task_handler_enable_collector(cryo_agent, task_data: dict) -> None:
    collector_name = task_data["collector_name"]
    LOGGER.info(f"Starting collector {collector_name}")
    cryo_agent.enable_collector(collector_name)
