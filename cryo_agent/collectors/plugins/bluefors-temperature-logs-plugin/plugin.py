import logging
import os
from datetime import datetime, timezone
from typing import Any

from sqlalchemy.orm import Session

from cryo_agent.collectors.plugins.core import BaseCollectorPlugin
from cryo_agent.logic.base import write_latest_datapoints
from cryo_agent.utils.file import read_last_line

LOGGER = logging.getLogger(__name__)


def parse_data(data: str) -> dict:
    split_data = data.split(",")
    if len(split_data) < 3:
        LOGGER.warning(f"Could not parse `{split_data}`")
        return {}

    dt = (
        datetime.strptime(f"{split_data[0]}T{split_data[1]}", "%d-%m-%yT%H:%M:%S")
        .astimezone()
        .astimezone(tz=timezone.utc)
    )

    return {
        "timestamp": dt,
        "value": float(split_data[2]),
    }


class CollectorPlugin(BaseCollectorPlugin):
    """
    A multiprocess process for collecting data from an bluefors Cryostat using its log files
    """

    @property
    def log_file_root_path(self) -> str:
        return self.settings.get("abs_logs_folder_path", "C:\\bf_logging")

    def collect_values(self):
        date_string_now = datetime.now().date().strftime("%y-%m-%d")
        if date_string_now not in os.listdir(self.log_file_root_path):
            LOGGER.debug(f"No folder found for {date_string_now}")
            return None

        logs_folder_now = self.log_file_root_path + "\\" + date_string_now
        ret = {}
        for data_group in self.data_groups:
            ret.update(self.collect_datagroup(data_group, logs_folder_now))

        return ret

    def collect_datagroup(self, data_group: dict[str, Any], logs_folder: str):
        ret = {}
        for data_point in data_group.get("data_points", []):
            file_name_part = f"CH{data_point['channel']} T"
            file_name_candidates = [file for file in os.listdir(logs_folder) if file_name_part in file]
            if len(file_name_candidates) == 0:
                LOGGER.warning(f"Could not find temperature file for `CH{data_point['channel']}`")
                ret.update({data_point["id"]: {"timestamp": datetime.utcnow(), "value": float("nan")}})
            else:
                latest_data = self.collect_datapoint(logs_folder + "\\" + file_name_candidates[0])
                if latest_data is not None:
                    ret.update({data_point["id"]: latest_data})

        return ret

    def collect_datapoint(self, abs_path_log_file: str):
        with open(abs_path_log_file, "rb") as log_file_handler:
            last_line = read_last_line(log_file_handler).decode().strip()
            if last_line:
                return parse_data(last_line)

        return None

    def collect(self, session: Session) -> None:
        """
        Entrypoint for single cycle of the collector
        """

        # Collect latest values from source
        latest_values = self.collect_values()
        if (latest_values is None) or (len(latest_values) == 0):
            return

        write_latest_datapoints(session, latest_values)
