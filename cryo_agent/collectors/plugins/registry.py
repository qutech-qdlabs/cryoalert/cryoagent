import logging

from cryo_agent.utils.types import PluginConfig, PluginData

LOGGER = logging.getLogger(__name__)


class PluginRegistry:
    def __init__(self) -> None:
        self.plugin_registry: dict[str, PluginData] = {}

    def register_plugin(self, plugin_config: PluginConfig, plugin_class: type, internal: bool) -> None:
        plugin_name_alias = plugin_config["alias"]
        if plugin_name_alias not in self.plugin_registry:
            self.plugin_registry.update(
                {plugin_name_alias: PluginData(config=plugin_config, plugin_class=plugin_class, internal=internal)}
            )
        else:
            LOGGER.warning(
                f"Tried to load plugin class {plugin_class} with alias `{plugin_name_alias}`,\
                 but a plugin class with this alias already exists: {self.plugin_registry[plugin_name_alias]['plugin_class']}"
            )

    def register_internal_plugin(self, plugin_config: PluginConfig, plugin_class: type):
        self.register_plugin(plugin_config, plugin_class, internal=True)

    def register_external_plugin(self, plugin_config: PluginConfig, plugin_class: type):
        self.register_plugin(plugin_config, plugin_class, internal=False)


# Initialize global plugin registry
plugin_registry = PluginRegistry()
