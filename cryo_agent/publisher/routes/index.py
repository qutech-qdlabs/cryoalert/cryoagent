from flask import Blueprint, render_template
from sqlalchemy.orm import Session

import cryo_agent
from cryo_agent.logic.alerts import get_current_enabled_status
from cryo_agent.logic.clean import get_last_cleaned_time_ago
from cryo_agent.logic.nitrogen import get_last_filled_time_ago
from cryo_agent.logic.settings import get_fridge_name
from cryo_agent.publisher.db import get_engine

bp = Blueprint("index", __name__)


@bp.route("/", methods=["GET"])
def index():
    with Session(get_engine()) as session:
        return render_template(
            "index.html",
            version=cryo_agent.__version__,
            fridge_name=get_fridge_name(session),
            current_status=get_current_enabled_status(session),
            last_filled_ago=get_last_filled_time_ago(session),
            last_cleaned_ago=get_last_cleaned_time_ago(session),
        )
