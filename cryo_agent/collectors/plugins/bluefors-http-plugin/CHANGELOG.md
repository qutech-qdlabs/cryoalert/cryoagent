# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.1.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [0.1.0] - 2024-08-07

### Added

- Added `valid_data_only` boolean data point setting to enforce using only valid data
- Added a helper script that simulates the Bluefors HTTP API. Not used during produciton, but usefull during development/testing

### Fixed

### Changed

### Removed

- Removed unused code leftover from CryoAgent V1
