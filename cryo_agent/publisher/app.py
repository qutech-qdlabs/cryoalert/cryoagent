import logging
from datetime import datetime

from flask import Flask, redirect, url_for
from prometheus_client import make_wsgi_app
from werkzeug.middleware.dispatcher import DispatcherMiddleware

from cryo_agent.models import WebSettings
from cryo_agent.publisher import routes
from cryo_agent.utils.exceptions import ObjectNotFoundException
from cryo_agent.utils.types import PluginData

LOGGER = logging.getLogger(__name__)


def create_app(
    sqlite_path: str,
    data_source_name: str,
    config: WebSettings,
    task_router,
    registered_plugins: dict[str, PluginData],
    activate_metrics: bool = True,
):
    # Create flask app
    app = Flask(__name__)

    # Add relevant config options also in app config for later use
    config_dict = config.get_publisher_settings()
    config_dict.update({"sqlite_path": sqlite_path, "data_source_name": data_source_name})

    # ensuring all config variables have UPPER-case keys, as required by the app.config.from_mapping method
    config_dict = {key.upper(): value for key, value in config_dict.items()}

    # Load app config from file
    app.config.from_mapping(config_dict)

    # Add registred plugins to app for later use by views
    app.config["REGISTERED_PLUGINS"] = registered_plugins

    # Add task router to app
    app.config["TASK_ROUTER"] = task_router

    @app.context_processor
    def utility_processor():
        def year():
            return datetime.now().year

        return {"year": year}

    @app.template_filter("ts2dt")
    def timestamp_to_datetime(timestamp: int):
        # Assume timestamp in milliseconds
        return datetime.fromtimestamp(timestamp / 1000.0).strftime("%d-%m-%Y %H:%M")

    @app.template_filter("path_to_breadcrumbs")
    def path_to_breadcrumbs(path: str):
        if path.startswith("/"):
            path = path[1:]

        if path.endswith("/"):
            path = path[:-1]

        return path.split("/")

    # add error_handling
    @app.errorhandler(ObjectNotFoundException)
    def object_not_found_handler(exc: ObjectNotFoundException):
        return redirect(url_for("exceptions.exceptions", exc=exc.description)), 302

    # app.register_error_handler(ObjectNotFoundException, object_not_found_handler)

    # add routes
    app.register_blueprint(routes.api.bp)
    app.register_blueprint(routes.alerts.bp)
    app.register_blueprint(routes.clean.bp)
    app.register_blueprint(routes.collectors.bp)
    app.register_blueprint(routes.exceptions.bp)
    app.register_blueprint(routes.index.bp)
    app.register_blueprint(routes.latest_data.bp)
    app.register_blueprint(routes.logs.bp)
    app.register_blueprint(routes.nitrogen.bp)
    app.register_blueprint(routes.settings.bp)

    # Add prometheus metric endpoints specified
    if activate_metrics:
        app.wsgi_app = DispatcherMiddleware(app.wsgi_app, {"/metrics": make_wsgi_app()})  # type: ignore
    else:
        LOGGER.info("Metric endpoint has been disabled")

    # Finally, return the app
    return app
