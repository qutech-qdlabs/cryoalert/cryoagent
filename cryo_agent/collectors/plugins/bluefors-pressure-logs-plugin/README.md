# Bluefors-pressure-logs-plugin

## Notes

This collector should only have a single data group called `pressures`. Although the name itself is irrelevant, it describes best what data is collected. Any other data groups will collect the same data.
