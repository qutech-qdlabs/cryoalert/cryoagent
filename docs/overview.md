# Overview

CryoAgent serves to safely expose data from a data source into a standarized format and provide an external HTTP endpoint for scraping by [Prometheus](https://prometheus.io/).

## Design

The basic functionality of CryoAgent is very simple, as is shown in the image below. In essence, there are one or more collector processes that each write to a local SQLite database. Another process, running a webserver that serves the HTTP endpoint needed for scraping by Prometheus, reads from this database.

![High Level Architecture](./img/highlevelarch.png "High Level Architecture")

In addition to the the collector and web processes, there is the main process from which these processes are spawned. The main process is responsible for starting and stopping its various child processes.

Furthermore, the web process provides a web interface for defining, starting, and managing the various collectors. This is achieved by leveraging interprocess communication.

## Collector Plugins

The collectors are created using a plugin design pattern. This allows for relatively standalone operation of each collector, and the possibility to easily add new ones, even external to the CryoAgent application.

For CryoAgent, the following collectors are available. For further documentation on each collector, follow the link.

- [Dummy](../cryo_agent/collectors/plugins/dummy-plugin/README.md)
- Bluefors
  - [HTTP API](../cryo_agent/collectors/plugins/bluefors-http-plugin/README.md)
  - Logs files
    - [Temperatures](../cryo_agent/collectors/plugins/bluefors-temperature-logs-plugin/README.md)
    - [Pressures](../cryo_agent/collectors/plugins/bluefors-pressure-logs-plugin/README.md)
    - [Compressor Status](../cryo_agent/collectors/plugins/bluefors-status-logs-plugin/README.md)
- Oxford
  - [Triton TCP API](../cryo_agent/collectors/plugins/oxford-triton-plugin/README.md)
  - [ILM200 TCP API](../cryo_agent/collectors/plugins/oxford-ilm200-plugin/README.md)
- [CryoScale](../cryo_agent/collectors/plugins/cryoscale-plugin/README.md)
- [Resistance Thermometers](../cryo_agent/collectors/plugins/resistance-thermometer-plugin/README.md)
