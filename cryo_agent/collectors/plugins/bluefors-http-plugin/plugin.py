import logging
from copy import deepcopy
from datetime import datetime
from typing import Optional
from urllib.parse import urljoin

import requests
from sqlalchemy.orm import Session

import cryo_agent
from cryo_agent.collectors.plugins.core import BaseCollectorPlugin
from cryo_agent.logic.base import write_latest_datapoints
from cryo_agent.utils.types import DataPointData

from .utils import BlueforsDataClass

LOGGER = logging.getLogger(__name__)


class CollectorPlugin(BaseCollectorPlugin):
    """
    A multiprocess process for collecting data from an bluefors Cryostat
    """

    @property
    def base_url(self) -> str:
        return self.settings["base_url"]

    def _loop_data_group(self, data_group_info: dict) -> dict[int, DataPointData]:
        """
        Internal method to process all data queries and processing per datagroup configured
        """

        # Extract group meta information (everything except the datapoints information)
        group_meta = deepcopy(data_group_info)
        group_meta.pop("data_points")
        group_meta.update({"agent_version": cryo_agent.__version__})  # add version for compatability

        # get url for the group
        asset_url = urljoin(self.base_url, data_group_info["group_address"].replace(".", "/"))

        # Try to get the data
        try:
            res: requests.Response = requests.get(asset_url, timeout=5)
        except (requests.exceptions.Timeout, requests.exceptions.ConnectionError) as exc:
            LOGGER.error(exc)
            return []

        # Try parsing the data
        try:
            raw_data = res.json()
        except requests.exceptions.JSONDecodeError as exc:
            LOGGER.error(exc, exc_info=1)
            return []

        # Extract raw data to DataClass
        data = BlueforsDataClass.from_dict(raw_data)

        # Extract data for all datapoints in the group
        dp_data_dict = {}
        for data_point_info in data_group_info["data_points"]:
            full_address = ".".join([data_group_info["group_address"], data_point_info["data_address"]])
            dp_data_dict.update(
                {
                    data_point_info["id"]: self._loop_data_point(
                        data,
                        full_address,
                        data_point_info,
                        group_meta=group_meta,
                    )
                }
            )

        return dp_data_dict

    def _loop_data_point(
        self,
        data: BlueforsDataClass,
        full_value_address: str,
        info: dict[str, str],
        group_meta: Optional[dict] = None,
    ) -> DataPointData:
        """
        Internal method to process all datapoints in a datagroup as configured
        """
        group_meta = group_meta if group_meta else {}

        # Get value
        value_data = data.data.get(full_value_address, None)

        # Check if value_data is not None, probably caused by wrong address
        if not value_data:
            LOGGER.warning(f"Did not find any values at specified address: {full_value_address=}")
            return {
                "timestamp": datetime.utcnow(),
                "value": float("NaN"),
                "meta": {
                    "error": f"Did not find any values at specified address: {full_value_address=}",
                    **group_meta,
                },
            }

        # Update datapoint dictionary for commiting to database
        if value_data.content and value_data.content.latest_valid_value:
            LOGGER.debug("Found and using latest_valid_data")
            val = value_data.content.latest_valid_value.value
            val_date = datetime.fromtimestamp(value_data.content.latest_valid_value.date / 1000)
            val_outdated = value_data.content.latest_valid_value.outdated
        elif value_data.content and not info.get("valid_data_only", True) and value_data.content.latest_value:
            LOGGER.debug("Found and using latest_data")
            val = value_data.content.latest_value.value
            val_date = datetime.fromtimestamp(value_data.content.latest_value.date / 1000)
            val_outdated = value_data.content.latest_value.outdated
        else:
            LOGGER.debug("Found no data!")
            val = float("NaN")
            val_date = datetime.utcnow()
            val_outdated = True

        dp_data = {
            "timestamp": val_date,
            "value": float(val),
            "meta": {
                "value_address": value_data.name,
                "expected_type": value_data.type,
                "outdated": val_outdated,
                **group_meta,
            },
        }

        return dp_data

    def collect(self, session: Session) -> None:
        """
        Entrypoint for single cycle of the collector
        """

        latest_data_points_data: dict[int, DataPointData] = {}
        for data_group_info in self.data_groups:
            latest_data_points_data.update(self._loop_data_group(data_group_info))

        # Create datapoint and write it to the database
        write_latest_datapoints(session, latest_data_points_data)

    @staticmethod
    def validate_datapoint_config(
        config: dict, collector_config: dict = None, key_exceptions: list[str] = None
    ) -> list[str]:
        config["valid_data_only"] = "valid_data_only" in config
        return BaseCollectorPlugin.validate_datapoint_config(config, collector_config, key_exceptions)
