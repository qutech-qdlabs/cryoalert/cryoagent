import logging
from datetime import datetime, timedelta
from typing import Any

from sqlalchemy.orm import Session

from cryo_agent.models import (
    Collector,
    CollectorDataGroup,
    CollectorDataPoint,
    LatestData,
)
from cryo_agent.utils.types import DataPointData

LOGGER = logging.getLogger(__name__)

TIMEOUT_INTERVAL_MS = 5 * 60 * 1_000
TIMEOUT_INTERVAL_MIN = 5


def _cleanup_old_data(session: Session) -> None:
    # Get the ids
    # The idea is to select only those that belong to a plugin, not the base collector
    ids = session.scalars(
        session.query(LatestData.id)
        .join(CollectorDataPoint)
        .join(CollectorDataGroup)
        .join(Collector)
        .where(Collector.is_plugin == 1)
        .where(LatestData.value is not None)
        .where(LatestData.timestamp < (datetime.utcnow() - timedelta(minutes=TIMEOUT_INTERVAL_MIN)))
    ).all()

    # Execute the update query
    session.query(LatestData).where(LatestData.id.in_(ids)).update({LatestData.value: float("NaN")})


def write_latest_datapoints(session: Session, data_points_dict: dict[int, DataPointData]) -> None:
    new_latest_data = []
    for data_point_id, data_point_data in data_points_dict.items():
        latest_data: LatestData = session.query(LatestData).filter(LatestData.data_point_id == data_point_id).first()
        if latest_data:  # row exists, update it
            session.query(LatestData).filter(LatestData.id == latest_data.id).update(data_point_data)
        else:  # Row does not exist, create it
            new_latest_data.append(LatestData(data_point_id=data_point_id, **data_point_data))

    # Iff time_collected is older than some interval, set it to NaN
    _cleanup_old_data(session)

    session.add_all(new_latest_data)
    session.commit()


def get_latest_data(session: Session, data_point_id: int) -> LatestData:
    return session.query(LatestData).filter(LatestData.data_point_id == data_point_id).first()


def delete_latest_data(session: Session, data_point_id: int) -> None:
    session.query(LatestData).filter(LatestData.data_point_id == data_point_id).delete()
    session.commit()


def get_all_latest_datapoints(session: Session) -> dict[str, dict[str, Any]]:
    """
    Function to get the latest datapoints from all collectors
    """

    all_latest_data = session.query(LatestData).all()
    ret: dict[str, dict[str, Any]] = {}
    for latest_data in all_latest_data:
        collector_name = latest_data.data_point.data_group.collector.collector_name
        data_group_label = latest_data.data_point.data_group.group_label
        if collector_name not in ret:
            ret[collector_name] = {}

        if data_group_label not in ret[collector_name]:
            ret[collector_name][data_group_label] = []

        ret[collector_name][data_group_label].append(latest_data.serialize())

    return ret
