import logging

from flask import Blueprint, render_template, request

LOGGER = logging.getLogger(__name__)

bp = Blueprint("exceptions", __name__, url_prefix="/exceptions")


@bp.route("/")
def exceptions():
    exception_str = request.args.get("exc")
    return render_template(
        "exceptions.html",
        exc_str=exception_str,
    )
