import logging
import os
from datetime import datetime, timezone
from typing import Any

from sqlalchemy.orm import Session

from cryo_agent.collectors.plugins.core import BaseCollectorPlugin
from cryo_agent.logic.base import write_latest_datapoints
from cryo_agent.utils.file import read_last_line

LOGGER = logging.getLogger(__name__)


def parse_data(data: str, data_name) -> dict:
    split_data = data.split(",")
    if len(split_data) < 3:
        LOGGER.warning(f"Could not parse `{split_data}`")
        return {}

    dt = (
        datetime.strptime(f"{split_data[0]}T{split_data[1]}", "%d-%m-%yT%H:%M:%S")
        .astimezone()
        .astimezone(tz=timezone.utc)
    )

    # Get the index which indicates where the information for the requested data name is located
    try:
        data_name_index = split_data.index(data_name)
    except ValueError:
        LOGGER.warning(f"Could not find index for `{data_name}` in `{split_data}`")
        return {}

    return {
        "timestamp": dt,
        "value": float(split_data[data_name_index + 1]),
    }


class CollectorPlugin(BaseCollectorPlugin):
    """
    A multiprocess process for collecting data from an bluefors Cryostat using its log files
    """

    @property
    def log_file_root_path(self) -> str:
        return self.settings.get("abs_logs_folder_path", "C:\\bf_logging")

    def collect_values(self):
        date_string_now = datetime.now().date().strftime("%y-%m-%d")
        if date_string_now not in os.listdir(self.log_file_root_path):
            return None

        logs_folder_now = self.log_file_root_path + "\\" + date_string_now
        ret = {}
        for data_group in self.data_groups:
            ret.update(self.collect_datagroup(data_group, logs_folder_now))

        return ret

    def collect_datagroup(self, data_group: dict[str, Any], logs_folder: str):
        # Get file name and extract data from it
        # If no file name is found, return empty dict
        try:
            file_name = next(file for file in os.listdir(logs_folder) if "Status" in file)
        except StopIteration:
            return {}

        with open(f"{logs_folder}\\{file_name}", "rb") as log_file_handler:
            last_line = read_last_line(log_file_handler).decode().strip()
            if not last_line:
                return {}

            # For each data point, extract data from the last_line of the data file
            ret = {}
            for data_point in data_group.get("data_points", []):
                latest_data = self.collect_datapoint(last_line, data_point)
                if latest_data is not None:
                    ret.update({data_point["id"]: latest_data})

            return ret

    def collect_datapoint(self, last_line: str, data_point_info: dict):
        return parse_data(last_line, data_point_info["data_name"])

    def collect(self, session: Session) -> None:
        """
        Entrypoint for single cycle of the collector
        """

        # Collect latest values from parser
        latest_values = self.collect_values()
        if (latest_values is None) or (len(latest_values) == 0):
            return

        # Write latest_values
        write_latest_datapoints(session, latest_values)
