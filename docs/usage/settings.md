# Settings

## General Settings

![general_settings](../img/settings_general.PNG)

## Web Settings

![web_settings](../img/settings_web.PNG)
