# Oxford-triton-plugin

## Notes

Defaults data_commands:

- temperatures -> "READ:DEV:T{channel}:TEMP:SIG:TEMP"
- pressures -> "READ:DEV:P{channel}:PRES:SIG:PRES"
- status -> "READ:DEV:C{channel}:PTC"
