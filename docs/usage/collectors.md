# Collectors

## Collectors

![collectors_overview](../img/collectors.PNG)

### Overview

![collector_overview](../img/collector_detail.PNG)

### Add Collector

![collector_add](../img/collectors_add.PNG)

## Data Groups

### Overview

![data_group](../img/collector_data_group.PNG)

### Add

![data_group_add](../img/collector_add_data_group.PNG)

## Data Points

### Overview

![data_point](../img/collector_data_group_data_point.PNG)

### Add

Note that, when adding units, it is possible to use SI-prefixes. However, when Prometheus scrapes the data, the base units will be used and the values will be converted accordingly.

The following units can be used:

```py
READABLE_UNITS_MAP = {
    # pressures
    "Pa": "pascal",
    "Bar": "bar",
    "Psi": "psi",
    #
    # time
    "s": "seconds",
    "seconds": "seconds",
    #
    # temperatures
    "K": "kelvin",
    "Kelvin": "kelvin",
    "Celsius": "celsius",
    "C": "celsius",
    "F": "fahrenheit",
    "Fahrenheit": "fahrenheit",
    #
    # weights
    "kg": "kg",
    #
    # Frequencies
    "Hz": "hertz",
    #
    # Power
    "W": "watt",
}
```

![data_point_add](../img/collector_data_group_data_point_add.PNG)
