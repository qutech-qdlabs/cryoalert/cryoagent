from datetime import datetime
from random import random

from flask import Flask

app = Flask(__name__)


def gen_temp_data(name: str):
    return {
        "name": name,
        "type": "Value.Number.Float.Unit.temperature",
        "content": {
            "read_only": True,
            "maximum_age": 5000,
            "lockable": False,
            "locked": False,
            "owner": name,
            "latest_valid_value": {
                "value": 315 * random(),
                "outdated": False,
                "date": int(datetime.utcnow().timestamp() * 1000),
                "status": "SYNCHRONIZED",
                "exception": "",
            },
            "latest_value": {
                "value": 315 * random(),
                "outdated": False,
                "date": int(datetime.utcnow().timestamp() * 1000),
                "status": "SYNCHRONIZED",
                "exception": "",
            },
        },
    }


def gen_temp_name(name: str, temp_name: str):
    return {
        "name": name,
        "type": "Value.String",
        "content": {
            "read_only": True,
            "maximum_age": 5000,
            "lockable": False,
            "locked": False,
            "owner": name,
            "latest_valid_value": {
                "value": temp_name,
                "outdated": False,
                "date": int(datetime.utcnow().timestamp() * 1000),
                "status": "SYNCHRONIZED",
                "exception": "",
            },
            "latest_value": {
                "value": temp_name,
                "outdated": False,
                "date": int(datetime.utcnow().timestamp() * 1000),
                "status": "SYNCHRONIZED",
                "exception": "",
            },
        },
    }


@app.route("/values/driver/bftc/data/channels")
def temperature_data():
    return {
        "data": {
            "driver.bftc.data.channels": {
                "name": "driver.bftc.data.channels",
                "type": None,
            },
            "driver.bftc.data.channels.channel_1.temperature": gen_temp_data(
                "driver.bftc.data.channels.channel_1.temperature"
            ),
            "driver.bftc.data.channels.channel_1.name": gen_temp_name(
                "driver.bftc.data.channels.channel_1.name", "50K-flange"
            ),
            "driver.bftc.data.channels.channel_2.temperature": gen_temp_data(
                "driver.bftc.data.channels.channel_2.temperature"
            ),
            "driver.bftc.data.channels.channel_2.name": gen_temp_name(
                "driver.bftc.data.channels.channel_2.name", "temp_c2"
            ),
            "driver.bftc.data.channels.channel_5.temperature": gen_temp_data(
                "driver.bftc.data.channels.channel_5.temperature"
            ),
            "driver.bftc.data.channels.channel_5.name": gen_temp_name(
                "driver.bftc.data.channels.channel_5.name", "temp_c5"
            ),
            "driver.bftc.data.channels.channel_6.temperature": gen_temp_data(
                "driver.bftc.data.channels.channel_6.temperature"
            ),
            "driver.bftc.data.channels.channel_6.name": gen_temp_name(
                "driver.bftc.data.channels.channel_6.name", "temp_c6"
            ),
        }
    }


def gen_pres_data(name: str):
    return {
        "name": name,
        "type": "Value.Number.Float.Unit.pressure",
        "content": {
            "read_only": True,
            "maximum_age": 5000,
            "lockable": False,
            "locked": False,
            "owner": name,
            "latest_valid_value": {
                "value": random(),
                "outdated": False,
                "date": int(datetime.utcnow().timestamp() * 1000),
                "status": "SYNCHRONIZED",
                "exception": "",
            },
            "latest_value": {
                "value": random(),
                "outdated": False,
                "date": int(datetime.utcnow().timestamp() * 1000),
                "status": "SYNCHRONIZED",
                "exception": "",
            },
        },
    }


def gen_pres_name(name: str):
    return {
        "name": name,
        "type": "Value.String",
        "content": {
            "read_only": True,
            "maximum_age": 5000,
            "lockable": False,
            "locked": False,
            "owner": name,
            "latest_valid_value": {
                "value": "          ",
                "outdated": False,
                "date": int(datetime.utcnow().timestamp() * 1000),
                "status": "SYNCHRONIZED",
                "exception": "",
            },
            "latest_value": {
                "value": "        ",
                "outdated": False,
                "date": int(datetime.utcnow().timestamp() * 1000),
                "status": "SYNCHRONIZED",
                "exception": "",
            },
        },
    }


@app.route("/values/driver/maxigauge/pressures")
def pressure_data():
    return {
        "data": {
            "driver.maxigauge.pressures.p1": gen_pres_data("driver.maxigauge.pressures.p1"),
            "driver.maxigauge.pressures.p1.name": gen_pres_name("driver.maxigauge.pressures.p1.name"),
            "driver.maxigauge.pressures.p2": gen_pres_data("driver.maxigauge.pressures.p2"),
            "driver.maxigauge.pressures.p2.name": gen_pres_name("driver.maxigauge.pressures.p2.name"),
            "driver.maxigauge.pressures.p3": gen_pres_data("driver.maxigauge.pressures.p3"),
            "driver.maxigauge.pressures.p3.name": gen_pres_name("driver.maxigauge.pressures.p3.name"),
            "driver.maxigauge.pressures.p4": gen_pres_data("driver.maxigauge.pressures.p4"),
            "driver.maxigauge.pressures.p4.name": gen_pres_name("driver.maxigauge.pressures.p4.name"),
            "driver.maxigauge.pressures.p5": gen_pres_data("driver.maxigauge.pressures.p5"),
            "driver.maxigauge.pressures.p5.name": gen_pres_name("driver.maxigauge.pressures.p5.name"),
            "driver.maxigauge.pressures.p6": gen_pres_data("driver.maxigauge.pressures.p6"),
            "driver.maxigauge.pressures.p6.name": gen_pres_name("driver.maxigauge.pressures.p6.name"),
        }
    }


def gen_compressor_data(name: str, dtype=float):
    return {
        "name": name,
        "type": "Value.Number.Float",
        "content": {
            "read_only": True,
            "maximum_age": 5000,
            "lockable": False,
            "locked": False,
            "owner": name,
            "latest_valid_value": None,
            # {
            #    "value": dtype(60 * random()),
            #    "outdated": False,
            #    "date": int(datetime.utcnow().timestamp() * 1000),
            #    "status": "SYNCHRONIZED",
            #    "exception": "",
            # },
            "latest_value": {
                "value": dtype(60 * random()),
                "outdated": False,
                "date": int(datetime.utcnow().timestamp() * 1000),
                "status": "SYNCHRONIZED",
                "exception": "",
            },
        },
    }


@app.route("/values/driver/cpa")
def compressor_data2():
    return {
        "data": {
            "driver.cpa": {"name": "driver.cpa", "type": None},
            "driver.cpa.compressor_running_status": gen_compressor_data(
                "driver.cpa.compressor_running_status", dtype=bool
            ),
            "driver.cpa.coolant_in_temperature": gen_compressor_data("driver.cpa.coolant_in_temperature"),
            "driver.cpa.coolant_out_temperature": gen_compressor_data("driver.cpa.coolant_out_temperature"),
            "driver.cpa.oil_temperature": gen_compressor_data("driver.cpa.oil_temperature"),
            "driver.cpa.helium_temperature": gen_compressor_data("driver.cpa.helium_temperature"),
        }
    }


@app.route("/values/driver/cpa2")
def compressor_data():
    return {
        "data": {
            "driver.cpa2": {"name": "driver.cpa", "type": None},
            "driver.cpa2.compressor_running_status": gen_compressor_data(
                "driver.cpa.compressor_running_status", dtype=bool
            ),
            "driver.cpa2.coolant_in_temperature": gen_compressor_data("driver.cpa.coolant_in_temperature"),
            "driver.cpa2.coolant_out_temperature": gen_compressor_data("driver.cpa.coolant_out_temperature"),
            "driver.cpa2.oil_temperature": gen_compressor_data("driver.cpa.oil_temperature"),
            "driver.cpa2.helium_temperature": gen_compressor_data("driver.cpa.helium_temperature"),
        }
    }


if __name__ == "__main__":
    app.run(port=49099)
